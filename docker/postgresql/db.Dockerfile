#
# example Dockerfile for https://docs.docker.com/engine/examples/postgresql_service/
#

FROM postgres:11.5-alpine

COPY init.sql /docker-entrypoint-initdb.d/