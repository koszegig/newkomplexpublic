#!/bin/bash
cp  cp .env_set .env
docker-compose up --build -d
docker-compose exec php composer install --ignore-platform-reqs
docker-compose exec php php artisan migrate --seed
docker-compose exec php chmod -R 777 /var/www/

