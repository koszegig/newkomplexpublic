#!/bin/bash
cd resources/komplex_admin
ng build --prod --aot
rm -f ../../public/*.js; rm -f ../../public/*.css; rm -f ../../public/*.ico;cp dist/*.js ../../public/; cp dist/*.css ../../public/; cp dist/*.ico ../../public/;cp dist/index.html ../views/index.html
cp dist/index.html ../views/angular.blade.php
