<?php


namespace App\GraphQL\Queries;
use App\Libraries\Database\QueryBuilderSP;
use App\Models\Types\Tnavtasks1;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Closure;
use App\Helpers\QuerysBuilderHelper;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class NavTasksQuery extends Query
{

    protected $attributes = [
        'name' => 'navtasks',
    ];
    public function type(): Type
    {
        return Type::listOf(GraphQL::type('NavTask'));
    }

    public function args(): array
    {
        return [
            'nvtsks_tasktype' => [
                'name' => 'nvtsks_tasktype',
                'type' => Type::int()
                //'rules' => ['required']
            ],
            'nvtsks_start' => [
                'name' => 'nvtsks_start',
                'type' => Type::string()
            ],
            'nvtsks_lasttick' => [
                'name' => 'nvtsks_lasttick',
                'type' => Type::string()
            ],
            'nvtsks_stop' => [
                'name' => 'nvtsks_stop',
                'type' => Type::string()
            ],
            'nvtsks_pid' => [
                'name' => 'nvtsks_pid',
                'type' => Type::boolean()
            ],
            'nvtsks_statuscode' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'limit' => [
                'type' => Type::int(),
                'defaultValue' => 100,
            ],
            'page' => [
                'type' => Type::int(),
                'defaultValue' => 1,
            ],
            'orderBy' => [
                'type' => Type::string(),
                'defaultValue' => '',
            ],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $fields = $getSelectFields();
        $selectfields = array_map((function($value) {
            return substr($value,strrpos($value,'.')+1);
        }), $fields->getSelect());
        $orderby ='order by nvtsks_id desc';
        if (isset($args['orderBy']) && trim($args['orderBy']) !='' ) {
            $orderby ='order by '.$args['orderBy'];
        }
        $model = "App\Models\Types\Tnavtasks1";
        $_model = new $model();
        $_request = \App\Helpers\QuerysBuilderHelper::tempfilters();
        $select = \App\Helpers\QuerysBuilderHelper::selectfields($fields->getSelect(),$_model->getFillable());
        $builder = new QueryBuilderSP($model, [], null, $select,[],false,$orderby);
        if (isset($args['page']) && isset($args['limit'])) {
            $dmdata =$builder->Paginator($args['page'],$args['limit']);
        } else{
            $dmdata =$builder->get();
        }
        return $dmdata;
    }
}
