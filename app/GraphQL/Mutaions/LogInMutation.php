<?php


namespace App\GraphQL\Mutaions;
use GraphQL\Type\Definition\Type;
use JWTAuth;
use Folklore\GraphQL\Support\Mutation;

class LogInMutation extends Mutation
{
    protected $attributes = [
        'name' => 'logIn'
    ];

    public function type()
    {
        return Type::string();
    }

    public function args()
    {
        return [
            'email' => [
                'name' => 'username',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required', 'username'],
            ],
            'password' => [
                'name' => 'password',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $credentials = [
            'username' => $args['username'],
            'password' => $args['password']
        ];

        $token =  JWTAuth::attempt($credentials);

        if (!$token) {
            throw new \Exception('Unauthorized!');
        }

        return $token;
    }
}
