<?php


namespace App\GraphQL\Types;
use App\Models\Types\Tnavtasks1;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class NavTaskType extends GraphQLType
{
    protected $attributes = [
        'name' => 'NavTask',
        'description' => 'NAV beküldések',
        'model' => Tnavtasks1::class
    ];

    public function fields(): array
    {
        return [

            'nvtsks_tasktype' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_start' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_lasttick' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_stop' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_pid' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_statuscode' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_stopme' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_limit' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_cron' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_kasszazaraskor' => [
                'type' => Type::string(),
                'description' => '',
            ],
            'nvtsks_indulhat' => [
                'type' => Type::string(),
                'description' => '',
            ]

        ];
    }
}
