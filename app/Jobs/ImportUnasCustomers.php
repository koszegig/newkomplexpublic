<?php

namespace App\Jobs;

use App\Libraries\WebshopConnect\UnasRestApi;
use Barryvdh\Debugbar\Twig\Extension\Dump;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use App\Models\Tables\WebTasks;
use App\Models\Types\TWebTasks1;
use App\Models\Types\TWebQueues1;
use App\Models\Tables\WebQueues;
use App\Models\Tables\DMWebProductRecord;

class ImportUnasCustomers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $_argumentum;
    /**
     * @var array
     */
    protected $options;
    /**
     * ExportProducts constructor.
     * @param $_argumentum
     * @param $options
     */
    public function __construct($_argumentum = [], $options = [])
    {
        $this->_argumentum =$_argumentum;
        $this->options =$options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        WebTasks::Start('ImportUnasCustomers');
        WebTasks::updateLastTicket('ImportUnasCustomers',10);
        $unas =  new UnasRestApi();
        $response=$unas->getAuthToken();
        $response =$unas->Get('getCustomer','<Params>
			</Params>');
        dd(json_encode($response));
        WebTasks::Stop('ImportUnasCustomers',$this->newCount,$this->refreshCount);
    }
}
