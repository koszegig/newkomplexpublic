<?php

namespace App\Jobs;

use App\Libraries\WebshopConnect\UnasRestApi;
use Barryvdh\Debugbar\Twig\Extension\Dump;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


use App\Models\Tables\WebTasks;
use App\Models\Types\TWebTasks1;
use App\Models\Types\TWebQueues1;
use App\Models\Tables\WebQueues;
use App\Models\Tables\DMWebProductRecord;

use Illuminate\Support\Facades\Log;

/**
 * Class ExportProducts
 * @package App\Jobs
 */
class ExportUnasProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     *
     */
    const  SKU_VALIDATOR = '/^[A-Z0-9]+$/';
    /**
     * Create a new job instance.
     *
     * @return void
     */
    /**
     * @var
     */
    protected $newCount=0;
    /**
     * @var
     */
    protected $refreshCount=0;
    /**
     * @var array
     */
    protected $_argumentum;
    /**
     * @var array
     */
    protected $options;
    /**
     * @var array
     */
    protected $configurableoptions = [];
    /**
     * @var int
     */
    protected $limitCount=0;
    /**
     * @var array
     */
    protected $product =[];
    /**
     * @var
     */
    protected $productClass;
    /**
     * @var
     */
    protected $productClassFields;
    /**
     * @var int
     */
    protected $i= 0;
    /**
     * ExportProducts constructor.
     * @param $_argumentum
     * @param $options
     */
    public function __construct($_argumentum = [], $options = [])
    {
        $this->_argumentum =$_argumentum;
        $this->options =$options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->export();
    }


    /**
     * @return bool
     */
    public function export(){

        WebTasks::Start('ExportUnasProducts');
        WebTasks::updateLastTicket('ExportUnasProducts',10);
        $unas =  new UnasRestApi();
        $response=$unas->getAuthToken();
        $products = TWebQueues1::getQueue("postunasproducts","postproduct");
        foreach ($products as $product) {
            $response =[];
           $response =$unas->Post('setProduct',$product['wbque_xmlmessage']);
            if($response['Product']['Status'] =='ok')
            {
                $record = [
                    'wbswcp_unas_id' => $response['Product']['Id'] ,
                ];
                DMWebProductRecord::UpdateOrCreate(['wbswcp_wbque_id' => $product['wbque_id']],$record);
                $queueRecord = [
                    'wbque_ready' => true ,
                ];
                WebQueues::UpdateOrCreate(['wbque_id' => $product['wbque_id']],$queueRecord);
            }
            \MyArray::toStringToLAll($product['wbque_id']);
        }
        $putproducts = TWebQueues1::getQueue("putunasproducts","putunasproducts");
        foreach ($putproducts as $putproduct) {
            $response =[];
            $response =$unas->Post('setProduct',$putproduct['wbque_xmlmessage']);
            if($response['Product']['Status'] =='ok')
            {
                $record = [
                    'wbswcp_modosult' => false ,
                ];
                 DMWebProductRecord::UpdateOrCreate(['wbswcp_sku' => $response['Product']['Sku']],$record);
                $queueRecord = [
                    'wbque_ready' => true ,
                ];
                WebQueues::UpdateOrCreate(['wbque_id' => $putproduct['wbque_id']],$queueRecord);
            }
          //  \MyArray::toStringToLAll($putproduct['wbque_id']);
        }

        WebTasks::Stop('ExportUnasProducts',$this->newCount,$this->refreshCount);

    }

}

