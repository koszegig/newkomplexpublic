<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use App\Email;
use Illuminate\Database\Eloquent\Builder;

trait HasEmails
{
	 /**
     * A model may have multiple emails.
     */

	public function emails(){
		return $this->hasMany('App\Email','related_id');
	}

	private function getClass(){
    	$class = get_called_class ();
    	$class = explode('\\', $class);
    	return array_last($class);
  	}

  	protected function getStoredEmail($email): Email
    {
        if (\MyString::isUUID($email)) {
            return app(Email::class)->find($email);
        }

        if (\MyString::isEmail($email)) {
            return app(Email::class)->findByEmail($email);
        }

        return $email;
    }



  	protected function getPrimaryEmail(): Email
    {
        return app(Email::class)->getPrimary();
    }

	/**
     * Assign the given email to the model.
     *
     * @param string $email
     *
     * @return $this
     */
    public function addEmail($email)
    {
    	$related = $this->getClass();
        $_email = new Email(['email' => $email['email'], 'primary' => $email['primary'],'related_id' => $this->id,'related_type' => $related]);
        $this->emails()->save($_email);

        return $this;
    }

      /**
     * Assign the given email to the model.
     *
     * @param array $emails
     *
     * @return $this
     */
    public function addEmails($emails)
    {
    	$related = $this->getClass();
    	$_emails = [];
    	foreach($emails as $email)
        	$_emails[] = new Email(['email' => $email['email'], 'primary' => $email['primary'],'related_id' => $this->id,'related_type' => $related]);

        $this->emails()->save($_emails);

        return $this;
    }

      /**
     * Revoke the given email from the model.
     *
     * @param array|string $email
     */
    public function removeEmail($emails)
    {
    	if(!is_array($emails))
    		$emails = [$emails];
    	foreach($emails as $email)
        	$this->emails()->detach($this->getStoredEmail($email));
    }

    /**
     * Remove all current emails and set the given ones.
     *
     * @param array|string $emails
     *
     * @return $this
     */
    public function syncEmails($emails)
    {
        $this->emails()->detach();
        if(is_array($emails))
        	return $this->addEmails($emails);

        return $this->addEmail($emails);
    }
}
