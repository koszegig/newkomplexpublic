<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use App\HealthcareWorkers;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasHealthCareWorkers
{
	/**
     * A model may have multiple healthcareworkers.
     */
    public function healthcareworkers(): BelongsToMany
    {
        return $this->belongsToMany(
            \App\HealthcareWorkers::class,
            'surgeryconnection',
            'surgeryid',
            'healthcareworkerid'
        );
    }

    /**
     * Assign the given group to the model.
     *
     * @param array|string|\App\Contracts\UserHealthcareworker ...$healthcareworkers
     *
     * @return $this
     */
    public function assignHealthcareworker(...$healthcareworkers)
    {
        $healthcareworkers = collect($healthcareworkers)
            ->flatten()
            ->map(function ($role) {
                return $this->getStoredHealthcareworker($role);
            })
            ->all();

        $this->healthcareworkers()->saveMany($healthcareworkers);

        return $this;
    }

      protected function getStoredHealthcareworker($group): UserHealthcareworker
    {
        if (\MyString::isUUID($group)) {
            return app(UserHealthcareworker::class)->findById($group);
        }

        if (is_string($group)) {
            return app(UserHealthcareworker::class)->findByName($group);
        }

        return $group;
    }

     /**
     * Revoke the given group from the model.
     *
     * @param string|\App\Contracts\UserHealthcareworker $group
     */
    public function removeHealthcareworker($group)
    {
        $this->healthcareworkers()->detach($this->getStoredHealthcareworker($group));
    }



    /**
     * Remove all current group and set the given ones.
     *
     * @param array|\App\Contracts\UserHealthcareworker|string ...$group
     *
     * @return $this
     */
    public function syncHealthcareworkers(...$group)
    {
        $this->healthcareworkers()->detach();

        return $this->assignHealthcareworker($group);
    }

    /**
     * Determine if the model has (one of) the given group(s).
     *
     * @param string|array|\App\Contracts\UserHealthcareworker|\Illuminate\Support\Collection $healthcareworkers
     *
     * @return bool
     */
    public function hasHealthcareworker($healthcareworkers): bool
    {
        if (is_string($healthcareworkers) && false !== strpos($healthcareworkers, '|')) {
            $healthcareworkers = $this->convertPipeToArray($healthcareworkers);
        }

        if (is_string($healthcareworkers)) {
            return $this->healthcareworkers->contains('name', $healthcareworkers);
        }

        if ($healthcareworkers instanceof UserHealthcareworker) {
            return $this->healthcareworkers->contains('id', $healthcareworkers->id);
        }

        if (is_array($healthcareworkers)) {
            foreach ($healthcareworkers as $group) {
                if ($this->hasHealthcareworker($group)) {
                    return true;
                }
            }

            return false;
        }

        return $healthcareworkers->intersect($this->healthcareworkers)->isNotEmpty();
    }

    public function getHealthcareworkerNames(): Collection
    {
        return $this->healthcareworkers->pluck('name');
    }
}
