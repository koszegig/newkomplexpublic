<?php

namespace App\Traits\LaravelGrapHQL\Scalars;

use App\Traits\LaravelGrapHQL\Scalar;

class EnumType implements Scalar
{
    protected $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function __toString()
    {
        return $this->value;
    }

    public static function match($value): bool
    {
        return false;
    }
}
