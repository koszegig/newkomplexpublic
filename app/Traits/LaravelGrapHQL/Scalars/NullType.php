<?php

namespace App\Traits\LaravelGrapHQL\Scalars;

use App\Traits\LaravelGrapHQL\Scalar;


class NullType implements Scalar
{
    public function __toString()
    {
        return 'null';
    }

    public static function match($value): bool
    {
        return $value === null;
    }
}
