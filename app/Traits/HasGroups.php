<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use App\UserGroup;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasGroups
{
	/**
     * A model may have multiple groups.
     */
    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(
            \App\UserGroup::class,
            'user_usergroup',
            'user_id',
            'group_id'
        );
    }

    /**
     * Assign the given group to the model.
     *
     * @param array|string|\App\Contracts\UserGroup ...$groups
     *
     * @return $this
     */
    public function assignGroup(...$groups)
    {
        $groups = collect($groups)
            ->flatten()
            ->map(function ($role) {
                return $this->getStoredGroup($role);
            })
            ->all();

        $this->groups()->saveMany($groups);

        return $this;
    }

      protected function getStoredGroup($group): UserGroup
    {
        if (\MyString::isUUID($group)) {
            return app(UserGroup::class)->findById($group);
        }

        if (is_string($group)) {
            return app(UserGroup::class)->findByName($group);
        }

        return $group;
    }

     /**
     * Revoke the given group from the model.
     *
     * @param string|\App\Contracts\UserGroup $group
     */
    public function removeGroup($group)
    {
        $this->groups()->detach($this->getStoredGroup($group));
    }



    /**
     * Remove all current group and set the given ones.
     *
     * @param array|\App\Contracts\UserGroup|string ...$group
     *
     * @return $this
     */
    public function syncGroups(...$group)
    {
        $this->groups()->detach();

        return $this->assignGroup($group);
    }

    /**
     * Determine if the model has (one of) the given group(s).
     *
     * @param string|array|\App\Contracts\UserGroup|\Illuminate\Support\Collection $groups
     *
     * @return bool
     */
    public function hasGroup($groups): bool
    {
        if (is_string($groups) && false !== strpos($groups, '|')) {
            $groups = $this->convertPipeToArray($groups);
        }

        if (is_string($groups)) {
            return $this->groups->contains('name', $groups);
        }

        if ($groups instanceof UserGroup) {
            return $this->groups->contains('id', $groups->id);
        }

        if (is_array($groups)) {
            foreach ($groups as $group) {
                if ($this->hasGroup($group)) {
                    return true;
                }
            }

            return false;
        }

        return $groups->intersect($this->groups)->isNotEmpty();
    }

    public function getGroupNames(): Collection
    {
        return $this->groups->pluck('name');
    }
}
