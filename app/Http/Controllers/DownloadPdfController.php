<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use App\Enums\InvoiceOperationEnum as operations;
use App\Models\Tables\User;

class DownloadPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $test = SzmSzla::take(30)->orderBy('SZMSZ_L_MOD_DT','desc')->get()->toArray();
        $User = User::all()->toArray();
        \MyArray::toString($User);
    }
    public function env()
    {
        return response()->success(env("TAVNYOMTATASURL"));
    }

    public function downloadpdf(Request $request,$file){
        return $this->komplextavnyomtatas($request,'/downloaddata/'.$file,'not');
    }
}
