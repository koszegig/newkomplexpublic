<?php

namespace App\Http\Controllers;

use App\Models\Types\Tnavtasks1;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;

class DMnavtasksController extends Controller
{
    public function __construct()
    {
        $this->model = App\Models\Types\Tnavtasks1::class;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = [ "nvtsks_id",
                    "nvtsks_tasktype",
                    "nvtsks_start",
                    "nvtsks_lasttick",
                    "nvtsks_stop",
                    "nvtsks_pid",
                    "nvtsks_statuscode",
                    "nvtsks_stopme",
                    "nvtsks_limit",
                    "nvtsks_letre_felh_nev",
                    "nvtsks_letre_dat",
                    "nvtsks_uuid",
                    "nvtsks_cron",
                    "nvtsks_kasszazaraskor",
                    "nvtsks_indulhat"
        ];
        $model = "App\Models\Types\Tnavtasks1";

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[],true,' order by nvtsks_id desc');
        return response()->success($dmdata);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function listprint(Request $request, $size = 200)
    {
        $select = ["nvtsks_id",
            "nvtsks_tasktype",
            "nvtsks_start",
            "nvtsks_lasttick",
            "nvtsks_stop",
            "nvtsks_pid",
            "nvtsks_statuscode",
            "nvtsks_stopme",
            "nvtsks_limit",
            "nvtsks_letre_felh_nev",
            "nvtsks_letre_dat",
            "nvtsks_uuid",
            "nvtsks_cron",
            "nvtsks_kasszazaraskor",
            "nvtsks_indulhat"
        ];
        $model = "App\Models\Types\Tnavtasks1";
//        \MyArray::toStringToLog('User->'.$request->felh_userid);
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[],true,' order by nvtrt_id desc');
        return response()->success($dmdata);
    }
}
