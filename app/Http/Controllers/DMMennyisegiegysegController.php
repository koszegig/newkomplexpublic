<?php

namespace App\Http\Controllers;

use App\DMMennyisegiegyseg;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;
use Validator;
use Log;
class DMMennyisegiegysegController extends Controller
{
    public function __construct(){
      $this->model =  \App\DMMennyisegiegyseg::class;
        $this->dnmodel ='\App\Models\Types\TMennyisegiegysegek1';
        $this->dncol = [
            'me_rovidites as name'
        ];
    }
        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [
        'me_id',
        'me_rovidites',
        'me_nev',
        'me_letre_felh_nev',
        'me_letre_dat',
        'me_aktiv',
        'me_decimal',
        'me_navme_id',
        'me_navme_megnevezes',
        'navme_unitofmeasuretype'
                  ];
      $model = '\App\Models\Types\TMennyisegiegysegek1';
      $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
      return response()->success($dmdata);
    }
    public function onepage($skip='1',$base_macro =' ')
    {
        //
                $dmdata = self::getMasterAll(DMBizonylatfej::MASTERSQL,['&limit' => 1,'&skip' => $skip-1, '&base_macro' => $base_macro]);
         //\Log::debug("mesage" .print_r($dmcikkek,true));
                //$dmcikkek = \DB::select(DMCikkek::MASTERSQL);
                //$dmcikkek = \App\DMMenu::hydrate($dmcikkek);
                //$dmcikkek = \App\DMMenu::hydrate($dmcikkek);
        return response()->success(compact('dmdata'));
    }



}
