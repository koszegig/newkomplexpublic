<?php

namespace App\Http\Controllers;

use App\Models\Tables\DMazonositok;
use App\Models\Tables\DMCommon;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;
use DB;
use Session;
use Auth;
class DMAzonositokController extends Controller
{
    public function __construct()
    {
      $this->model = App\Models\Tables\DMazonositok::class;


    }
/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [
            'azon_id',
            'azon_egyseg_prt_id',
            'azon_prt_nev',
            'azon_biztip_kod',
            'azon_biztip_kod_nev',
            'azon_ev',
            'azon_nyito',
            'azon_kezdo',
            'azon_befejezo',
            'azon_atadott',
            'azon_letre_felh_nev',
            'azon_letre_dat',
            'azon_rovidneve',
            'azon_default',
            'azon_nyilvar_hatas_in',
            'azon_nyilvar_hatas_in_kod_nev',
            'azon_rovidmegjegyzes',
            //'azon_megjegyzes',
            'azon_nyomtatasipeldanyszam',
            'azon_handle',
            'azon_lezart',
            'azon_nullakszama',
            'azon_jovedeki_termek',
            'azon_jovedeki_termek_kod_nev'
                  ];
      $model = '\App\Models\Types\TAzonositok1';
      $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
      return response()->success($dmdata);
    }

    public function onepage(Request $request)
    {
      //$this->_setFilters($request);
       $size = 25;
        $select = [
          'azon_id',
          'azon_egyseg_prt_id',
          'azon_prt_nev',
          'azon_biztip_kod',
          'azon_biztip_kod_nev',
          'azon_ev',
          'azon_nyito',
          'azon_kezdo',
          'azon_befejezo',
          'azon_atadott',
          'azon_letre_felh_nev',
          'azon_letre_dat',
          'azon_rovidneve',
          'azon_default',
          'azon_nyilvar_hatas_in',
          'azon_nyilvar_hatas_in_kod_nev',
          'azon_rovidmegjegyzes',
          //'azon_megjegyzes',
          'azon_nyomtatasipeldanyszam',
          'azon_handle',
          'azon_lezart',
          'azon_nullakszama',
          'azon_jovedeki_termek',
          'azon_jovedeki_termek_kod_nev',
            ' CURRENT_DATE as currentdate ',
            '             now() as currentdatetime'
                ];
    $model = '\App\Models\Types\TAzonositok1';
    $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
        return response()->success(compact('dmdata'));
    }
    public function azonositokByMenuid(Request $request)
    {
       \MyArray::toStringToLog('-azonositokByMenuid-----');
       $size = 25;
       $select = [
         'get_azonositokbymenuid_2'
               ];
       $model = '\App\Models\Types\TAzonositokbymenuid2';
       $params = [ $request->params['felh_userid'],
                   $request->params['menu_id'],
                 ];
       $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,$params);
       return response()->success(compact('dmdata'));

    }
    public function oneUserPage(Request $request)
    //public function oneUserPage()
    {
//        \MyArray::toStringToLog('-oneUserPages-----');
        $size = 25;
        $select = [
          'azon_id',
          'azon_egyseg_prt_id',
          'azon_prt_nev',
          'azon_biztip_kod',
          'azon_biztip_kod_nev',
          'azon_ev',
          'azon_nyito',
          'azon_kezdo',
          'azon_befejezo',
          'azon_atadott',
          'azon_letre_felh_nev',
          'azon_letre_dat',
          'azon_rovidneve',
          'azon_default',
          'azon_nyilvar_hatas_in',
          'azon_nyilvar_hatas_in_kod_nev',
          'azon_rovidmegjegyzes',
          //'azon_megjegyzes',
          'azon_nyomtatasipeldanyszam',
          'azon_handle',
          'azon_lezart',
          'azon_jovedeki_termek',
          'azon_jovedeki_termek_kod_nev',
           ' CURRENT_DATE as currentdate ',
           '             now() as currentdatetime'
                ];
        $model = '\App\Models\Types\TAzonositok2';
        $params = [ $request->params['felh_userid'],
                    $request->params['BIZTIP_KOD'],
                    $request->params['bizonylatok_eve'],
                  ];
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,$params);
  //      \MyArray::toStringToLog('-oneUserPages-----');
        return response()->success(compact('dmdata'));
    }

    public function _setFilters(Request &$request){
      \MyArray::toStringToLog('-$request->params-----');
      \MyArray::toStringToLog($request->params[0]);
      \MyArray::toStringToLog('-----$request->params-');
      $request->filters = [
        [
        'field' => [
          [
            'field' => "base_macro",
            'op' => "=",
            'lop' => "OR"
          ]
        ],
        'fieldtype' => "a-c4",
        "op" => null,
        "lop" => "AND",
        "value" => 'true'
      ],
        [
        'field' => [
          [
            'field' => "azon_biztip_kod",
            'op' => "=",
            'lop' => "OR"
          ]
        ],
        'fieldtype' => "a_kodok_c1",
        "op" => null,
        "lop" => "AND",
        "value" => $request->params[0]['BIZTIP_KOD']
      ],
      [
      'field' => [
        [
          'field' => "azon_ev",
          'op' => "=",
          'lop' => "OR"
        ]
      ],
      'fieldtype' => "a_c4",
      "op" => null,
      "lop" => "AND",
      "value" => $request->params[0]['bizonylatok_eve']
    ]
    ];
    }



}
