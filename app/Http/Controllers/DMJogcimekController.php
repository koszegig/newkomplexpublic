<?php

namespace App\Http\Controllers;


use App\DMJogcimek;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;
class Member extends Model {
    }
class DMJogcimekController extends Controller
{
    public function __construct(){
      $this->model = \App\Models\Tables\DMJogcimek::class;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            "jc_id",
            "jc_azon",
            "jc_nev",
            "jc_relacio_kod",
            "jc_relacio_kod_nev",
            "jc_keszlet_valtozasi_kod",
            "jc_keszlet_valtozasi_kod_nev",
            "jc_nyilvar_hatas_in",
            "jc_nyilvar_hatas_in_kod_nev",
            "jc_kontirozas_kod",
            "jc_kontirozas_kod_nev",
            "jc_kulso_azon",
            "jc_alkalmazott_in",
            "jc_letre_felh_nev",
            "jc_letre_dat",
            "jc_cikkartonba",
            "jc_cikkartonban_visible",
            "jc_rakt_status_ertekesites_kod",
            "jc_rakt_status_ertekesites_kod_nev",

        ];
        $model = '\App\Models\Tables\DMJogcimek';
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
 //       print_r($dmdata);
//                \MyArray::toStringToLog('$dmdata->'.$dmdata);
        return response()->success($dmdata);
    }

    public function onepage(Request $request)
	{
        $select = [
            "jc_id",
            "jc_azon",
            "jc_nev",
            "jc_relacio_kod",
            "jc_relacio_kod_nev",
            "jc_keszlet_valtozasi_kod",
            "jc_keszlet_valtozasi_kod_nev",
            "jc_nyilvar_hatas_in",
            "jc_nyilvar_hatas_in_kod_nev",
            "jc_kontirozas_kod",
            "jc_kontirozas_kod_nev",
            "jc_kulso_azon",
            "jc_alkalmazott_in",
            "jc_letre_felh_nev",
            "jc_letre_dat",
            "jc_cikkartonba",
            "jc_cikkartonban_visible",
            "jc_rakt_status_ertekesites_kod",
            "jc_rakt_status_ertekesites_kod_nev",

        ];
        $model = '\App\Models\Types\TJogcimek2';
        $jc_azon = $request->jc_azon;
        $jc_azon = str_replace("'", '', $jc_azon);
        $jc_azon = intval($jc_azon);;
        //$jc_azon = (float)$jc_azon;
                \MyArray::toStringToLog('jc_azon->'. $jc_azon);
        $dmdata = $this->selectStoredProcedure($model, $request, $select, 25,[ $jc_azon]);
        //\MyArray::toStringToLog('$dmdata->'. $dmdata);
        return response()->success($dmdata);
    }

}

