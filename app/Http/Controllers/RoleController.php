<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Validator;
use App\Role;

class RoleController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'slug' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dmdata = Role::all();

        return response()->success(compact('dmdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($data)
    {
        return Role::create([
            'name' => $data['name'],
            'slug' => $data['slug']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all());
        $role = $this->create($request->all());
        return response()->success($role);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pano  $pano
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = $this->get($id);
        $role->permissions;
        $role->permissionnames = $role->permissions->pluck('name')->toArray();
        return response()->success($role);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pano  $pano
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        return Role::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pano  $pano
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        $role->name = $request->name;
        $role->slug = $request->slug;
        $role->save();
        return response()->success($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pano  $pano
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::destroy($id);
        return response()->success('success');
    }
}
