<?php

namespace App\Http\Controllers;

use App\Models\Tables\DMPartner;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;

class DMPartnerController extends Controller
{
    public function __construct(){
      $this->model = \App\Models\Tables\DMPartner::class;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [
        'prt_id',
        'prt_prt_id',
        'prt_aktiv',
        'prt_nev',
        'prt_elonev',
        'prt_utonev',
        'prt_letre_felh_nev',
        'prt_letre_dat',
        'prt_adoszam',
        'prt_cegjegyzekszam',
        'prt_kozossegiszam',
        'prt_cegkod',
        'prt_ugynok_prt_id',
        'prt_ugynok_prt_nev',
        'prt_partner_tipus_kod',
        'prt_partner_tipus_kod_nev',
        'prt_fztm_id',
        'prt_fztm_megnevezes',
        'prt_szamlazasi_limit',
        'prt_artbl_arkategoria_kod',
        'prt_artbl_arkategoria_kod_nev',
        'prt_utalasi_nap',
        'prt_szallito_vevo_tipus_kod',
        'prt_szallito_vevo_tipus_kod_nev',
        'prt_tiltas_kod',
        'prt_tiltas_kod_nev',
        'prt_ujsag_kod',
        'prt_ujsag_kod_nev',
        'prt_szallitasi_koltseg',
        'prt_visszateritesi_koltseg',
        'prt_gln_kod_1',
        'prt_gln_kod_2',
        'prt_eu_afa_reg_szam',
        'prt_afa_csoport_azonosito',
        'prt_jovedeki_engedelyszam',
        'prt_mokudesi_engedely_szam',
        'prt_handle',
        //'prt_megjegyzes',
        'prt_egyenikedvezmeny',
        'prt_programothasznalo',
        'prt_ckcsmf_id',
        'prt_ckcsmf_nev',
        'prt_alap_teljescime',
        'prt_bsz_bankszamlaszam',
        'prt_prtelhtsg_mobilszam',
        'prt_forditottafa',
        'prt_forditottafa_kod_nev',
        'prt_karbegy_darabszam',
        'prt_kapjonlevelet_kod',
        'prt_kapjonlevelet_kod_nev',
        'prt_beveteli_kedvezmeny',
        'prt_default_biztip_kod',
        'prt_default_biztip_kod_nev',
        'prt_szamlazasi_egyszeri_limit',
        'prt_prt_nev',
        'prt_prt_adoszam',
        'prt_prt_cegjegyzekszam',
        'prt_vipd',
        'prt_nebih',
        'prt_kisadozo',
        'prt_kisadozo_kod_nev',
        'prt_egyeni_vallalkozo',
        'prt_egyeni_vallalkozo_kod_nev',
        'prt_penzforgelsz',
        'prt_penzforgelsz_kod_nev',
        'prt_neta',
        'prt_neta_kod_nev',
        'prt_ev_neve',
        'prt_adoazonosito_jel',
        'prt_adoszam_helyes',
        'prt_adoszam_helyes_kod_nev',
        'prt_adoszam_ellenorzes',
        'prt_prtkedv_sum_netto_ert',
        'prt_prtkedv_sum_brutto_ert',
        'prt_prtelhtsg_emailcim',
        'prt_prtelhtsg_weboldal',
        'prt_uuid',
                        ];
      $model = '\App\Models\Types\TPartnerek1';
      $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
      return response()->success($dmdata);
    }
    /* public function index()
    {
         $dmdata = self::getMasterAll(DMPartner::MASTERSQL,['&limit' => 25,'&skip' => 0]);
        return response()->success(compact('dmdata'));

    }*/
    public function onepage($skip='1',$base_macro =' ')
    {
        //
       // $dmdata = self::getMasterAll(DMPartner::MASTERSQL,['&limit' => 25,'&skip' => $skip-1, '&base_macro' => $base_macro]);
       // return response()->success(compact('dmdata'));
    }

}
