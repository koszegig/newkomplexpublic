<?php

namespace App\Http\Controllers;

use App\Models\Tables\DMmenuparameterek;
use Illuminate\Http\Request;
use App\Libraries\Database\QueryBuilderSP;
use Illuminate\Auth\AuthenticationException;
class DMmenuparameterekController extends Controller
{
	    //
    public function getMenuparameterek($menu_id)
    {
        $typeModel = '\App\Models\Types\TMenuparameterek';
        $typeSelect = [
            'menuprm_name',
            'menuprm_value',
        ];
            $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$menu_id]);
            $dmmenuparameterek =  $builder->get();
        return response()->success(compact('dmmenuparameterek'));
    }


}
