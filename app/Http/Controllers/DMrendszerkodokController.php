<?php

namespace App\Http\Controllers;

use App\Models\Tables\TRendszerKodok1;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;

class DMrendszerkodokController extends Controller
{
    protected $kodmezo;
    public function __construct(){
        $this->model = \App\Models\Tables\DMfizetesimodok::class;
        $this->dnmodel ='\App\Models\Types\TRendszerKodok1';
        $this->dncol = [
            'kod_kod as id',
            'kod_kod as kod',
            'kod_nev as name'
        ];
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            "kodf_id",
                "kodf_fgl_id",
                "kod_mezo",
                "kod_mezo",
                "kod_mezo_leiras",
                "kod_id",
                "kod_fgl_id",
                "kod_kod",
                "kod_nev",
                "kod_nev_leiras",
                "kod_elo_nev",
                "kod_uto_nev",
                "kod_hosszu_nev"
        ];
        $model = '\App\Models\Types\TRendszerKodok1';
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
        return response()->success($dmdata);
    }
    public function listForDn(Request $request)
    {
        $this->kodmezo =$request->kodmezo;
        $dmdata = $this->getDnData($request->all());
        return response()->success($dmdata);
    }

    protected function getDnData($condition)
    {
        $dmdata = $this->selectStoredDnProcedure($this->dnmodel, $this->dncol,null,[$this->kodmezo]);
        return $dmdata;
    }
}
