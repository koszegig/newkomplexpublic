<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExportcikkekController extends Controller
{
    public function __construct()
    {
        $this->model = App\Models\Types\TKulcsszoftCikkek1::class;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = ['sorszam',
            'kod',
            'megnevezes',
            'afakod',
            'afaszazlek',
            'ar6',
            'ar5',
            'ar4',
            'ar3',
            'ar2',
            'ar1',
            'egyseg',
            'csopkod',
            'csoport',
            'gyartokod',
            'gyartonev',
            'idegennev',
            'idegenmerteke',
            'tkodgyartonal',
            'besorolas',
            'vonalkod',
            'vtsz',
            'csoport2',
            'csoport3',
            'mennyiseg',
            'tipus',
            'jovedekitermek',
            'keszletmod',
            'szavidos',
            'kiszereles',
            'alkoholfok',
            'forditottafa',
            'uuid '
        ];
        $model = "App\Models\Types\TKulcsszoftCikkek1";
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
}
