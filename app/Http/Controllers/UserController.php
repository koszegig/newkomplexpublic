<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Validator;
use App\Models\Tables\User;

class UserController extends Controller
{

  public function __construct()
  { }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'firstname' => 'required|string|max:255',
      'lastname' => 'required|string|max:255',
      'email' => 'required|string|email|max:255',
    ]);
  }

  /**
   * Get all users.
   *
   * @return JSON
   */
  public function index()
  {
    $dmdata = User::where('system', false)->get();
    return response()->success(compact('dmdata'));
  }


  /**
   * Get user current context.
   *
   * @return JSON
   */
  public function getMe()
  {
    return response()->success(\MyUser::getme());
  }

  /**
   * Update user current context.
   *
   * @return JSON success message
   */
  public function putMe(Request $request)
  {
    $this->validator($request->all())->validate();
    $user = \MyUser::getCurrentUser();
    $user->firstname = $request->firstname;
    $user->lastname = $request->lastname;
    $user->email = $request->email;
    $user->save();
    return response()->success($user);
  }

  /**
   * Get user details referenced by id.
   *
   * @param string User id
   *
   * @return JSON
   */
  public function show($id)
  {
    $user = $this->getUser($id);
    $user->getGroupNames();
    return response()->success($user);
  }

  public function getUser($id)
  {
    return  $user = User::find($id);
  }

  /**
   * Get system user.
   *
   * @param string User id
   *
   * @return JSON
   */
  public function getSystem()
  {
    $user = $this->selectSystemUser();
    return response()->success($user);
  }

  public function selectUser($id)
  {
    return $this->getUser($id);
  }

  public function selectSystemUser()
  {
    return User::where('system', true)->first();
  }

  /**
   * Update user data.
   *
   * @return JSON success message
   */
  public function update(Request $request, $id)
  {
    $user = User::find($request->id);
    $user->firstname = $request->firstname;
    $user->lastname = $request->lastname;
    $user->username = $request->username;
    $user->save();
    $user->updateEmail($request->primary_email);
    $user->syncGroups($request->groups);
    return response()->success($user);
  }


  /**
   * Create new user.
   *
   * @return JSON
   */
  public function store(Request $request)
  {
    $this->validator($request->all());
    $user = $this->create($request->all());
    $user->addEmail(['email' => $request->email, 'primary' => true]);
    $user->assignGroup($request->groups);
    return response()->success($user);
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return \App\User
   */
  public function create($data)
  {

    $token = \MyUser::updatePwToken();
    $data['password_reset_token'] = $token['password_reset_token'];
    $data['password_reset_token_expired'] = $token['password_reset_token_expired'];
    $data['remember_token'] = \MyString::generateTokenString();
    array_forget($data, 'email');
    array_forget($data, 'groups');
    $user = User::create($data);
    return $user;
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function activate($id)
  {
    return User::find($id)->setAsActive();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function inactivate($id)
  {
    return User::find($id)->setAsInActive();
  }

  /**
   * Update user data only password information.
   *
   * @return JSON success message
   */
  public function refresh_token()
  {
    $token = \MyUser::refreshToken();
    return response()->success($token);
  }
}
