<?php

namespace App\Http\Controllers;

use App\Models\Types\Tmunkahelyek1;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;


class DMmunkahelyekController extends Controller
{
    /**
     * DMmunkahelyekController constructor.
     */
    public function __construct()
    {
        $this->model = App\Models\Types\Tmunkahelyek1::class;
    }

    /**
     * @param Request $request
     * @param int $size
     * @return mixed
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            "munhely_id",
            "munhely_prt_id",
            "munhely_prt_nev",
            "munhely_megnevezes",
            "munhely_cim",
            "munhely_prt_cim_id",
            "munhely_prt_teljescim",
            "munhely_limit",
            "munhely_handle",
            "munhely_letre_felh_nev",
            "munhely_letre_dat",
            "munhely_prtelhtsg_id",
            "munhely_prtelhtsg_mobilszam",
            "munhely_default",
        ];
        $model = "App\Models\Types\Tmunkahelyek1";

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function listprint(Request $request, $size = 200)
    {
        $select = [
            "munhely_id",
            "munhely_prt_id",
            "munhely_prt_nev",
            "munhely_megnevezes",
            "munhely_cim",
            "munhely_prt_cim_id",
            "munhely_prt_teljescim",
            "munhely_limit",
            "munhely_handle",
            "munhely_letre_felh_nev",
            "munhely_letre_dat",
            "munhely_prtelhtsg_id",
            "munhely_prtelhtsg_mobilszam",
            "munhely_default",
        ];
        $model = "App\Models\Types\Tmunkahelyek1";
//        \MyArray::toStringToLog('User->'.$request->felh_userid);
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
}
