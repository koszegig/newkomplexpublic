<?php

namespace App\Http\Controllers;


use App\Models\Tables\DMPenznem;
use App\Models\Tables\DMCommon;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;
class DMPenznemController extends Controller
{
	    //
    public function __construct(){
      $this->model = App\Models\Tables\DMPenznem::class;
        $this->dnmodel ='\App\Models\Types\TPenznem1';
        $this->dncol = [
            'pnzn_nev as name'
        ];
    }
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [
        'pnzn_id',
        'pnzn_rovidites',
        'pnzn_nev',
        'pnzn_letre_felh_nev',
        'pnzn_letre_dat',
        'pnzn_aktiv',
        'pnzn_default',
        'pnzn_kerekit',
        'pnzn_szamlaszam',
        'pnzn_jel',
        'pnzn_kerekitesmerteke',
        'pnzn_uuid'
                  ];
      $dmdata = $this->selectStoredProcedure($this->dnmodel, $request, $select, $size);
      return response()->success($dmdata);
    }
    public function onepage($base_macro =' ')
	{
        //
        $dmdata = self::getMasterAll(DMPenznem::MASTERSQL,['&base_macro' => 'where 1=1 '.$base_macro]);
        return response()->success(compact('dmdata'));
    }



}
