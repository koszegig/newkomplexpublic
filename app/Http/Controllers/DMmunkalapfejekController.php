<?php

namespace App\Http\Controllers;

use App\Models\Types\Tmunkalapfejek1;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;

class DMmunkalapfejekController extends Controller
{
    /**
     * DMmunkahelyekController constructor.
     */
    public function __construct()
    {
        $this->model = App\Models\Types\Tmunkalapfejek1::class;
    }

    /**
     * @param Request $request
     * @param int $size
     * @return mixed
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            "mulapf_id",
            "mulapf_mnklpazon_id",
            "mulapf_munhely_id",
            "mulapf_munhely_megnevezes",
            "mulapf_munhely_cim",
            "mulapf_munkaszam",
            "mulapf_rogzito_felh_nev",
            "mulapf_rogz_dat",
            "mulapf_letre_felh_nev",
            "mulapf_letre_dat",
            "mulapf_feldolg_status_kod",
            "mulapf_kezdes",
            "mulapf_befejezes",
         //   "mulapf_megjegyzes",
            "mulapf_feldolg_status_kod_nev",
            "mulapf_megnevezese",
            "mulapf_munhely_prt_id",
            "mulapf_munhely_prt_nev",
            "mulapf_karbegy_id",
            "mulapf_karbegy_nev",
            "mulapf_csatolt_munkalap",
            "mulapf_fajta_id",
            "mnkfjt_nev",
            "mulapf_limit"
        ];
        $model = "App\Models\Types\Tmunkalapfejek1";

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function listprint(Request $request, $size = 200)
    {
        $select = [
            "mulapf_id",
            "mulapf_mnklpazon_id",
            "mulapf_munhely_id",
            "mulapf_munhely_megnevezes",
            "mulapf_munhely_cim",
            "mulapf_munkaszam",
            "mulapf_rogzito_felh_nev",
            "mulapf_rogz_dat",
            "mulapf_letre_felh_nev",
            "mulapf_letre_dat",
            "mulapf_feldolg_status_kod",
            "mulapf_kezdes",
            "mulapf_befejezes",
            "mulapf_megjegyzes",
            "mulapf_feldolg_status_kod_nev",
            "mulapf_megnevezese",
            "mulapf_munhely_prt_id",
            "mulapf_munhely_prt_nev",
            "mulapf_karbegy_id",
            "mulapf_karbegy_nev",
            "mulapf_csatolt_munkalap",
            "mulapf_fajta_id",
            "mnkfjt_nev",
            "mulapf_limit"
        ];
        $model = "App\Models\Types\Tmunkalapfejek1";
//        \MyArray::toStringToLog('User->'.$request->felh_userid);
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
}
