<?php

namespace App\Http\Controllers;


use App\DMPartnerBankszamlaszamok;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;
class DMPartnerBankszamlaszamokController extends Controller
{
	    //
    public function __construct(){
      $this->model = \App\DMPartnerBankszamlaszamok::class;
    }
    public function list(Request $request, $size = 25)
    {
        $select = [
            'bsz_id',
            'bsz_prt_id',
            'bsz_pfjszk',
            'bsz_bankszamlaszam',
            'bsz_atfutasi_napok',
            'bsz_iban',
            'bsz_letre_felh_nev',
            'bsz_letre_dat',
            'bsz_tipus_kod',
            'bsz_bank_id',
            'bsz_valasztott',
            'bsz_uuid'
        ];
        $model = '\App\Models\Types\TBankszamlaszamok2';
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
        return response()->success($dmdata);
    }

    public function onepage(Request $request)
	{
         $size = 25;
        $select = [
            'bsz_id',
            'bsz_prt_id',
            'bsz_pfjszk',
            'bsz_bankszamlaszam',
            'bsz_atfutasi_napok',
            'bsz_iban',
            'bsz_letre_felh_nev',
            'bsz_letre_dat',
            'bsz_tipus_kod',
            'bsz_bank_id',
            'bsz_valasztott',
            'bsz_uuid'
        ];
        $model = '\App\Models\Types\TBankszamlaszamok2';
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
        return response()->success(compact('dmdata'));
    }



}
