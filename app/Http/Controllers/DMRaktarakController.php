<?php

namespace App\Http\Controllers;


use App\Models\Tables\DMRaktarak;
use App\Models\Tables\DMCommon;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;


class DMRaktarakController extends Controller
{
    public function __construct(){
        $this->model = App\Models\Tables\DMRaktarak::class;
        $this->dnmodel ='\App\Models\Types\TRaktarak1';
        $this->dncol = [
            'rakt_nev as name'
        ];
      }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [
        'rakt_id',
        'rakt_prt_id',
        'rakt_prt_nev',
        'rakt_nev',
        'rakt_letre_felh_nev',
        'rakt_letre_dat',
        'rakt_aktiv',
        'rakt_jovedeki_termek',
        'rakt_jovedeki_termek_kod_nev',
        'rakt_rendezo',
        'rakt_status_ertekesites_kod',
        'rakt_status_ertekesites_kod_nev',
        'rakt_uuid'
                  ];
      $dmdata = $this->selectStoredProcedure($this->dnmodel, $request, $select, $size);
      return response()->success($dmdata);
    }
    public function onepage($skip='1',$base_macro =' ')
	{
        //
        $dmdata = self::getMasterAll(DMRaktarak::MASTERSQL,['&limit' => 25,'&skip' => $skip-1, '&base_macro' => $base_macro]);
        return response()->success(compact('dmdata'));
    }

 public static function getLookUpRaktarak(Request $request)
    {
        $result = self::RESULT_DEFAULT;
        $param = '';
        if (isset(Input::all()['jovedekitermek'])) {
            $param = " and rakt_jovedeki_termek = '".Input::all()['jovedekitermek']."'";
        }

        foreach(DMRaktarak::quRaktarak($param) as $row) {
            $result['data'][] = ['rakt_id' => $row['rakt_id'],
                                 'rakt_nev' => $row['rakt_nev']];
        }
        $result['isSuccess'] = true;
           // Log::info("mesage" .print_r($result,true));
        return response()->json($result);
    }
}
