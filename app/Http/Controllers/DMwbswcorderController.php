<?php

namespace App\Http\Controllers;
use App\Models\Types\Twbswcorderrecord1;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;

class DMwbswcorderController extends Controller
{
    public function __construct()
    {
        $this->model = App\Models\Types\Twbswcorderrecord1::class;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = ["wbswco_id",
                   "wbswco_org_id",
                    "wbswco_order_number",
                    "wbswco_status",
                    "wbswco_currency",
                    "wbswco_total",
                    "wbswco_subtotal",
                    "wbswco_total_tax",
                    "wbswco_total_shipping",
                    "wbswco_cart_tax",
                    "wbswco_shipping_tax",
                    "wbswco_shipping_methods",
                    "wbswco_payment_method_id",
                    "wbswco_payment_method_title",
                    "wbswco_payment_paid",
                    "wbswco_note",
                    "wbswco_customer_ip",
                    "wbswco_customer_org_id",
                    "wbswco_bizf_id",
                    "wbswco_bizf_azon",
                    "wbswco_modosult",
                    "wbswco_letre_felh_nev",
                    "wbswco_letre_dat",
                    "wbswco_uuid",
                    "wbswco_wbswcob_id",
                    "wbswco_wbswcob_wbswco_id",
                    "wbswco_wbswcob_org_id",
                    "wbswco_wbswcob_first_name",
                    "wbswco_wbswcob_last_name",
                    "wbswco_wbswcob_company",
                    "wbswco_wbswcob_address_1",
                    "wbswco_wbswcob_address_2",
                    "wbswco_wbswcob_city",
                    "wbswco_wbswcob_state",
                    "wbswco_wbswcob_postcode",
                    "wbswco_wbswcob_country",
                    "wbswco_wbswcob_email",
                    "wbswco_wbswcob_phone",
                    "wbswco_wbswcob_uuid",
                    "wbswco_wbswcos_id",
                    "wbswco_wbswcos_wbswco_id",
                    "wbswco_wbswcos_org_id",
                    "wbswco_wbswcos_first_name",
                    "wbswco_wbswcos_last_name",
                    "wbswco_wbswcos_company",
                    "wbswco_wbswcos_address_1",
                    "wbswco_wbswcos_address_2",
                    "wbswco_wbswcos_city",
                    "wbswco_wbswcos_state",
                    "wbswco_wbswcos_postcode",
                    "wbswco_wbswcos_country",
                    "wbswco_wbswcos_uuid"
        ];
        $model = "App\Models\Types\Twbswcorderrecord1";

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function listprint(Request $request, $size = 200)
    {
        $select = ["wbswco_id",
            "wbswco_org_id",
            "wbswco_order_number",
            "wbswco_status",
            "wbswco_currency",
            "wbswco_total",
            "wbswco_subtotal",
            "wbswco_total_tax",
            "wbswco_total_shipping",
            "wbswco_cart_tax",
            "wbswco_shipping_tax",
            "wbswco_shipping_methods",
            "wbswco_payment_method_id",
            "wbswco_payment_method_title",
            "wbswco_payment_paid",
            "wbswco_note",
            "wbswco_customer_ip",
            "wbswco_customer_org_id",
            "wbswco_bizf_id",
            "wbswco_bizf_azon",
            "wbswco_modosult",
            "wbswco_letre_felh_nev",
            "wbswco_letre_dat",
            "wbswco_uuid",
            "wbswco_wbswcob_id",
            "wbswco_wbswcob_wbswco_id",
            "wbswco_wbswcob_org_id",
            "wbswco_wbswcob_first_name",
            "wbswco_wbswcob_last_name",
            "wbswco_wbswcob_company",
            "wbswco_wbswcob_address_1",
            "wbswco_wbswcob_address_2",
            "wbswco_wbswcob_city",
            "wbswco_wbswcob_state",
            "wbswco_wbswcob_postcode",
            "wbswco_wbswcob_country",
            "wbswco_wbswcob_email",
            "wbswco_wbswcob_phone",
            "wbswco_wbswcob_uuid",
            "wbswco_wbswcos_id",
            "wbswco_wbswcos_wbswco_id",
            "wbswco_wbswcos_org_id",
            "wbswco_wbswcos_first_name",
            "wbswco_wbswcos_last_name",
            "wbswco_wbswcos_company",
            "wbswco_wbswcos_address_1",
            "wbswco_wbswcos_address_2",
            "wbswco_wbswcos_city",
            "wbswco_wbswcos_state",
            "wbswco_wbswcos_postcode",
            "wbswco_wbswcos_country",
            "wbswco_wbswcos_uuid"
        ];
        $model = "App\Models\Types\Twbswcorderrecord1";
//        \MyArray::toStringToLog('User->'.$request->felh_userid);
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }

}
