<?php

namespace App\Http\Controllers;

use App\Models\Tables\DMBizonylattetelek;
use Illuminate\Database\Eloquent\Model;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Arr;
use Spatie\QueryBuilder\Filter;
use Validator;
use Log;
class DMBizonylattetelekController extends Controller
{    public $typeSelect = [
    'bizt_id',
    'bizt_bizf_id ',
    'bizt_bizf_id as bizf_id',
    'bizt_bizf_azon ',
    'bizt_cik_id',
    'bizt_cik_kod ',
    'bizt_cik_nev',
    'bizt_mnklp_tetel_id',
    'bizt_bizf_szlev_id',
    'bizt_bizf_szlev_szam',
    'bizt_jc_id',
    'bizt_jc_nev',
    'bizt_feldolg_status_kod',
    'bizt_feldolg_status_kod_nev',
    'bizt_afa_id',
    'bizt_afa_kod',
    'bizt_tet_sorszam',
    'bizt_konyv_datum',
    'round(bizt_menny,2)::public.a_n_3_2 as bizt_menny',
    'bizt_me_id',
    'bizt_me_nev',
    'bizt_nyilv_ar',
    'bizt_alap_ar',
    'bizt_elad_ar',
    'bizt_fogy_ar',
    'bizt_szav_datum',
    'bizt_artbl_arkategoria_kod_kod',
    'bizt_artbl_arkategoria_kod_nev',
    //   'bizt_megjegyzes',
    'bizt_rakt_id',
    'bizt_rakt_nev',
    'bizt_letre_felh_nev',
    'bizt_letre_dat',
    'bizt_brutto_nyilv_ar',
    'bizt_brutto_alap_ar',
    'bizt_brutto_elad_ar',
    'bizt_brutto_fogy_ar',
    'bizt_prt_egyenikedvezmeny',
    'bizt_me_rovidites',
    'bizt_akcios_ar',
    'bizt_akcios_ar_kod_nev',
    'bizt_afaertek',
    'bizt_cik_egyseg_ar',
    'bizt_cik_brutto_egyseg_ar',
    'bizt_cik_beszerzesi_ar',
    'bizt_cik_bruttto_beszerzesi_ar',
    'bizt_visszavett_menny',
    'bizt_cgy_id',
    'bizt_cgy_gyariszam',
    'bizt_beszerzesi_ar',
    'bizt_beszerzesi_brutto_ar',
    'bizt_beszerzesi_afaertek',
    'bizt_forditottafa',
    'bizt_forditottafa_kod_nev',
    'bizt_sulymennyiseg',
    'bizt_cik_suly_me_id',
    'bizt_me_suly_rovidites',
    'bizt_arazott_menny',
    'bizt_armeny_szorzo',
    'bizt_armeny_me_id',
    'bizt_cikk_vtsz_szam',
    'bizt_jc_relacio_kod',
    'bizt_jc_relacio_kod_nev',
    'bizt_jc_keszlet_valtozasi_kod',
    'bizt_rendelt_mennyiseg',
    'bizt_eladott_mennyiseg',
    'bizt_cik_fixaras'
];
    public $typeModel = '\App\Models\Types\TBiztetelek6Web';
     public $bizf_id= 0;
      public function __construct(){
      $this->model =  \App\Models\Tables\DMBizonylattetelek::class;
    }
    /*public function getDbData($data){
        return [
            'bizt_bizf_id' => $data['bizt_bizf_id'],
            'bizt_cik_id' => $data['bizt_cik_id'],
            'bizt_tetel_kod' => $data['bizt_tetel_kod'] ?? null,
            'bizt_tetel_megnevezes' => $data['bizt_tetel_megnevezes'],
            'bizt_me_id' => $data['bizt_me_id'] ?? '',
            'bizt_feldolg_status_kod' => $data['bizt_feldolg_status_kod'] ?? '',
            'bizt_artbl_arkategoria_kod_kod' => $data['bizt_artbl_arkategoria_kod_kod'],
            'bizt_rakt_id' => $data['bizt_rakt_id'],
            'bizt_afa_id' => $data['bizt_afa_id'],
            'bizt_akcios_ar' =>  $data['bizt_akcios_ar'] ?? 0 ,
            'bizt_jc_id' => $data['bizt_jc_id'] ?? 0 ,
            'bizt_forditottafa' => $data['bizt_forditottafa'] ?? 0 ,
            'bizt_menny' => $data['bizt_menny'] ?? 0 ,
            'bizt_nyilv_ar' => $data['bizt_nyilv_ar'] ?? 0 ,
            'bizt_fogy_ar' => $data['bizt_fogy_ar'] ?? 0 ,
            'bizt_cik_egyseg_ar' => $data['bizt_cik_egyseg_ar'] ?? '',
            'bizt_vonal_azon' => $data['bizt_vonal_azon'],
            'bizt_cik_beszerzesi_ar' =>  $data['bizt_cik_beszerzesi_ar'] ?? 'N',
            'bizt_alap_ar' => $data['bizt_alap_ar'] ?? 0 ,
            'bizt_elad_ar' => $data['bizt_elad_ar'] ?? 0 ,
            'bizt_armeny_szorzo' => $data['bizt_armeny_szorzo'] ?? 0 ,
            'bizt_armeny_me_id' => $data['bizt_armeny_me_id'],
            'bizt_prt_egyenikedvezmeny' => $data['bizt_prt_egyenikedvezmeny'],
        ];
    }*/

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
        ]);
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $biztetel = $this->get($id)[0];
        $sql = 'delete from "04_rak".biz_tetelek where bizt_id = '.$id;
        \DB::select($sql);
        $this->model::destroy($id);
        DMBizonylattetelek::bizonylattetlekrendberakasa($biztetel->bizt_bizf_id);
        DMBizonylattetelek::bizonylatfejujraszamolasa($biztetel->bizt_bizf_id);
        return response()->success('success');
    }
    public function postStore($dmdata,$reques){
        //return $this->show($dmdata->id);
          //$model = new $this->model();
          //$propid = $model->getKeyName();

         DMBizonylattetelek::bizonylattetlekrendberakasa($dmdata->bizt_bizf_id);
          DMBizonylattetelek::bizonylatfejujraszamolasa($dmdata->bizt_bizf_id);
          parent::postStore($dmdata,$reques);
        //return $this->show($dmdata->$propid);
    }
    public function postUpdate($id, $reques)
    {
        $biztetel = $this->get($id)[0];
        DMBizonylattetelek::bizonylattetlekrendberakasa($biztetel->bizt_bizf_id);
        DMBizonylattetelek::bizonylatfejujraszamolasa($biztetel->bizt_bizf_id);
        return $this->show($id);
    }

}
