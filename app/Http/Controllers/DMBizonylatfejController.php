<?php

namespace App\Http\Controllers;

use App\Models\Tables\DMBizonylatfej;
//use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;
use App\Helpers\UserHelpers;
use Validator;
use Log;
class DMBizonylatfejController extends Controller
{
    public $typeSelect = [
        "bizf_id",
        "bizf_azon",
        "bizf_azon_id",
        "bizf_azon_egyseg_prt_id",
        "bizf_azon_prt_nev",
        "bizf_azon_biztip_kod",
        "bizf_azon_biztip_kod_nev",
        "bizf_esed_datum",
        "bizf_azon_ev",
        "bizf_atado_prt_id",
        "bizf_atado_prt_nev",
        "bizf_atado_prt_adoszam",
        "bizf_atado_prt_cim_orsz_nev",
        "bizf_atado_prt_cim_helys_megye",
        "bizf_atado_prt_cim_helys_irsz",
        "bizf_atado_prt_cim_helys_nev",
        "bizf_atado_prt_cim_kozterulet_jelleg_kod_nev",
        "bizf_atado_prt_cim_kozterulet_nev",
        "bizf_atado_prt_cim_hazszam",
        "bizf_atado_prt_cim_epulet",
        "bizf_atado_prt_cim_emelet",
        "bizf_atado_prt_cim_lepcsohaz",
        "bizf_atado_prt_cim_ajto",
        "bizf_atado_prt_bsz_bankszamlaszam",
        "bizf_atvevo_prt_id",
        "bizf_atvevo_prt_nev",
        "bizf_atvevo_prt_cim_orsz_nev",
        "bizf_atvevo_prt_cim_helys_megye",
        "bizf_atvevo_prt_cim_helys_irsz",
        "bizf_atvevo_prt_cim_helys_nev",
        "bizf_atvevo_prt_cim_kozterulet_jelleg_kod_nev",
        "bizf_atvevo_prt_cim_kozterulet_nev",
        "bizf_atvevo_prt_cim_hazszam",
        "bizf_atvevo_prt_cim_epulet",
        "bizf_atvevo_prt_cim_lepcsohaz",
        "bizf_atvevo_prt_cim_emelet",
        "bizf_atvevo_prt_cim_ajto",
        "bizf_atvevo_prt_bsz_bankszamlaszam",
        "bizf_atvevo_prt_adoszam",
        "bizf_ukoto_prt_id",
        "bizf_ukoto_prt_nev",
        "bizf_fizeto_prt_id",
        "bizf_fizeto_prt_nev",
        "bizf_fuv_prt_id",
        "bizf_fuv_prt_nev",
        "bizf_penznem_id",
        "bizf_rogziteshelye_kod",
        "bizf_rogziteshelye_kod_nev",
        "bizf_elorogzites_kod",
        "bizf_elorogzites_kod_nev",
        "bizf_fztm_id"
    ];
    public $typeModel = '\App\Models\Types\TBizfejek1';
    public function __construct(){
      $this->model =  \App\Models\Tables\DMBizonylatfej::class;
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function getDbData($data){
        return [
                  'bizf_azon_id' => $data['bizf_azon_id'],
                  'bizf_atado_prt_id' => $data['bizf_atado_prt_id'],
                  'bizf_atvevo_prt_id' => $data['bizf_atvevo_prt_id'],
                  'bizf_penznem_id' => $data['bizf_penznem_id'],
                  'bizf_feldolg_status_kod' => '0',
                  'bizf_szlev_szam' => $data['bizf_szlev_szam'] ?? '',
                  'bizf_szla_szam' => $data['izf_szla_szam'] ?? '',
                  'bizf_telj_datum' => $data['bizf_telj_datum'],
                  'bizf_esed_datum' => $data['bizf_esed_datum'],
                  'bizf_kif_datuma' => $data['bizf_kif_datuma'],
                  'bizf_rend_ert' =>  $data['bizf_rend_ert'] ?? 0 ,
                  'bizf_nyilv_ert' => $data['bizf_nyilv_ert'] ?? 0 ,
                  'bizf_netto_ert' => $data['bizf_netto_ert'] ?? 0 ,
                  'bizf_brutto_ert' => $data['bizf_brutto_ert'] ?? 0 ,
                  'bizf_tetel_szam' => $data['bizf_tetel_szam'] ?? 0 ,
                  'bizf_rendelt_tetel_szam' => $data['bizf_rendelt_tetel_szam'] ?? 0 ,
                  'bizf_megjegyzes' => $data['bizf_megjegyzes'] ?? '',
                  'bizf_fztm_id' => $data['bizf_fztm_id'],
                  'bizf_eredeti_kinyomtatva' =>  $data['bizf_eredeti_kinyomtatva'] ?? 'N',
                  'bizf_masolat_peldanyszama' => $data['bizf_masolat_peldanyszama'] ?? 0 ,
                  'bizf_masodlat_peldanyszama' => $data['bizf_masodlat_peldanyszama'] ?? 0 ,
                  'bizf_nyomtatas_szam' => $data['bizf_nyomtatas_szam'] ?? 0 ,
                  'bizf_rakt_id' => $data['bizf_rakt_id'],
                  'bizf_kszk_id' => $data['bizf_kszk_id'],
                  'bizf_pnztr_id' => $data['bizf_pnztr_id'],
                  'bizf_rovidmegjegyzes' => $data['bizf_rovidmegjegyzes'] ?? '',
                  'bizf_atado_prt_cim_id' => $data['bizf_atado_prt_cim_id'],
                  'bizf_atvevo_prt_cim_id' => $data['bizf_atvevo_prt_cim_id'],
                  'bizf_atado_bsz_id' => $data['bizf_atado_bsz_id'] ??  null,
                  'bizf_atvevo_bsz_id' => $data['bizf_atvevo_bsz_id'] ??  null,
                  'bizf_atvevo_szalitasicim_prt_cim_id' => $data['bizf_atvevo_szalitasicim_prt_cim_id'] ?? $data['bizf_atvevo_prt_cim_id'],
                  'bizf_arfolyam' => $data['bizf_arfolyam'] ?? 1,
                  'bizf_mulapf_id' => $data['bizf_mulapf_id'] ?? null,
                  'bizf_bizonylatkep' => $data['bizf_bizonylatkep'] ?? '',
                  'bizf_netto_beszerzesi_ert' => $data['bizf_netto_beszerzesi_ert'] ?? 0,
                  'bizf_brutto_beszerzesi_ert' => $data['bizf_brutto_beszerzesi_ert'] ?? 0,
                  'bizf_kifizetendo_beszerzesi_ert' => $data['bizf_kifizetendo_beszerzesi_ert'] ?? 0,
                  'bizf_fkod_id' => $data['bizf_fkod_id'],
                  'bizf_ervenyeseg' => $data['bizf_ervenyeseg'] ?? null,
                  'bizf_afa_id' => $data['bizf_afa_id'] ?? null,
                  'bizf_forgalom_tipusa' => $data['bizf_forgalom_tipusa'] ?? null,
                  'bizf_tko_okmanyszam' => $data['bizf_tko_okmanyszam'] ?? null,
                  'bizf_eko_okmanyszam' => $data['bizf_eko_okmanyszam'] ?? null,
                  'bizf_rogziteshelye_kod' => '1',
                  'bizf_elorogzites_kod' => $data['bizf_elorogzites_kod'] ?? 'N'
        ];
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /*protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
        ]);
    }*/
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            'bizf_id',
            'bizf_azon',
            'bizf_azon_id',
            'bizf_azon_egyseg_prt_id',
            'bizf_azon_prt_nev',
            'bizf_azon_biztip_kod',
            'bizf_azon_biztip_kod_nev',
            'bizf_azon_ev',
            'bizf_atado_prt_id',
            'bizf_atado_prt_nev',
            'bizf_atado_prt_adoszam',
            'bizf_atado_prt_cim_orsz_nev',
            'bizf_atado_prt_cim_helys_megye',
            'bizf_atado_prt_cim_helys_irsz',
            'bizf_atado_prt_cim_helys_nev',
            'bizf_atado_prt_cim_kozterulet_jelleg_kod_nev',
            'bizf_atado_prt_cim_kozterulet_nev',
            'bizf_atado_prt_cim_hazszam',
            'bizf_atado_prt_cim_epulet',
            'bizf_atado_prt_cim_emelet',
            'bizf_atado_prt_cim_lepcsohaz',
            'bizf_atado_prt_cim_ajto',
            'bizf_atado_prt_bsz_bankszamlaszam',
            'bizf_atvevo_prt_id',
            'bizf_atvevo_prt_nev',
            'bizf_atvevo_prt_cim_orsz_nev',
            'bizf_atvevo_prt_cim_helys_megye',
            'bizf_atvevo_prt_cim_helys_irsz',
            'bizf_atvevo_prt_cim_helys_nev',
            'bizf_atvevo_prt_cim_kozterulet_jelleg_kod_nev',
            'bizf_atvevo_prt_cim_kozterulet_nev',
            'bizf_atvevo_prt_cim_hazszam',
            'bizf_atvevo_prt_cim_epulet',
            'bizf_atvevo_prt_cim_lepcsohaz',
            'bizf_atvevo_prt_cim_emelet',
            'bizf_atvevo_prt_cim_ajto',
            'bizf_atvevo_prt_bsz_bankszamlaszam',
            'bizf_atvevo_prt_adoszam',
            'bizf_ukoto_prt_id',
            'bizf_ukoto_prt_nev',
            'bizf_fizeto_prt_id ',
            'bizf_fizeto_prt_nev',
            'bizf_fuv_prt_id',
            'bizf_fuv_prt_nev',
            'bizf_penznem_id',
            'bizf_penznem_nev',
            'bizf_fztm_id',
            'bizf_fizmod_kod_nev',
            " (case when bizf_feldolg_status_kod ='0' and bizf_elorogzites_kod ='N' and bizf_rogziteshelye_kod ='1'  then bizf_feldolg_status_kod
                      when bizf_feldolg_status_kod ='0' and bizf_elorogzites_kod ='I' then '1'
                       when bizf_feldolg_status_kod ='0' and bizf_rogziteshelye_kod ='0' then '1'
                      else bizf_feldolg_status_kod
                end ) as   bizf_feldolg_status_kod",
            'bizf_feldolg_status_kod_nev',
            'bizf_biz_szam',
            'bizf_szlev_szam',
            'bizf_szla_szam',
            'bizf_telj_datum',
            'bizf_konyv_datum',
            'bizf_esed_datum',
            'bizf_kif_datuma',
            'bizf_rend_ert',
            'bizf_nyilv_ert',
            'bizf_netto_ert',
            'bizf_brutto_ert ',
            'bizf_tetel_szam',
            'bizf_rendelt_tetel_szam',
            'bizf_leigazolas_datuma',
            'bizf_pufelad_datuma',
            'bizf_eredeti_kinyomtatva',
            'bizf_eredeti_kinyomtatva_kod_nev',
            'bizf_masolat_peldanyszama',
            'bizf_masodlat_peldanyszama',
            'bizf_nyomtatas_szam',
            'bizf_rakt_id',
            'bizf_rakt_nev',
            'bizf_kszk_id',
            'bizf_kszk_nev',
            'bizf_pnztr_id',
            'bizf_pnzn_nev',
            //'bizf_megjegyzes',
            'bizf_letre_felh_nev',
            'bizf_letre_dat',
            'bizf_afa_ertek',
            'bizf_rovidmegjegyzes',
            'bizf_vrend_iktatoszama',
            'bizf_fizetendo_brutto_ert',
            'bizf_atvevoszallitasicim_helyseg',
            'bizf_atvevoszallitasicim_kozterulet',
            'bizf_nem_arazott',
            'bizf_visszavett',
            ' bizf_mulapf_id',
            'bizf_mulapf_munkaszam',
            'bizf_sztorno_bizf_id',
            'bizf_sztornozott_bizf_id',
            'bizf_bizonylatkep',
            'bizf_netto_beszerzesi_ert',
            'bizf_brutto_beszerzesi_ert',
            'bizf_kifizetendo_beszerzesi_ert',
            'bizf_fkod_id',
            'bizf_mozgas_kod',
            'bizf_mozgas_nev',
            'bizf_ervenyeseg',
            'bizf_kifizetett_brutto_ert',
            'bizf_maradekosszeg',
            'bizf_beszerzesi_maradekosszeg',
            'bizf_kifizetes_datuma',
            'bizf_beszerzesi_afa_erteke',
            'bizf_afa_id',
            'bizf_afa_nev',
            'bizf_atvevo_prt_bsz_iban',
            'bizf_atvevo_prt_bank_swift_kod',
            'bizf_atvevo_prt_kozossegiszam',
            'bizf_atado_prt_bsz_iban',
            'bizf_atado_prt_bank_swift_kod',
            'bizf_atado_prt_kozossegiszam',
            'bizf_munhely_megnevezes',
            'bizf_munhely_cim',
            'bizf_mulapf_megnevezese',
            'bizf_nemarazotetelszam as nemarazotetelszam',
            'bizf_bearazotetelszam as bearazotetelszam',
            'bizf_atado_prt_prt_cim_id',
            'bizf_atvevo_prt_prt_cim_id',
            'bizf_reszben_arazott',
            'bizf_lezartbizonylat_tomb',
            'bizf_atado_prt_prt_nev',
            'bizf_atado_prt_prt_cim_hely_nev',
            'bizf_atado_prt_prt_cim_kozterulet',
            'bizf_atvevo_prt_prt_nev',
            'bizf_atvevo_prt_prt_cim_hely_nev',
            'bizf_atvevo_prt_prt_cim_kozterulet',
            'bizf_atvevo_szallaitasi_prt_teljescime',
            'bizf_egyeszrekerekites',
            'bizf_emailout_id',
            'bizf_emailout_cimzett',
            'bizf_emailout_elkuldve',
            'bizf_emailout_elkuldesideje',
            'bizf_prtautsz_nev',
            'bizf_prtautsz_szamlazasinapok',
            'bizf_atvevo_prt_csoportosadoszam',
            'bizf_atado_prt_csoportosadoszam',
            'bizf_azon_szamlazhato',
            'bizf_nvtrt_inv_status_kod_nev',
            'bizf_atvevo_prt_partner_tipus_kod',
            'bizf_atvevo_prt_partner_tipus_kod_nev',
            "bizf_rogziteshelye_kod",
            "bizf_rogziteshelye_kod_nev",
            "bizf_elorogzites_kod",
            "bizf_elorogzites_kod_nev"
        ];
        $model = '\App\Models\Types\TBizfejek1';
        $bizonylattipus = $request->menuparameter['bizonylattipus'];
        $felhasznalo = UserHelpers::getFelhasznalo($request->felh_userid);
        if ($felhasznalo->felh_cikk_szum_keszlet_kod == '6') {
            $bizonylattipus= $request->menuparameter['bizonylattipus'].' and (bizf_atvevo_prt_id='.$felhasznalo->felh_prt_prt_id.' or bizf_atado_prt_id='.$felhasznalo->felh_prt_prt_id.')';
        }
        $bizonylattipus=$bizonylattipus." and azon.azon_biztip_kod ='".$request->menuparameter['BIZTIP_KOD']."'";
        $bizonylattipus=$bizonylattipus." and bizf_feldolg_status_kod ='0' ";
        $bizonylattipus=$bizonylattipus." and bizf_rogziteshelye_kod ='1' ";
        $bizonylattipus=$bizonylattipus." and bizf_elorogzites_kod ='N' ";
//        \MyArray::toStringToLog($bizonylattipus);

        ///\MyArray::toStringToLog($bizonylattipus);
        //$dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$request->menuparameter['bizonylattipus']], true,' order by bizf_esed_datum desc');
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$bizonylattipus], true,' order by bizf_esed_datum desc');
        return response()->success($dmdata);
    }
    public function onepage($skip='1',$base_macro =' ')
    {
        //
                $dmdata = self::getMasterAll(DMBizonylatfej::MASTERSQL,['&limit' => 25,'&skip' => $skip-1, '&base_macro' => $base_macro]);
         //\Log::debug("mesage" .print_r($dmcikkek,true));
                //$dmcikkek = \DB::select(DMCikkek::MASTERSQL);
                //$dmcikkek = \App\DMMenu::hydrate($dmcikkek);
                //$dmcikkek = \App\DMMenu::hydrate($dmcikkek);
        return response()->success(compact('dmdata'));
    }
    public function masterGet($id){
        $model = new $this->model();
        $dmdata = self::getMasterAll($this->model::MASTERSQL."' and bizf.bizf_id=".$id."')  as bizf",['&base_macro' => ' where '.$model->alias.'.'.$model->alias.'_id = '.$id])[0];
        return  $dmdata;
        //return  $this->model::hydrate($dmdata);
        //return $this->model::find($id);
    }

    public function destroy($id)
    {
        $sql = 'delete from "04_rak".biz_tetelek where bizt_bizf_id = '.$id;
        \DB::select($sql);
        $this->model::destroy($id);
        return response()->success('success');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->model::find($id)->modifyelrogzites('I');
        return response()->success('success');
    }
}
