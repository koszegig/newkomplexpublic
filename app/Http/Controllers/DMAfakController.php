<?php

namespace App\Http\Controllers;

use App\Models\Tables\DMAfak;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;

/**
 * Class DMAfakController
 * @package App\Http\Controllers
 */
class DMAfakController extends Controller
{
    /**
     * DMAfakController constructor.
     */
    public function __construct(){
      $this->model = \App\Models\Tables\DMAfak::class;
        $this->dnmodel ='\App\Models\Types\TAfak1';
        $this->dncol = [
            'afa_nev as name'
        ];
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [
        'afa_id',
        'afa_kod',
        'afa_nev',
        'afa_szazalek',
        'afa_szorzo',
        'afa_letre_felh_nev',
        'afa_letre_dat',
        'afa_aktiv',
        'afa_kezdete',
        'afa_vege',
        'afa_forditottafa',
        'afa_forditottafa_kod_nev',
        'afa_biztip_kod',
        'afa_biztip_kod_nev',
        'afa_mid',
        'afa_mplu',
        'afa_mgyujto',
        'afa_mpluszam',
        'afa_mbillid',
        'afa_mvonalkod',
        'afa_kszlaket_afakod',
        'afa_uuid'
                  ];
      $dmdata = $this->selectStoredProcedure($this->dnmodel, $request, $select, $size);
      return response()->success($dmdata);
    }

    /**
     * @param string $skip
     * @param string $base_macro
     * @return mixed
     */
    public function onepage($skip='1', $base_macro =' ')
    {
        //
        $model = new $this->model();
        $dmdata = self::getMasterAll($model->MASTERSQL,['&limit' => 25,'&skip' => $skip-1, '&base_macro' => $base_macro]);
        return response()->success(compact('dmdata'));
    }


}
