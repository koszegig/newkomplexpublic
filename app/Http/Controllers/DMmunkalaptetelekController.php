<?php

namespace App\Http\Controllers;
use App\Models\Types\TMunkalaptetelek1;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;
class DMmunkalaptetelekController extends Controller
{
    /**
     * DMmunkahelyekController constructor.
     */
    public function __construct()
    {
        $this->model = App\Models\Types\TMunkalaptetelek1::class;
    }
    /**
     * @param Request $request
     * @param int $size
     * @return mixed
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            "mulapt_id",
			 "mulapt_mulapf_id",
			 "mulapt_mulapf_munhely_megnevezes",
			 "mulapt_mulapf_munhely_cim",
			 "mulapt_vrtm_id",
			 "mulapt_letre_felh_nev",
			 "mulapt_letre_dat",
			 "mulapt_foly_kezd",
			 "mulapt_foly_vege",
			 "mulapt_raforditott_ora",
			 "mulapt_feldolg_statusz_kod",
			 "mulapt_feldolg_status_kod_nev",
			 "mulapt_szrl_id",
			 "mulapt_szrl_prt_nev",
			 "mulapt_jarmu_id",
			 "mulapt_jarmu_rendszam",
			 "mulapt_kilometerallas",
			 "mulapt_tervezet_kezd",
			 "mulapt_tervezet_vege",
			 "mulapt_osszeg",
			 "mulapt_pnzn_id",
			 "mulapt_pnzn_rovidites",
			 "mulapt_dlgpnzjogc_nev",
			 "mulapt_elvegzettmunka",
			 "mulapt_oradij",
			 "mulapt_csatoltdokumentum",
			 "mulapt_mulapf_megnevezese",
			 "mulapt_tenyleges_osszeg",
			 "mulapt_mulapf_munkaszam",
			 "mulapt_munkalapkeziszam",
			 "mulapt_mulapf_munhely_prt_nev",
			 "mulapt_munhely_prtelhtsg_mobilszam",
			 "mulapt_dlgpnzjogc_id",
        ];
        $model = "App\Models\Types\TMunkalaptetelek1";

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function listprint(Request $request, $size = 200)
    {
        $select = [
            "mulapt_id",
			 "mulapt_mulapf_id",
			 "mulapt_mulapf_munhely_megnevezes",
			 "mulapt_mulapf_munhely_cim",
			 "mulapt_vrtm_id",
			 "mulapt_letre_felh_nev",
			 "mulapt_letre_dat",
			 "mulapt_foly_kezd",
			 "mulapt_foly_vege",
			 "mulapt_raforditott_ora",
			 "mulapt_feldolg_statusz_kod",
			 "mulapt_feldolg_status_kod_nev",
			 "mulapt_szrl_id",
			 "mulapt_szrl_prt_nev",
			 "mulapt_jarmu_id",
			 "mulapt_jarmu_rendszam",
			 "mulapt_kilometerallas",
			 "mulapt_tervezet_kezd",
			 "mulapt_tervezet_vege",
			 "mulapt_osszeg",
			 "mulapt_pnzn_id",
			 "mulapt_pnzn_rovidites",
			 "mulapt_dlgpnzjogc_nev",
			 "mulapt_elvegzettmunka",
			 "mulapt_oradij",
			 "mulapt_csatoltdokumentum",
			 "mulapt_mulapf_megnevezese",
			 "mulapt_tenyleges_osszeg",
			 "mulapt_mulapf_munkaszam",
			 "mulapt_munkalapkeziszam",
			 "mulapt_mulapf_munhely_prt_nev",
			 "mulapt_munhely_prtelhtsg_mobilszam",
			 "mulapt_dlgpnzjogc_id",
					];
        $model = "App\Models\Types\TMunkalaptetelek1";
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
}
