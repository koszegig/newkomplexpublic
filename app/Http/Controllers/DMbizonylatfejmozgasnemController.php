<?php

namespace App\Http\Controllers;

use App\Models\Tables\DMbizonylatfejmozgasnem;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;


class DMbizonylatfejmozgasnemController extends Controller
{
    public function __construct(){
        $this->model = \App\Models\Tables\DMbizonylatfejmozgasnem::class;
        $this->dnmodel ='App\Models\Tables\DMbizonylatfejmozgasnem';
        $this->dncol = [
            'bizfmgsn_id as id',
            'bizfmgsn_nev as name'
        ];
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            "bizfmgsn_id",
            "bizfmgsn_rovidites",
            "bizfmgsn_nev",
            "bizfmgsn_aktiv"

        ];
        $model = '\App\Models\Tables\DMbizonylatfejmozgasnem';
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
        return response()->success($dmdata);
    }

}
