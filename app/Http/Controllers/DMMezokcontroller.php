<?php

namespace App\Http\Controllers;


use App\Models\Tables\DMMezok;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
class DMMezokcontroller extends Controller
{
    public function __construct(){
      $this->model = \App\DMMezok::class;
    }
	    //
    public function getMezoLeirasok($p_table_named)
    {
        $dmmezoleirasok = DMMezok::getMezoLeirasok($p_table_named);
         if(empty($dmmezoleirasok) || is_null($dmmezoleirasok)) return response()->success([]);
        return response()->success($dmmezoleirasok);
    }

}
