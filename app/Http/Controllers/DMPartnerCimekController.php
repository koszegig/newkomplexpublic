<?php

namespace App\Http\Controllers;


use App\DMPartnerCimek;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;
class DMPartnerCimekController extends Controller
{
    public function __construct()
    {
      $this->model = App\Models\Tables\DMPartnerCimek::class;


    }

/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [
                'prt_cim_id',
                'prt_cim_prt_id',
                'prt_cim_tipus_kod',
                'prt_cim_tipus_kod_nev',
                'prt_cim_helys_id',
                'prt_cim_orsz_nev',
                'prt_cim_helys_megye_nev',
                'prt_cim_helys_irsz',
                'prt_cim_helys_nev',
                'prt_cim_kozterulet_jelleg',
                'prt_cim_kozterulet_jelleg_kod_nev',
                'prt_cim_kozterulet_nev',
                'prt_cim_hazszam',
                'prt_cim_epulet',
                'prt_cim_lepcsohaz',
                'prt_cim_emelet',
                'prt_cim_ajto',
                'prt_letre_felh_nev',
                'prt_letre_dat',
                'prt_valasztott',
                'prt_teljescim',
                'prt_telephely_megnevezese',
                'prt_telephelyszam',
                'prt_kerulet',
                'prt_cim_kozterulet_jelleg_id',
                'prt_cim_helyrajziszam',
                'prt_cim_uuid'
                  ];
      $model = '\App\Models\Types\TPartnerekcimei2';
      $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
      return response()->success($dmdata);
    }

    public function onepage(Request $request)
    {
      //$this->_setFilters($request);
       $size = 25;
        $select = [
            'prt_cim_id',
            'prt_cim_prt_id',
            'prt_cim_tipus_kod',
            'prt_cim_tipus_kod_nev',
            'prt_cim_helys_id',
            'prt_cim_orsz_nev',
            'prt_cim_helys_megye_nev',
            'prt_cim_helys_irsz',
            'prt_cim_helys_nev',
            'prt_cim_kozterulet_jelleg',
            'prt_cim_kozterulet_jelleg_kod_nev',
            'prt_cim_kozterulet_nev',
            'prt_cim_hazszam',
            'prt_cim_epulet',
            'prt_cim_lepcsohaz',
            'prt_cim_emelet',
            'prt_cim_ajto',
            'prt_letre_felh_nev',
            'prt_letre_dat',
            'prt_valasztott',
            'prt_teljescim',
            'prt_telephely_megnevezese',
            'prt_telephelyszam',
            'prt_kerulet',
            'prt_cim_kozterulet_jelleg_id',
            'prt_cim_helyrajziszam',
            'prt_cim_uuid'
                ];
    $model = '\App\Models\Types\TPartnerekcimei2';
    $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
        return response()->success(compact('dmdata'));
    }


}
