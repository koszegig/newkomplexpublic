<?php

namespace App\Http\Controllers;

use App\Models\Tables\TCikkek2;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;

class DMSelectCikkekController extends Controller
{
    public function __construct()
    {
        $this->model = \App\Models\Types\TCikkek2::class;


    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
                    'cik_id',
              'cik_kod',
              'cik_nev',
              'cik_vtsz_id',
              'cik_vtszafa_afak_kod',
              'cik_vtszafa_afa_nev',
              'cik_vtszafa_vtsz_szam',
              'cik_vtszafa_vtsz_nev',
              'cik_vtszafa_vtsz_szavidos',
              'cik_me_id',
              'cik_me_rovidites ',
              'cik_me_nev',
              'cik_aktiv',
              'cik_kep',
              'cik_tipus_kod',
              'cik_tipus_kod_nev',
              'cik_keszlet_mod_kod',
              'cik_keszlet_mod_kod_nev',
              'cik_jovedeki_termek_kod',
              'cik_jovedeki_termek_kod_nev',
              'cik_arlistas_kod',
              'cik_arlistas_kod_nev',
              'cik_szavidos_kod',
              'cik_szavidos_kod_nev',
              'cik_letre_felh_nev',
              'cik_letre_dat',
              'cik_afa_id',
              'cik_afa_nev',
              'cikk_handle',
              'cik_suly',
              'cik_suly_me_id',
              'cik_sulyme_rovidites',
              'cik_sulyme_nev',
              'cik_fixaras_kod',
              'cik_fixaras_kod_nev',
              'cik_beszerzersinettoar',
              'cik_beszerzersibruttoar',
              'cik_eladasinettoar',
              'cik_eladasibruttoar',
              'cik_pnzn_id',
              'cik_pnzn_rovidites',
              'cik_pnzn_nev',
              'cik_akcios_ar',
              'cik_egyseg_ar',
              'cik_cgy_gyariszam',
              'cik_cgy_id',
              'cik_cgy_bevetel_datuma',
               'cik_cgy_eladas_datuma',
               'cik_cgy_rakt_id',
               'cik_forditottafa',
               'cik_forditottafa_kod_nev',
               'cik_besz_pnzn_rovidites',
               'cik_besz_pnzn_nev',
               'cik_besz_pnzn_id',
               'cik_armeny_szorzo',
               'cik_armeny_me_id',
               'cik_csatolt_dokumentum_path',
               'cik_csatolt_tervrajz',
               'cik_vonal_azon',
               'cik_szigoruszamadasu',
               'cik_lapszam',
               'cik_beszmodidopontja',
               'cik_mpraktkeszl_akt_keszl',
               'cik_mpraktkeszl_min_keszl',
               'cik_min_keszl_alatt',
               'cik_csz_cikkszam',
               'cik_be_rakt_id',
               'cik_be_rakt_nev',
               'cik_ki_rakt_id',
              'cik_ki_rakt_nev',
               'cik_mpraktkeszl_fogl_keszl'
        ];
        $model = '\App\Models\Types\TCikkek2';
        $menuparameter = $request->menuparameter;
        //\MyArray::toStringToLog($menuparameter);
        if (isset($menuparameter['datum'])) {
            $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$menuparameter['datum'],$menuparameter['prt_id'],$menuparameter['pnzn_id'],$menuparameter['pnza_arfolyam'],$menuparameter['artbl_arkategoria'],$menuparameter['rakt_id']]);
            return response()->success($dmdata);
        } else {
            $menuparameter['datum'] = '2021-04-09 00:00:00';
            $menuparameter['prt_id'] = 6;
            $menuparameter['pnzn_id'] = 1;
            $menuparameter['pnza_arfolyam'] = 1.000;
            $menuparameter['artbl_arkategoria'] = 'k';
            $menuparameter['rakt_id'] = 5;
            $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$menuparameter['datum'],$menuparameter['prt_id'],$menuparameter['pnzn_id'],$menuparameter['pnza_arfolyam'],$menuparameter['artbl_arkategoria'],$menuparameter['rakt_id']]);
            return response()->success($dmdata);
        }
    }

}
