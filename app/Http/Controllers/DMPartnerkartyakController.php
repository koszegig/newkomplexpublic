<?php

namespace App\Http\Controllers;
use App\Models\Types\TPartnerkartyak1;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;

class DMPartnerkartyakController extends Controller
{
    /**
     * DMmunkahelyekController constructor.
     */
    public function __construct()
    {
        $this->model = App\Models\Types\TPartnerkartyak1::class;
    }
    /**
     * @param Request $request
     * @param int $size
     * @return mixed
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            "prtkrt_id",
            "prtkrt_prt_id",
            "prtkrt_prt_nev",
            "prtkrt_szam",
            "prtkrt_kep",
            "prtkrt_qrkod",
            "prtkrt_tipus_kod",
            "prtkrt_tipus_kod_nev",
           // "prtkrt_rovidmegjegyzes",
           // "prtkrt_megjegyzes",
            "prtkrt_aktiv",
            "prtkrt_letre_felh_nev",
            "prtkrt_letre_dat",
            "prtkrt_uuid",
            "prtkrt_eredeti_kinyomtatva",
            "prtkrt_eredeti_kinyomtatva_kod_nev",
        ];
        $model = "App\Models\Types\TPartnerkartyak1";

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function listprint(Request $request, $size = 200)
    {
        $select = [
            "prtkrt_id",
            "prtkrt_prt_id",
            "prtkrt_prt_nev",
            "prtkrt_szam",
            "prtkrt_kep",
            "prtkrt_qrkod",
            "prtkrt_tipus_kod",
            "prtkrt_tipus_kod_nev",
          //  "prtkrt_rovidmegjegyzes",
          //  "prtkrt_megjegyzes",
            "prtkrt_aktiv",
            "prtkrt_letre_felh_nev",
            "prtkrt_letre_dat",
            "prtkrt_uuid",
            "prtkrt_eredeti_kinyomtatva",
            "prtkrt_eredeti_kinyomtatva_kod_nev",
        ];
        $model = "App\Models\Types\TPartnerkartyak1";
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[]);
        return response()->success($dmdata);
    }
}
