<?php

namespace App\Http\Controllers;
use App\Models\Types\Tnavtranzakciotetelek1;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;

class DMnavtranzakciotetelekController extends Controller
{
    public function __construct()
    {
        $this->model = App\Models\Types\Tnavtranzakciotetelek1::class;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = ["nvtrt_id",
            "nvtrt_nvtrf_id",
            "nvtrt_inv_id",
            "nvtrt_inv_bizf_id",
            "nvtrt_inv_bizf_azon",
            "nvtrt_inv_bizf_esed_datum",
            "nvtrt_inv_bizf_telj_datum",
            "nvtrt_inv_status_kod",
            "nvtrt_inv_status_kod_nev",
            "nvtrt_inv_invoicedata",
            "nvtrt_inv_filename",
            "nvtrt_index",
            "nvtrt_inv_status",
            "nvtrt_letre_felh_nev",
            "nvtrt_letre_dat",
            "nvtrt_nvtrf_transactionid",
            "nvtrt_nvtrf_eles",
            "nvtrt_nvtrf_version",
            "nvtrt_nvtrf_aktive",
            "nvtrt_nvtrf_senddatetime",
            "nvtrt_nvtrf_recevedatetime"
        ];
        $model = "App\Models\Types\Tnavtranzakciotetelek1";

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[],true,' order by nvtrt_id desc');
        return response()->success($dmdata);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function listprint(Request $request, $size = 200)
    {
        $select = ["nvtrt_id",
                    "nvtrt_nvtrf_id",
                    "nvtrt_inv_id",
                    "nvtrt_inv_bizf_id",
                    "nvtrt_inv_bizf_azon",
                    "nvtrt_inv_bizf_esed_datum",
                    "nvtrt_inv_bizf_telj_datum",
                    "nvtrt_inv_status_kod",
                    "nvtrt_inv_status_kod_nev",
                    "nvtrt_inv_invoicedata",
                    "nvtrt_inv_filename",
                    "nvtrt_index",
                    "nvtrt_inv_status",
                    "nvtrt_letre_felh_nev",
                    "nvtrt_letre_dat",
                    "nvtrt_nvtrf_transactionid",
                    "nvtrt_nvtrf_eles",
                    "nvtrt_nvtrf_version",
                    "nvtrt_nvtrf_aktive",
                    "nvtrt_nvtrf_senddatetime",
                    "nvtrt_nvtrf_recevedatetime"
        ];
        $model = "App\Models\Types\Tnavtranzakciotetelek1";
//        \MyArray::toStringToLog('User->'.$request->felh_userid);
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[],true,' order by nvtrt_id desc');
        return response()->success($dmdata);
    }

}

