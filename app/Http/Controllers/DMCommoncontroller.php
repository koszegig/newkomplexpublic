<?php

namespace App\Http\Controllers;


use App\Models\Tables\DMCommon;
use App\Models\Tables\DMbizonylatfejmozgasnem;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use App\Libraries\Database\QueryBuilderSP;

class DMCommoncontroller extends Controller
{

    public function bizonylatfejmozgasnemlistForDn(Request $request)
    {
        $select = [
            'bizfmgsn_nev as name',
        ];
        $model = 'App\Models\Tables\DMbizonylatfejmozgasnem';
        $dmdata = $this->selectStoredDnProcedure($model, $select);

        return response()->success($dmdata);
    }
    public function get_rendszer_kodokForDn(Request $request)
    {
        $data = $request->all();
        if (isset($data['kodmezo'])){
            $kod_mezo = $data['kodmezo'];
            $dmdata = DMCommon::get_rendszer_kodok($kod_mezo);
            $dmdata = DMCommon::hydrate($dmdata);
            return response()->success(compact('dmdata'));
        }
    }


}
