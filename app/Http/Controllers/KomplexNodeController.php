<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use App\Enums\InvoiceOperationEnum as operations;
use App\Models\Tables\User;


class KomplexNodeController extends Controller
{
    public function env()
    {
        return response()->success(env("KOMPLEXNODEURL"));
    }

}
