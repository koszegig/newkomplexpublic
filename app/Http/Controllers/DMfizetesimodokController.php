<?php

namespace App\Http\Controllers;


use App\Models\Tables\DMfizetesimodok;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;
class DMfizetesimodokController extends Controller
{
        //
    public function __construct(){
      $this->model = \App\Models\Tables\DMfizetesimodok::class;
      $this->dnmodel ='\App\Models\Types\Tfizetesimod1';
        $this->dncol = [
            'fztm_megnevezes as name'
        ];
    }
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [
        'fztm_id',
        'fztm_megnevezes',
        'fztm_tipus_kod',
        'fztm_kod_nev',
        'fztm_fizetesinapok',
        'fztm_felh_nev',
        'fztm_letre_dat',
        'fztm_kassza',
        'fztm_kassza_sorrend',
        'fztm_alap',
        'fztm_kerekites',
        'fztm_szamlan_a_megnevezes',
        'fztm_nyomtatasipeldanyszam',
        'fztm_azonalifizetes',
        'fztm_vegosszegkerekitesmerteke',
        'fztm_paymentmethod_kod',
        'fztm_paymentmethod_kod_nev',
        'fztm_uuid'
                  ];
      $model = '\App\Models\Types\Tfizetesimod1';
      $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
      return response()->success($dmdata);
    }
    public function onepage($skip='1',$base_macro =' ')
    {
        //
        $dmdata = self::getMasterAll(DMfizetesimodok::MASTERSQL,['&limit' => 25,'&skip' => $skip-1, '&base_macro' => $base_macro]);
        return response()->success(compact('dmdata'));
    }


    public function getBizfKifDatuma(Request $request) {
        $data = $request->all();
        $bizf_fztm_id = $data['bizf_fztm_id'];
        $bizf_telj_datum = $data['bizf_telj_datum'];
        $partner_id = $data['partner_id'];
         $dmdata = DMfizetesimodok::get_bizf_kif_datuma($bizf_fztm_id, $bizf_telj_datum,$partner_id);
         $dmdata = $this->model::hydrate($dmdata);
         //\Log::debug("mesage" .print_r($dmcikkek,true));
        return response()->success(compact('dmdata'));
    }
    public function get_azonali_fizetes(Request $request) {
         $regdata = $request->all();
         $fztm_id = $regdata['fztm_id'];
         $data = DMfizetesimodok::get_azonali_fizetes($fztm_id);
         $data = $this->model::hydrate($data);
        return response()->success(compact('data'));
    }



}
