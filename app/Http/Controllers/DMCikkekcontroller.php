<?php

namespace App\Http\Controllers;

use App\Models\Tables\DMCikkek;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Input;
use Log;
use Validator;

class DMCikkekcontroller extends Controller
{

  public function __construct()
  {
    $this->model = App\Models\Tables\DMCikkek::class;


  }
/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
      $select = [       'cik_id'  ,
      'cik_kod'  ,
      'cik_nev'  ,
      'cik_vtsz_id'  ,
      'cik_vtszafa_afak_kod'  ,
      'cik_vtszafa_afa_nev'  ,
      'cik_vtszafa_vtsz_szam'  ,
      'cik_vtszafa_vtsz_nev' ,
      'cik_vtszafa_vtsz_szavidos'  ,
      'cik_me_id'  ,
      'cik_me_rovidites'  ,
      'cik_me_nev'  ,
      'cik_aktiv'  ,
      //'cik_megjegyzes'  ,
      'cik_kep'  ,
      'cik_tipus_kod'  ,
      'cik_tipus_kod_nev' ,
      'cik_keszlet_mod_kod'  ,
      'cik_keszlet_mod_kod_nev'  ,
      'cik_jovedeki_termek_kod' ,
      'cik_jovedeki_termek_kod_nev'  ,
      'cik_arlistas_kod'  ,
      'cik_arlistas_kod_nev'  ,
      'cik_szavidos_kod'  ,
      'cik_szavidos_kod_nev'   ,
      'cik_letre_felh_nev'  ,
      'cik_letre_dat'  ,
      'cik_afa_id'  ,
      'cik_afa_nev'  ,
      'cikk_handle'  ,
      'cik_suly'  ,
      'cik_suly_me_id'  ,
      'cik_sulyme_rovidites'  ,
      'cik_sulyme_nev'  ,
      'cik_fixaras_kod'  ,
      'cik_fixaras_kod_nev'  ,
      'cik_beszerzersinettoar'  ,
      'cik_beszerzersibruttoar',
      'cik_egyseg_ar',
      'cik_cgy_gyariszam',
      'cikkek_cikcsop_nev',
      'cik_kiskernettoar',
      'cik_kiskerbruttoar',
      'cik_forditottafa',
      'cik_forditottafa_kod_nev',
      'cik_netadotk_id',
      'cik_netadotk_megnevezes',
      'cik_trmkdj_id',
      'cik_trmkdj_megnevezes',
      'cik_armeny_szorzo',
      'cik_armeny_me_id',
      'cik_armeny_me_rovidites',
      'cik_armeny_me_nev',
      'cik_csatolt_dokumentum_path',
      'cik_csatolt_tervrajz',
      'cik_kozv_szolgaltatas',
      'cik_kozv_szolgaltatas_kod_nev',
      'cik_szigoruszamadasu',
      'cik_lapszam',
      'cik_beszmodidopontja',
      'cik_vonal_azon',
      'cik_kiszereles',
      'cik_alkoholfok',
      'cik_be_rakt_id',
      'cik_be_rakt_nev',
      'cik_ki_rakt_id',
      'cik_ki_rakt_nev',
      'cik_nagykernettoar',
      'cik_nagykerbruttoar',
      'cik_kiszereles_me_id',
      'cik_kiszereles_me_rovidites',
      'cik_kiszereles_me_nev',
      'cik_kiskernettoegysregar',
      'cik_kiskerbruttoegysregiar',
      'cik_nagykernettoegysregar',
      'cik_nagykerbruttoegysregiar',
      'cik_egysegar_me_id',
      'cik_egysegar__me_rovidites',
      'cik_egysegar__me_nev'
                  ];
      $model = '\App\Models\Types\TCikkek3';
      $dmdata = $this->selectStoredProcedure($model, $request, $select, $size);
      return response()->success($dmdata);
    }
  //

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  /* public function __construct($auth = true)
    {
    if($auth)
            $this->middleware('auth');

        // $this->middleware('subscribed');
    }*/
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  /* public function list($size = 25)
    {
          $where = \MyQueryBuilder::generate_where();
         $dmdata = self::getMasterAll($this->model::MASTERSQL,['&base_macro' => $where]);
         $dmdata = $this->model::hydrate($dmdata);
          $dmdata = \MyPage::paginateWithoutKey($dmdata,$size);
         //\Log::debug("mesage" .print_r($dmcikkek,true));
        return response()->success(compact('dmdata'));
    }*/
  public function onepage($skip = '1', $base_macro = ' ')
  {
    //
    $dmdata = self::getMasterAll(DMCikkek::MASTERSQL, ['&limit' => 25, '&skip' => $skip - 1, '&base_macro' => $base_macro]);
    //\Log::debug("mesage" .print_r($dmcikkek,true));
    //$dmcikkek = \DB::select(DMCikkek::MASTERSQL);
    //$dmcikkek = \App\DMMenu::hydrate($dmcikkek);
    //$dmcikkek = \App\DMMenu::hydrate($dmcikkek);
    return response()->success(compact('dmdata'));
  }
}
