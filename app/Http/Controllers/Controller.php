<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\DMmenuparameterek;
use App\DMMezok;
use App\Libraries\NodeConnector\NodeConnector;
use Illuminate\Support\Arr;
use Spatie\QueryBuilder\QueryBuilder;
use App\Libraries\Database\QueryBuilderSP;
use Illuminate\Support\Facades\Config;
use Log;

class Controller extends BaseController
{

  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  protected $model;
  protected $dnmodel;

  protected $dncol = ['id', 'name'];
    protected $CommandLists=[
        ['command' =>'connect:import:attributes ',
            'jobname' =>  'ImportAttributes'
        ],
        ['command' =>'connect:import:categories',
            'jobname' =>  'ImportCategories'
        ]
    ];
  public $typeSelect = [];
  public $typeModel = '';


  public static function getMasterAll($query, $params = [])
  {
    //    $query = 'SELECT * FROM "01_sys".get_menu_web_1( ? ) ';
    if (isset($params['&base_macro'])) {
      $query = $query . ' ' . $params['&base_macro'];
    }

    $result = \DB::select($query);
    return $result;
  }
  public static function getInputmezoadatok($p_table_name = '')
  {

    $result = DMMezok::getmezo($p_table_name);
    $mezodatas = array();
    foreach ($result as $key => $item) {
      $mezodatas[$item['datafield']] = $item;
    }

    return $mezodatas;
  }
  public function destroy($id)
  {
    $this->model::destroy($id);
    return response()->success('success');
  }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        return $this->model::find($id)->modifyActive(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function inactivate($id)
    {
        return $this->model::find($id)->modifyActive(false);
    }

  public function listForDn(Request $request)
  {
      $dmdata = $this->getDnData($request->all());
      return response()->success($dmdata);
  }

  protected function getDnData($condition)
  {
      $dmdata = $this->selectStoredDnProcedure($this->dnmodel, $this->dncol);
      return $dmdata;
  }


  public function getFields()
  {
    $model = new $this->model();
    return $model->getFields();
  }
  public function find($id)
  {
      //\MyArray::toStringToLog($this->typeModel.'$this->typeModel');
      $model = new $this->model();
        $builder = new QueryBuilderSP($model->typeModel, [],null, $model->typeSelect,[$id],false);
         return $builder->get();
  }

  public function get($id){
      return $this->find($id);
  }


  public function masterGet($id)
  {
    $model = new $this->model();
    $dmdata = self::getMasterAll($model->MASTERSQL, ['&base_macro' => ' where ' . $model->alias . '.' . $model->alias . '_uuid = ' . $id])[0];
    return  $dmdata;
    //return  $this->model::hydrate($dmdata);
    //return $this->model::find($id);
  }
  protected function getModelname()
  {
    $model = explode('\\', $this->model);
    return array_last($model);
  }
     /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function listFieldopt($model, $field)
    {
        $dmdata = $this->getOptData($model, $field);
        $dmdata =  $dmdata->map(
            function ($item) {
                return $item->only(['name','display_name']);
            }
        );
        return response()->success(compact('dmdata'));
    }
  protected function getOptData($model, $field)
  {
    return \MyModOpt::showModelField($model, $field);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $dmdata = $this->get($id);
    $dmdata = $this->postGet($dmdata);
    return response()->success($dmdata);
  }

  public function postGet($dmdata)
  {
    return $dmdata;
  }

  public function postStore($dmdata, $reques)
  {
     $model = new $this->model();
     $idName = $model->getKeyName();
    //return $this->show($dmdata->id);
       //    \MyArray::toStringToLog($dmdata->$idName);
    return $this->show($dmdata->$idName);

  }
  public function preStore($data, Request $request)
  {
    return $data;
  }


  public function postUpdate($id, $reques)
  {
   // return $this->postStore($dmdata, $reques);
      return $this->show($id);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $this->validator($data);
    $data = $this->preStore($data, $request);
    $dmdata = $this->create($data);
    $dmdata = $this->postStore($dmdata, $request);
      return $dmdata;
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
        $data = $request->all();
        $this->validator($data);
       // $data = $this->preUpdate($data, $request);
        $dmdata = $this->edit($id, $data);
        $dmdata = $this->postUpdate($id, $request);

        return $dmdata;
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($data)
  {
    $data = $this->getCreateData($data);
      $model = new $this->model();
      $data[$model->getKeyName()] = $this->model::nextseq($model);
//      \MyArray::toStringToLog($model->getKeyName());
//      \MyArray::toStringToLog($data);
      return $this->model::create($data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id, $data)
  {
    $data = $this->getUpdateData($data);
      $model = new $this->model();
      $idName = $model->getKeyName();
      /*return $this->show($dmdata->$idName);*/

    $this->model::UpdateOrCreate([$idName=> $id],$data);
    /*$dmdata = $this->find($id);
      \MyArray::toStringToLog($dmdata);
     foreach ($data as $key => $value)
      $dmdata->$key = $value;
    $dmdata->save();*/
      return $this->show($id);
  }



  protected function getDbData($data)
  {
      $_data = [];
      $model = new $this->model();

      foreach($model->getFillable() as $field){
          $value = Arr::get($data, $field, null);
          if($value !== null){
              $_data[$field] = $value;
          }
      }
//        \MyArray::toStringToLog($_data);
      return $_data;

  }

  protected function getCreateData($data)
  {
    return $this->getDbData($data);
  }

  protected function getUpdateData($data)
  {
    return $this->getDbData($data);
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return true;
  }
    protected function filter()
    {
        return QueryBuilder::for($this->model)->get();
    }

    protected function selectStoredProcedure($model, $request, $select, $paginate = null, $params = [], $isAddId= true,$orderBy= '')
    {
      if(!isset($request->filters)) $request->filters = [];
      $this->setFilters($request);
      $this->setMacro($request);
    //    \MyArray::toStringToLog($request->filters);
      $builder = new QueryBuilderSP($model, $request, $paginate, $select, $params,$isAddId,$orderBy);
      return $builder->get();
    }
//
    public function setFilters(Request &$request){
      return;
    }
    public function setMacro(Request &$request){
        if(!isset($request->macro)) $request->macro = [];
        return;
    }

    protected function selectStoredDnProcedure($model,  $select, $paginate = null, $params = [])
    {
        $request = new \stdClass();
        $request->filters = [];
        $builder = new QueryBuilderSP($model, $request, $paginate, $select, $params);
        //return $builder->get()->toArray();
        return $builder->get();
    }
    public function runcommand(Request $request)
    {
       /* $path = storage_path('logs').'/';
        $data = $request->all();
        $taskid =0;
        if (isset($data['taskid'])){
            $taskid = $data['taskid'];
            \AionArray::toStringToLog ($taskid,' TaskController - $task ',__FILE__, __METHOD__, __LINE__);
            $task = Task::where("id", $taskid)->first();
            $command='';
            if (!empty($task)) {
                $task = $task->toArray();
                \AionArray::toStringToLog ($task,' TaskController - $task ',__FILE__, __METHOD__, __LINE__);
            }
        }
        foreach ($this->CommandLists as $CommandList) {
            if ($command =='') {
                if ($task['task_type'] == $CommandList['jobname']) {
                    $command = 'php '.base_path().'/artisan '.$CommandList['command'].' > '.$path.$task['task_type'] .'_frontend.log';
                    \AionArray::toStringToLog ($command,' TaskController - command ',__FILE__, __METHOD__, __LINE__);
                    exec($command);

                }
            }
        }*/


    }
    public function export(Request $request)
    {

    }
    public function komplexnode(Request $request,$action,$mode='not')
    {
        $data =  [
            //'tv_ipcimek' => [$pc->only('id','address','port')],
        ];
        $connector = new NodeConnector('komplexnode',$action,$data);

        if ($mode == 'not') {
            $result = $connector->get(false);
          return $result;
        }
         if ($mode == 'json') {
             $result = $connector->get(false);
             return $result;
         }
        if ($mode == 'success') {
           $result = $connector->get(false);
             return response()->success($result);
        }
    }

    public function komplextavnyomtatas(Request $request,$action,$mode='not'){
        $data =  [];
        $connector = new NodeConnector('komplextavnyomtatas',$action,$data);
        if ($mode == 'not') {
            $result = $connector->get(false);
            return $result;
        }
        if ($mode == 'json') {
            $result = $connector->get(false);
            return $result;
        }
        if ($mode == 'success') {
            $connector->get(false);
            return response()->success($result);
        }

    }

}
