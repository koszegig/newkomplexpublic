<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;
use DB;
use Session;
use Auth;

class DMBizonylatTetelekLekerdezeseController extends Controller
{
    public function __construct()
    {
        $this->model = \App\Models\Types\TBizfejek1::class;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = [
            'bizf_id',
              'bizf_azon',
              'bizf_azon_id',
              'bizf_azon_egyseg_prt_id',
              'bizf_azon_prt_nev',
              'bizf_azon_biztip_kod',
              'bizf_azon_biztip_kod_nev',
              'bizf_azon_ev',
              'bizf_atado_prt_id',
              'bizf_atado_prt_nev',
              'bizf_atado_prt_adoszam',
              'bizf_atado_prt_cim_orsz_nev',
              'bizf_atado_prt_cim_helys_megye',
              'bizf_atado_prt_cim_helys_irsz',
              'bizf_atado_prt_cim_helys_nev',
              'bizf_atado_prt_cim_kozterulet_jelleg_kod_nev',
              'bizf_atado_prt_cim_kozterulet_nev',
              'bizf_atado_prt_cim_hazszam',
              'bizf_atado_prt_cim_epulet',
              'bizf_atado_prt_cim_emelet',
              'bizf_atado_prt_cim_lepcsohaz',
              'bizf_atado_prt_cim_ajto',
              'bizf_atado_prt_bsz_bankszamlaszam',
              'bizf_atvevo_prt_id',
              'bizf_atvevo_prt_nev',
              'bizf_atvevo_prt_cim_orsz_nev',
              'bizf_atvevo_prt_cim_helys_megye',
              'bizf_atvevo_prt_cim_helys_irsz',
              'bizf_atvevo_prt_cim_helys_nev',
              'bizf_atvevo_prt_cim_kozterulet_jelleg_kod_nev',
              'bizf_atvevo_prt_cim_kozterulet_nev',
              'bizf_atvevo_prt_cim_hazszam',
              'bizf_atvevo_prt_cim_epulet',
              'bizf_atvevo_prt_cim_lepcsohaz',
              'bizf_atvevo_prt_cim_emelet',
              'bizf_atvevo_prt_cim_ajto',
              'bizf_atvevo_prt_bsz_bankszamlaszam',
              'bizf_atvevo_prt_adoszam',
              'bizf_ukoto_prt_id',
              'bizf_ukoto_prt_nev',
              'bizf_fizeto_prt_id ',
              'bizf_fizeto_prt_nev',
              'bizf_fuv_prt_id',
              'bizf_fuv_prt_nev',
              'bizf_penznem_id',
              'bizf_penznem_nev',
              'bizf_fztm_id',
              'bizf_fizmod_kod_nev',
              " (case when bizf_feldolg_status_kod ='0' and bizf_elorogzites_kod ='N' and bizf_rogziteshelye_kod ='1' then bizf_feldolg_status_kod
                      when bizf_feldolg_status_kod ='0' and bizf_rogziteshelye_kod ='0' then '1'
                      when bizf_feldolg_status_kod ='0' and bizf_elorogzites_kod ='I' then '1'
                      else bizf_feldolg_status_kod
                end ) as   bizf_feldolg_status_kod",
              'bizf_feldolg_status_kod_nev',
              'bizf_biz_szam',
              'bizf_szlev_szam',
              'bizf_szla_szam',
              'bizf_telj_datum',
              'bizf_konyv_datum',
              'bizf_esed_datum',
              'bizf_kif_datuma',
              'bizf_rend_ert',
              'bizf_nyilv_ert',
              'bizf_netto_ert',
              'bizf_brutto_ert ',
              'bizf_tetel_szam',
              'bizf_rendelt_tetel_szam',
              'bizf_leigazolas_datuma',
              'bizf_pufelad_datuma',
              'bizf_eredeti_kinyomtatva',
              'bizf_eredeti_kinyomtatva_kod_nev',
              'bizf_masolat_peldanyszama',
              'bizf_masodlat_peldanyszama',
              'bizf_nyomtatas_szam',
              'bizf_rakt_id',
              'bizf_rakt_nev',
              'bizf_kszk_id',
              'bizf_kszk_nev',
              'bizf_pnztr_id',
              'bizf_pnzn_nev',
              //'bizf_megjegyzes',
              'bizf_letre_felh_nev',
              'bizf_letre_dat',
              'bizf_afa_ertek',
              'bizf_rovidmegjegyzes',
              'bizf_vrend_iktatoszama',
              'bizf_fizetendo_brutto_ert',
              'bizf_atvevoszallitasicim_helyseg',
              'bizf_atvevoszallitasicim_kozterulet',
              'bizf_nem_arazott',
              'bizf_visszavett',
              ' bizf_mulapf_id',
               'bizf_mulapf_munkaszam',
               'bizf_sztorno_bizf_id',
               'bizf_sztornozott_bizf_id',
               'bizf_bizonylatkep',
               'bizf_netto_beszerzesi_ert',
               'bizf_brutto_beszerzesi_ert',
               'bizf_kifizetendo_beszerzesi_ert',
               'bizf_fkod_id',
               'bizf_mozgas_kod',
               'bizf_mozgas_nev',
               'bizf_ervenyeseg',
               'bizf_kifizetett_brutto_ert',
               'bizf_maradekosszeg',
               'bizf_beszerzesi_maradekosszeg',
               'bizf_kifizetes_datuma',
               'bizf_beszerzesi_afa_erteke',
               'bizf_afa_id',
               'bizf_afa_nev',
               'bizf_atvevo_prt_bsz_iban',
               'bizf_atvevo_prt_bank_swift_kod',
               'bizf_atvevo_prt_kozossegiszam',
               'bizf_atado_prt_bsz_iban',
               'bizf_atado_prt_bank_swift_kod',
               'bizf_atado_prt_kozossegiszam',
               'bizf_munhely_megnevezes',
               'bizf_munhely_cim',
               'bizf_mulapf_megnevezese',
               'bizf_nemarazotetelszam as nemarazotetelszam',
               'bizf_bearazotetelszam as bearazotetelszam',
               'bizf_atado_prt_prt_cim_id',
               'bizf_atvevo_prt_prt_cim_id',
               'bizf_reszben_arazott',
               'bizf_lezartbizonylat_tomb',
               'bizf_atado_prt_prt_nev',
               'bizf_atado_prt_prt_cim_hely_nev',
               'bizf_atado_prt_prt_cim_kozterulet',
               'bizf_atvevo_prt_prt_nev',
               'bizf_atvevo_prt_prt_cim_hely_nev',
               'bizf_atvevo_prt_prt_cim_kozterulet',
               'bizf_atvevo_szallaitasi_prt_teljescime',
               'bizf_egyeszrekerekites',
               'bizf_emailout_id',
               'bizf_emailout_cimzett',
               'bizf_emailout_elkuldve',
               'bizf_emailout_elkuldesideje',
               'bizf_prtautsz_nev',
               'bizf_prtautsz_szamlazasinapok',
               'bizf_atvevo_prt_csoportosadoszam',
               'bizf_atado_prt_csoportosadoszam',
               'bizf_azon_szamlazhato',
               'bizf_nvtrt_inv_status_kod_nev',
               'bizf_atvevo_prt_partner_tipus_kod',
               'bizf_atvevo_prt_partner_tipus_kod_nev',
                "bizf_rogziteshelye_kod",
                "bizf_rogziteshelye_kod_nev",
                "bizf_elorogzites_kod",
                "bizf_elorogzites_kod_nev",
                "bizf_fztm_id"
        ];
        $model = '\App\Models\Types\TBizfejek1';
        $bizonylattipus = $request->menuparameter['bizonylattipus'];
        $felhasznalo = UserHelpers::getFelhasznalo($request->felh_userid);
          if ($felhasznalo->felh_cikk_szum_keszlet_kod == '6') {
              $bizonylattipus= $request->menuparameter['bizonylattipus'].' and (bizf_atvevo_prt_id='.$felhasznalo->felh_prt_prt_id.' or bizf_atado_prt_id='.$felhasznalo->felh_prt_prt_id.')';
              If ($request->menuparameter['BIZTIP_KOD'] !='S') {
                  $bizonylattipus = $bizonylattipus . " and bizf_rogziteshelye_kod ='1' ";
              }
          }

        ///\MyArray::toStringToLog($bizonylattipus);
        //$dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$request->menuparameter['bizonylattipus']], true,' order by bizf_esed_datum desc');
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$bizonylattipus], true,' order by bizf_esed_datum desc');
        return response()->success($dmdata);
    }
    public function listprint(Request $request, $size = 200)
    {
        $select = [
            'bizf_id',
            'bizf_azon',
            'bizf_azon_id',
            'bizf_azon_egyseg_prt_id',
            'bizf_azon_prt_nev',
            'bizf_azon_biztip_kod',
            'bizf_azon_biztip_kod_nev',
            'bizf_azon_ev',
            'bizf_atado_prt_id',
            'bizf_atado_prt_nev',
            'bizf_atado_prt_adoszam',
            'bizf_atado_prt_cim_orsz_nev',
            'bizf_atado_prt_cim_helys_megye',
            'bizf_atado_prt_cim_helys_irsz',
            'bizf_atado_prt_cim_helys_nev',
            'bizf_atado_prt_cim_kozterulet_jelleg_kod_nev',
            'bizf_atado_prt_cim_kozterulet_nev',
            'bizf_atado_prt_cim_hazszam',
            'bizf_atado_prt_cim_epulet',
            'bizf_atado_prt_cim_emelet',
            'bizf_atado_prt_cim_lepcsohaz',
            'bizf_atado_prt_cim_ajto',
            'bizf_atado_prt_bsz_bankszamlaszam',
            'bizf_atvevo_prt_id',
            'bizf_atvevo_prt_nev',
            'bizf_atvevo_prt_cim_orsz_nev',
            'bizf_atvevo_prt_cim_helys_megye',
            'bizf_atvevo_prt_cim_helys_irsz',
            'bizf_atvevo_prt_cim_helys_nev',
            'bizf_atvevo_prt_cim_kozterulet_jelleg_kod_nev',
            'bizf_atvevo_prt_cim_kozterulet_nev',
            'bizf_atvevo_prt_cim_hazszam',
            'bizf_atvevo_prt_cim_epulet',
            'bizf_atvevo_prt_cim_lepcsohaz',
            'bizf_atvevo_prt_cim_emelet',
            'bizf_atvevo_prt_cim_ajto',
            'bizf_atvevo_prt_bsz_bankszamlaszam',
            'bizf_atvevo_prt_adoszam',
            'bizf_ukoto_prt_id',
            'bizf_ukoto_prt_nev',
            'bizf_fizeto_prt_id ',
            'bizf_fizeto_prt_nev',
            'bizf_fuv_prt_id',
            'bizf_fuv_prt_nev',
            'bizf_penznem_id',
            'bizf_penznem_nev',
            'bizf_fztm_id',
            'bizf_fizmod_kod_nev',
            'bizf_feldolg_status_kod',
            'bizf_feldolg_status_kod_nev',
            'bizf_biz_szam',
            'bizf_szlev_szam',
            'bizf_szla_szam',
            'bizf_telj_datum',
            'bizf_konyv_datum',
            'bizf_esed_datum',
            'bizf_kif_datuma',
            'bizf_rend_ert',
            'bizf_nyilv_ert',
            'bizf_netto_ert',
            'bizf_brutto_ert ',
            'bizf_tetel_szam',
            'bizf_rendelt_tetel_szam',
            'bizf_leigazolas_datuma',
            'bizf_pufelad_datuma',
            'bizf_eredeti_kinyomtatva',
            'bizf_eredeti_kinyomtatva_kod_nev',
            'bizf_masolat_peldanyszama',
            'bizf_masodlat_peldanyszama',
            'bizf_nyomtatas_szam',
            'bizf_rakt_id',
            'bizf_rakt_nev',
            'bizf_kszk_id',
            'bizf_kszk_nev',
            'bizf_pnztr_id',
            'bizf_pnzn_nev',
            //'bizf_megjegyzes',
            'bizf_letre_felh_nev',
            'bizf_letre_dat',
            'bizf_afa_ertek',
            'bizf_rovidmegjegyzes',
            'bizf_vrend_iktatoszama',
            'bizf_fizetendo_brutto_ert',
            'bizf_atvevoszallitasicim_helyseg',
            'bizf_atvevoszallitasicim_kozterulet',
            'bizf_nem_arazott',
            'bizf_visszavett',
            ' bizf_mulapf_id',
            'bizf_mulapf_munkaszam',
            'bizf_sztorno_bizf_id',
            'bizf_sztornozott_bizf_id',
            'bizf_bizonylatkep',
            'bizf_netto_beszerzesi_ert',
            'bizf_brutto_beszerzesi_ert',
            'bizf_kifizetendo_beszerzesi_ert',
            'bizf_fkod_id',
            'bizf_mozgas_kod',
            'bizf_mozgas_nev',
            'bizf_ervenyeseg',
            'bizf_kifizetett_brutto_ert',
            'bizf_maradekosszeg',
            'bizf_beszerzesi_maradekosszeg',
            'bizf_kifizetes_datuma',
            'bizf_beszerzesi_afa_erteke',
            'bizf_afa_id',
            'bizf_afa_nev',
            'bizf_atvevo_prt_bsz_iban',
            'bizf_atvevo_prt_bank_swift_kod',
            'bizf_atvevo_prt_kozossegiszam',
            'bizf_atado_prt_bsz_iban',
            'bizf_atado_prt_bank_swift_kod',
            'bizf_atado_prt_kozossegiszam',
            'bizf_munhely_megnevezes',
            'bizf_munhely_cim',
            'bizf_mulapf_megnevezese',
            'bizf_nemarazotetelszam as nemarazotetelszam',
            'bizf_bearazotetelszam as bearazotetelszam',
            'bizf_atado_prt_prt_cim_id',
            'bizf_atvevo_prt_prt_cim_id',
            'bizf_reszben_arazott',
            'bizf_lezartbizonylat_tomb',
            'bizf_atado_prt_prt_nev',
            'bizf_atado_prt_prt_cim_hely_nev',
            'bizf_atado_prt_prt_cim_kozterulet',
            'bizf_atvevo_prt_prt_nev',
            'bizf_atvevo_prt_prt_cim_hely_nev',
            'bizf_atvevo_prt_prt_cim_kozterulet',
            'bizf_atvevo_szallaitasi_prt_teljescime',
            'bizf_egyeszrekerekites',
            'bizf_emailout_id',
            'bizf_emailout_cimzett',
            'bizf_emailout_elkuldve',
            'bizf_emailout_elkuldesideje',
            'bizf_prtautsz_nev',
            'bizf_prtautsz_szamlazasinapok',
            'bizf_atvevo_prt_csoportosadoszam',
            'bizf_atado_prt_csoportosadoszam',
            'bizf_azon_szamlazhato',
            'bizf_nvtrt_inv_status_kod_nev',
            'bizf_atvevo_prt_partner_tipus_kod',
            'bizf_atvevo_prt_partner_tipus_kod_nev'
        ];
        $model = '\App\Models\Types\TBizfejek1';
        //  \MyArray::toStringToLog($request->menuparameter['bizonylattipus']);
        $bizonylattipus = $request->menuparameter['bizonylattipus'];
        $felhasznalo = UserHelpers::getFelhasznalo($request->felh_userid);
        if ($felhasznalo->felh_cikk_szum_keszlet_kod == '6') {
            $bizonylattipus= $request->menuparameter['bizonylattipus'].' and (bizf_atvevo_prt_id='.$felhasznalo->felh_prt_prt_id.' or bizf_atado_prt_id='.$felhasznalo->felh_prt_prt_id.')';
        }

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$bizonylattipus], true,' order by bizf_esed_datum desc');
        //$dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$request->menuparameter['bizonylattipus']], true,' order by bizf_esed_datum desc');
        return response()->success($dmdata);
    }
    public function downloadpdf(Request $request,$bizf_id)
    {
        return $this->komplexnode($request,'downloadpdf/'.$bizf_id,'not');
    }
    public function exportinvoice(Request $request,$bizf_id)
    {
        return $this->komplexnode($request,'/exportinvoice/'.$bizf_id,'not');
    }
    public function destroy($id)
    {
        $sql = 'delete from "04_rak".biz_tetelek where bizt_bizf_id = '.$id;
        \DB::select($sql);
        $this->model::destroy($id);
        return response()->success('success');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id)
    {
        $this->model::find($id)->modifyelrogzites('I');
        return response()->success('success');
    }
}
