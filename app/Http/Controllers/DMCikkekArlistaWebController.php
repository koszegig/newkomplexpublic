<?php
namespace App\Http\Controllers;
use App\Models\Types\DMCikkekArlistaWeb;

use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\UserHelpers;
use Input;
use Log;
use Validator;




class DMCikkekArlistaWebController  extends Controller
{
    public function __construct()
    {
        $this->model = App\Models\Types\DMCikkekArlistaWeb::class;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $size = 25)
    {
        $select = ["cik_id ",
            "cik_kod",
            "cik_nev",
            "cik_vtsz_id",
            "cik_vtszafa_afak_kod",
            "cik_vtszafa_afa_nev",
            "cik_vtszafa_vtsz_szam",
            "cik_vtszafa_vtsz_nev",
            "cik_vtszafa_vtsz_szavidos",
            "cik_me_id",
            "cik_me_rovidites",
            "cik_me_nev",
            "cik_aktiv",
            "cik_kep",
            "cik_tipus_kod",
            "cik_tipus_kod_nev",
            "cik_keszlet_mod_kod",
            "cik_keszlet_mod_kod_nev",
            "cik_jovedeki_termek_kod",
            "cik_jovedeki_termek_kod_nev",
            "cik_arlistas_kod",
            "cik_arlistas_kod_nev",
            "cik_szavidos_kod",
            "cik_szavidos_kod_nev",
            "cik_letre_felh_nev",
            "cik_letre_dat",
            "cik_afa_id",
            "cik_afa_nev",
            "cikk_handle",
            "cik_suly",
            "cik_suly_me_id",
            "cik_sulyme_rovidites",
            "cik_sulyme_nev",
            "cik_fixaras_kod",
            "cik_fixaras_kod_nev",
            "cik_beszerzersinettoar::public.a_n_12_2 as cik_beszerzersinettoar",
            "cik_beszerzersibruttoar::public.a_n_12_2 as cik_beszerzersibruttoar",
            "cik_eladasinettoar::public.a_n_12_2 as cik_eladasinettoar",
            "cik_eladasibruttoar::public.a_n_12_2 as cik_eladasibruttoar",
            "cik_pnzn_id",
            "cik_pnzn_rovidites",
            "cik_pnzn_nev",
            "cik_akcios_ar",
            "cik_egyseg_ar",
            "cik_cgy_gyariszam",
            "cik_cgy_id",
            "cik_cgy_bevetel_datuma",
            "cik_cgy_eladas_datuma",
            "cik_cgy_rakt_id",
            "cik_forditottafa",
            "cik_forditottafa_kod_nev",
            "cik_besz_pnzn_rovidites",
            "cik_besz_pnzn_nev",
            "cik_besz_pnzn_id",
            "cik_armeny_szorzo",
            "cik_armeny_me_id",
            "cik_csatolt_dokumentum_path",
            "cik_csatolt_tervrajz",
            "cik_vonal_azon",
            "cik_szigoruszamadasu",
            "cik_lapszam",
            "cik_beszmodidopontja",
            "cik_mpraktkeszl_akt_keszl",
            "cik_mpraktkeszl_min_keszl",
            "cik_min_keszl_alatt",
            "cik_csz_cikkszam",
            "cik_be_rakt_id",
            "cik_be_rakt_nev",
            "cik_ki_rakt_id",
            "cik_ki_rakt_nev",
            "cik_prt_egyenikedvezmeny",
            "cik_mpraktkeszl_fogl_keszl "
        ];
        $model = "App\Models\Types\DMCikkekArlistaWeb";

        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$request->felh_userid]);
        return response()->success($dmdata);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $size
     * @return \Illuminate\Http\Response
     */
    public function listprint(Request $request, $size = 200)
    {
        $select = ["cik_id ",
            "cik_kod",
            "cik_nev",
            "cik_vtsz_id",
            "cik_vtszafa_afak_kod",
            "cik_vtszafa_afa_nev",
            "cik_vtszafa_vtsz_szam",
            "cik_vtszafa_vtsz_nev",
            "cik_vtszafa_vtsz_szavidos",
            "cik_me_id",
            "cik_me_rovidites",
            "cik_me_nev",
            "cik_aktiv",
            "cik_kep",
            "cik_tipus_kod",
            "cik_tipus_kod_nev",
            "cik_keszlet_mod_kod",
            "cik_keszlet_mod_kod_nev",
            "cik_jovedeki_termek_kod",
            "cik_jovedeki_termek_kod_nev",
            "cik_arlistas_kod",
            "cik_arlistas_kod_nev",
            "cik_szavidos_kod",
            "cik_szavidos_kod_nev",
            "cik_letre_felh_nev",
            "cik_letre_dat",
            "cik_afa_id",
            "cik_afa_nev",
            "cikk_handle",
            "cik_suly",
            "cik_suly_me_id",
            "cik_sulyme_rovidites",
            "cik_sulyme_nev",
            "cik_fixaras_kod",
            "cik_fixaras_kod_nev",
            "cik_beszerzersinettoar::public.a_n_12_2 as cik_beszerzersinettoar",
            "cik_beszerzersibruttoar::public.a_n_12_2 as cik_beszerzersibruttoar",
            "cik_eladasinettoar::public.a_n_12_2 as cik_eladasinettoar",
            "cik_eladasibruttoar::public.a_n_12_2 as cik_eladasibruttoar",
            "cik_pnzn_id",
            "cik_pnzn_rovidites",
            "cik_pnzn_nev",
            "cik_akcios_ar",
            "cik_egyseg_ar",
            "cik_cgy_gyariszam",
            "cik_cgy_id",
            "cik_cgy_bevetel_datuma",
            "cik_cgy_eladas_datuma",
            "cik_cgy_rakt_id",
            "cik_forditottafa",
            "cik_forditottafa_kod_nev",
            "cik_besz_pnzn_rovidites",
            "cik_besz_pnzn_nev",
            "cik_besz_pnzn_id",
            "cik_armeny_szorzo",
            "cik_armeny_me_id",
            "cik_csatolt_dokumentum_path",
            "cik_csatolt_tervrajz",
            "cik_vonal_azon",
            "cik_szigoruszamadasu",
            "cik_lapszam",
            "cik_beszmodidopontja",
            "cik_mpraktkeszl_akt_keszl",
            "cik_mpraktkeszl_min_keszl",
            "cik_min_keszl_alatt",
            "cik_csz_cikkszam",
            "cik_be_rakt_id",
            "cik_be_rakt_nev",
            "cik_ki_rakt_id",
            "cik_ki_rakt_nev",
            "cik_mpraktkeszl_fogl_keszl "
        ];
        $model = "App\Models\Types\DMCikkekArlistaWeb";
//        \MyArray::toStringToLog('User->'.$request->felh_userid);
        $dmdata = $this->selectStoredProcedure($model, $request, $select, $size,[$request->felh_userid]);
        return response()->success($dmdata);
    }
    public function exportcikkcsv(Request $request,$file)
    {
        return $this->komplexnode($request,'exportcikkcsv/'.$file,'not');
    }
    public function exportcikkcsvdownload(Request $request,$file)
    {
        $this->komplexnode($request,'exportcikkcsv/'.$file,'not');
        return $this->komplexnode($request,'exportcikkcsvdownload/'.$file,'not');
    }
}
