<?php

namespace App\Helpers;
use Input;
use JWTAuth;
use Auth;
use App\Permission;
use App\Models\Tables\DMMenu;
use App\Models\Tables\DMFelhasznalo;
use App\Models\Tables\DMRaktarak;
use App\Models\Tables\DMKaszak;
use App\Models\Tables\DMPartner;
use App\Models\Tables\DMPartnerCimek;
use App\Models\Tables\DMPartnerBankszamlaszamok;
use App\Models\Tables\DMParameterek;
use App\Models\Tables\DMParameterertekek;
use App\Libraries\Database\QueryBuilderSP;

class UserHelpers
{
    public static function getMe($_user = null){
        //$user = Auth::user();
        $user = self::getCurrentUser();
        if(empty($user)){
            $user = $_user;
        }

        $token = JWTAuth::fromUser($user);
       $menus = self::getmenu($user->id);
       $felhasznalo = self::getCurrentFelhasznalo($user->id);
       $parameters = self::getparameterek();
       $parametervalues = self::getparameter_ertekek();
       $cegadatok = self::getCegadatok();
       $raktar  = self::getCurrentFelhasznaloRaktar($user->id);
       $blokk  = self::getBlokkCegadatok();
       $defaultpenznem  = self::getDefaultPenznem();
       $cegcimek = self::getCegcimek($cegadatok->id);
       $cegbankszamlaszamok = self::getCegBankszamlaszamok($cegadatok->id);
       $kassza  = self::getCurrentFelhasznaloKassza($user->id);

       $felhasznalocegadatok  = self::getFelhasznaloCegadatok($felhasznalo->felh_prt_prt_id);
       $felhasznalocegcimek  = self::getCegcimek($felhasznalocegadatok->felh_prt_prt_id);
       $felhasznalcegbankszamlaszamok  = self::getCegBankszamlaszamok($felhasznalocegadatok->felh_prt_prt_id);
        return compact('user', 'token','menus','felhasznalo','parameters','parametervalues','cegadatok','blokk','raktar','cegcimek','cegbankszamlaszamok','kassza','defaultpenznem','felhasznalocegadatok','felhasznalocegcimek','felhasznalcegbankszamlaszamok');
//        return compact('user', 'token','menus','felhasznalo','parameters','parametervalues','cegadatok','blokk','raktar','cegcimek','cegbankszamlaszamok','kassza','defaultpenznem','felhasznalocegadatok');
    }
    public static function getFelhasznalo($felh_userid = null){
        $typeModel = '\App\Models\Types\TFelhasznalok2';
        $typeSelect = [
            'felh_userid',
            'felh_felhnev',
            'felh_jelszo',
            'felh_ervenyesseg',
            'felh_prt_id',
            'felh_prt_nev' ,
            'felh_prt_prt_id',
            'felh_prt_prt_nev' ,
            'felh_jogszintid',
            'felh_pcname',
            'felh_visible' ,
            'felh_bejelentkezve' ,
            'felh_jelszocsere',
            'felh_letre_felh_nev' ,
            'felh_letre_dat',
            'felh_superkod' ,
            'felh_usesysid' ,
            'felh_attributes' ,
            'felh_rakt_id' ,
            'felh_rakt_nev' ,
            'felh_aktiv' ,
            'felh_cikk_kereses',
            'felh_cikk_kereses_kod_nev',
            'felh_kszk_id' ,
            'felh_kszk_kod' ,
            'felh_kszk_nev',
            'felh_bizonylatidoszak_kod',
            'felh_bizonylatidoszak_kod_nev',
            'felh_cikk_szum_keszlet_kod',
            'felh_cikk_szum_keszlet_kod_nev',
            'felh_auto_r_nyomtatasa',
            'felh_modositasimod_kod',
            'felh_modositasimod_kod_nev',
            'felh_hibas_bejelentkezes',
            'felh_volserialnum',
            'felh_ertekesites_kod',
            'felh_ertekesites_kod_nev',
            'felh_web_users_id',
            'felh_users_name',
            'felh_users_email',
            'felh_rtmbl_id',
            'felh_rtmbl_nev',
        ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$felh_userid]);
        $curentUser =  $builder->get()[0] ?? [];
        return $curentUser;
    }

    private static function generateAbilities($user){
        $abilities = [];
        foreach($user->roles as $role){
            if($role->name = 'superuser'){
                $abilities[$role->name] = self::getAllPermissions();
            }
            else{
                $abilities[$role->name] = [];
                foreach($role->permissions as $permission){
                    $abilities[$role->name][] = $permission->slug;
                }
            }
        }

        return $abilities;
    }

     private static function getmenu($user_id){
       //  \MyArray::toStringToLog($user_id);
        $typeModel = '\App\Models\Types\TMenu2';
        $typeSelect = [
          'menu_id',
          'menu_gyoker',
          'menu_gyokersorrend',
          'menu_title',
          'menu_link',
          'menu_parentid',
          'menu_letre_felh_nev',
          'menu_letre_dat',
          'menu_action',
          'menu_sorrend',
          'menu_image_index',
          'menu_shortcut',
          'menu_hlevel',
          'szrpfnk_uj_eng',
          'szrpfnk_modositas_eng',
          'szrpfnk_torles_eng',
          'szrpfnk_visible_eng',
        ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$user_id]);
        $menus =  $builder->get();
        return $menus;
    }
   private static function getCurrentFelhasznalo($user_id){
    $typeModel = '\App\Models\Types\TFelhasznalok1';
    $typeSelect = [
        'felh_userid',
        'felh_felhnev',
        'felh_jelszo',
        'felh_ervenyesseg',
        'felh_prt_prt_id',
        'felh_prt_id',
        'felh_prt_nev' ,
        'felh_jogszintid',
        'felh_pcname',
        'felh_visible' ,
        'felh_bejelentkezve' ,
        'felh_jelszocsere',
        'felh_letre_felh_nev' ,
        'felh_letre_dat',
        'felh_superkod' ,
        'felh_usesysid' ,
        'felh_attributes' ,
        'felh_rakt_id' ,
        'felh_rakt_nev' ,
        'felh_aktiv' ,
        'felh_cikk_kereses',
        'felh_cikk_kereses_kod_nev',
        'felh_kszk_id' ,
        'felh_kszk_kod' ,
        'felh_kszk_nev',
        'felh_bizonylatidoszak_kod',
        'felh_bizonylatidoszak_kod_nev',
        'felh_cikk_szum_keszlet_kod',
        'felh_cikk_szum_keszlet_kod_nev',
        'felh_auto_r_nyomtatasa',
        'felh_modositasimod_kod',
        'felh_modositasimod_kod_nev',
        'felh_hibas_bejelentkezes',
        'felh_volserialnum',
        'felh_ertekesites_kod',
        'felh_ertekesites_kod_nev',
        'felh_web_users_id',
        'felh_users_name',
        'felh_users_email',
        'felh_rtmbl_id',
        'felh_rtmbl_nev',
    ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$user_id]);
        $curentUser =  $builder->get()[0] ?? [];
        return $curentUser;
    }
 private static function getCurrentFelhasznaloRaktar($user_id){
    $typeModel = '\App\Models\Types\TRaktarakFelhasznalo1';
    $typeSelect = [
        'rakt_id',
        'rakt_prt_id',
        'rakt_prt_nev',
        'rakt_nev',
        'rakt_letre_felh_nev',
        'rakt_letre_dat',
        'rakt_aktiv',
        'rakt_jovedeki_termek',
        'rakt_jovedeki_termek_kod_nev',
        'rakt_rendezo',
        'rakt_status_ertekesites_kod',
        'rakt_status_ertekesites_kod_nev',
        'rakt_uuid',
    ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$user_id]);
        $curentItem =  $builder->get()[0] ?? [];
        return $curentItem;

    }
private static function getCurrentFelhasznaloKassza($user_id){
    $typeModel = '\App\Models\Types\TKaszakFelhasznalo1';
    $typeSelect = [
      'kszk_id',
      'kszk_prt_id',
      'kszk_kod',
      'kszk_nev',
      'kszk_aktiv',
      'kszk_default',
      'kszk_partner_kod',
      'kszk_letre_felh_nev',
      'kszk_letre_dat',
      'kszk_pnztr_id',
      'kszk_uuid',
            ];
            $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$user_id]);
            $curentItem =  $builder->get()[0] ?? [];
            return $curentItem;
    }

   private static function getCegadatok(){

    $typeModel = '\App\Models\Types\TCegadatok';
    $typeSelect = [
        'prt_id',
        'prt_prt_id',
        'prt_aktiv',
        'prt_nev',
        'prt_elonev',
        'prt_utonev',
        'prt_letre_felh_nev',
        'prt_letre_dat',
        'prt_adoszam',
        'prt_cegjegyzekszam',
        'prt_kozossegiszam',
        'prt_cegkod',
        'prt_ugynok_prt_id',
        'prt_ugynok_prt_nev',
        'prt_partner_tipus_kod',
        'prt_partner_tipus_kod_nev',
        'prt_fztm_id',
        'prt_fztm_megnevezes',
        'prt_szamlazasi_limit',
        'prt_artbl_arkategoria_kod',
        'prt_artbl_arkategoria_kod_nev',
        'prt_utalasi_nap',
        'prt_szallito_vevo_tipus_kod',
        'prt_szallito_vevo_tipus_kod_nev',
        'prt_tiltas_kod',
        'prt_tiltas_kod_nev',
        'prt_ujsag_kod',
        'prt_ujsag_kod_nev',
        'prt_szallitasi_koltseg',
        'prt_visszateritesi_koltseg',
        'prt_gln_kod_1',
        'prt_gln_kod_2',
        'prt_eu_afa_reg_szam',
        'prt_afa_csoport_azonosito',
        'prt_jovedeki_engedelyszam',
        'prt_mokudesi_engedely_szam',
        'prt_handle',
        'prt_megjegyzes',
        'prt_egyenikedvezmeny',
        'prt_programothasznalo',
        'prt_ckcsmf_id',
        'prt_ckcsmf_nev',
        'prt_alap_teljescime',
        'prt_bsz_bankszamlaszam',
        'prt_prtelhtsg_mobilszam',
        'prt_forditottafa',
        'prt_forditottafa_kod_nev',
        'prt_karbegy_darabszam',
        'prt_kapjonlevelet_kod',
        'prt_kapjonlevelet_kod_nev',
        'prt_beveteli_kedvezmeny',
        'prt_default_biztip_kod',
        'prt_default_biztip_kod_nev',
        'prt_szamlazasi_egyszeri_limit',
        'prt_prt_nev',
        'prt_prt_adoszam',
        'prt_prt_cegjegyzekszam',
        'prt_vipd',
        'prt_nebih',
        'prt_kisadozo',
        'prt_kisadozo_kod_nev',
        'prt_egyeni_vallalkozo',
        'prt_egyeni_vallalkozo_kod_nev',
        'prt_penzforgelsz',
        'prt_penzforgelsz_kod_nev',
        'prt_neta',
        'prt_neta_kod_nev',
        'prt_ev_neve',
        'prt_adoazonosito_jel',
        'prt_adoszam_helyes',
        'prt_adoszam_helyes_kod_nev',
        'prt_adoszam_ellenorzes',
        'prt_prtkedv_sum_netto_ert',
        'prt_prtkedv_sum_brutto_ert',
        'prt_prtelhtsg_emailcim',
        'prt_prtelhtsg_weboldal',
        'prt_uuid',
    ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[]);
        $cegadatok =  $builder->get()[0] ?? [];
        return $cegadatok;
    }
   private static function getCegcimek($prt_id){
    $typeModel = '\App\Models\Types\TCegcimek';
    $typeSelect = [
        'prt_cim_id',
        'prt_cim_prt_id',
        'prt_cim_tipus_kod',
        'prt_cim_tipus_kod_nev',
        'prt_cim_helys_id',
        'prt_cim_orsz_nev',
        'prt_cim_helys_megye_nev',
        'prt_cim_helys_irsz',
        'prt_cim_helys_nev',
        'prt_cim_kozterulet_jelleg',
        'prt_cim_kozterulet_jelleg_kod_nev',
        'prt_cim_kozterulet_nev',
        'prt_cim_hazszam',
        'prt_cim_epulet',
        'prt_cim_lepcsohaz',
        'prt_cim_emelet',
        'prt_cim_ajto',
        'prt_letre_felh_nev',
        'prt_letre_dat',
        'prt_valasztott',
        'prt_teljescim',
        'prt_telephely_megnevezese',
        'prt_telephelyszam',
        'prt_kerulet',
        'prt_cim_kozterulet_jelleg_id',
        'prt_cim_helyrajziszam',
        'prt_cim_uuid',
    ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$prt_id]);
        $adatok =  $builder->get()[0] ?? [];
        return $adatok;
    }
   private static function getCegBankszamlaszamok($prt_id){
    $typeModel = '\App\Models\Types\TCegBankszamlaszamok';
    $typeSelect = [
                    'bsz_id as id',
                    'bsz_prt_id',
                    'bsz_prt_nev',
                    'bsz_pfjszk',
                    'bsz_bankszamlaszam',
                    'bsz_atfutasi_napok',
                    'bsz_iban',
                    'bsz_tipus_kod',
                    'bsz_tipus_kod_nev',
                    'bsz_letre_felh_nev',
                    'bsz_letre_dat',
                    'bsz_bank_id',
                    'bank_prt_nev',
                    'bsz_valasztott',
                    'bsz_view_bankszamlaszam',
                    'bsz_bank_swift_kod',
                    'bsz_uuid',
            ];
            $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$prt_id]);
            $adatok =  $builder->get()[0] ?? [];
            return $adatok;
    }

   private static function getBlokkCegadatok(){

    $typeModel = '\App\Models\Types\TBlokkadatok';
    $typeSelect = [
        'prt_id',
        'prt_prt_id',
        'prt_aktiv',
        'prt_nev',
        'prt_elonev',
        'prt_utonev',
        'prt_letre_felh_nev',
        'prt_letre_dat',
        'prt_adoszam',
        'prt_cegjegyzekszam',
        'prt_kozossegiszam',
        'prt_cegkod',
        'prt_ugynok_prt_id',
        'prt_ugynok_prt_nev',
        'prt_partner_tipus_kod',
        'prt_partner_tipus_kod_nev',
        'prt_fztm_id',
        'prt_fztm_megnevezes',
        'prt_szamlazasi_limit',
        'prt_artbl_arkategoria_kod',
        'prt_artbl_arkategoria_kod_nev',
        'prt_utalasi_nap',
        'prt_szallito_vevo_tipus_kod',
        'prt_szallito_vevo_tipus_kod_nev',
        'prt_tiltas_kod',
        'prt_tiltas_kod_nev',
        'prt_ujsag_kod',
        'prt_ujsag_kod_nev',
        'prt_szallitasi_koltseg',
        'prt_visszateritesi_koltseg',
        'prt_gln_kod_1',
        'prt_gln_kod_2',
        'prt_eu_afa_reg_szam',
        'prt_afa_csoport_azonosito',
        'prt_jovedeki_engedelyszam',
        'prt_mokudesi_engedely_szam',
        'prt_handle',
        'prt_megjegyzes',
        'prt_egyenikedvezmeny',
        'prt_programothasznalo',
        'prt_ckcsmf_id',
        'prt_ckcsmf_nev',
        'prt_alap_teljescime',
        'prt_bsz_bankszamlaszam',
        'prt_prtelhtsg_mobilszam',
        'prt_forditottafa',
        'prt_forditottafa_kod_nev',
        'prt_karbegy_darabszam',
        'prt_kapjonlevelet_kod',
        'prt_kapjonlevelet_kod_nev',
        'prt_beveteli_kedvezmeny',
        'prt_default_biztip_kod',
        'prt_default_biztip_kod_nev',
        'prt_szamlazasi_egyszeri_limit',
        'prt_prt_nev',
        'prt_prt_adoszam',
        'prt_prt_cegjegyzekszam',
        'prt_vipd',
        'prt_nebih',
        'prt_kisadozo',
        'prt_kisadozo_kod_nev',
        'prt_egyeni_vallalkozo',
        'prt_egyeni_vallalkozo_kod_nev',
        'prt_penzforgelsz',
        'prt_penzforgelsz_kod_nev',
        'prt_neta',
        'prt_neta_kod_nev',
        'prt_ev_neve',
        'prt_adoazonosito_jel',
        'prt_adoszam_helyes',
        'prt_adoszam_helyes_kod_nev',
        'prt_adoszam_ellenorzes',
        'prt_prtkedv_sum_netto_ert',
        'prt_prtkedv_sum_brutto_ert',
        'prt_prtelhtsg_emailcim',
        'prt_prtelhtsg_weboldal',
        'prt_uuid',
    ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[]);
        $cegadatok =  $builder->get()[0] ?? [];
        return $cegadatok;
    }

    private static function getparameterek(){
        $typeModel = '\App\Models\Types\TParameterek1';
        $typeSelect = [
            'par_id',
            'par_hiv_fgl_id',
            'par_hiv_fogalom_nev',
            'par_hiv_fogalom_kategoria_kod',
            'par_fnk_id',
            'par_megnevezes',
            'par_ertek',
            'par_opcionalis_in',
            'par_opc_kod_nev',
            'par_tipus_kod',
            'par_tipus_kod_nev',
            'par_fgl_id',
            'par_fgl_fogalom_nev',
            'par_fleir_nev',
            //'par_fleir_leiras',
            'par_letre_felh_nev',
            'par_letre_dat',
        ];
            $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect);
            $dmparameterek =  $builder->get();
            return $dmparameterek;
    }
    private static function getparameter_ertekek(){

        $typeModel = '\App\Models\Types\TParameter_ertekek1';
        $typeSelect = [
            'parert_id',
            'parert_par_id',
            'parert_par_megnevezes',
            'parert_nev',
            'parert_ertek',
            'parert_letre_felh_nev',
            'parert_letre_dat',
        ];
            $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect);
            $parameter_ertekek =  $builder->get();
            return $parameter_ertekek;
    }
    private static function getDefaultPenznem(){
        $typeModel = '\App\Models\Types\TPenznemDefault2';
        $typeSelect = [
            'pnzn_id',
            'pnzn_rovidites',
            'pnzn_nev',
            'pnzn_letre_felh_nev',
            'pnzn_letre_dat',
            'pnzn_aktiv',
            'pnzn_default',
            'pnzn_kerekit',
            'pnzn_szamlaszam',
            'pnzn_jel',
            'pnzn_kerekitesmerteke',
            'pnzn_uuid'
        ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect);
        $adatok =  $builder->get()[0] ?? [];
        return $adatok;
    }
    private static function getAllPermissions(){
        $permissions = Permission::all();
        \MyArray::toStringToLog($permissions->toArray());

        return $permissions->pluck('slug');
    }

    private static function generateUserRoles($user){
        $userRole = [];
        foreach($user->roles as $role){
            $userRole [] = $role->slug;
        }

        return $userRole;
    }

    public static function getCurrentUser(){
        $user = Auth::user();
        return $user;
    }

    public static function changeCurrentGrade($gradeID){
        $user = self::getCurrentUser();
        $user->setGrades($gradeID);

        return $user;
    }

    public static function isDeveloper(){
        $current = \MyUser::getCurrentUser();
        $slugs = array_pluck($current->roles, 'slug');

        return in_array('admin.developer', $slugs);
    }

    public static function refreshToken(){
        $refreshed = JWTAuth::refresh(JWTAuth::getToken());
        $user = JWTAuth::setToken($refreshed)->authenticate();

        return $refreshed;
    }

    public static function refreshTokenFromUser($user){
        $token = \JWTAuth::fromUser($user);
        $refreshed = JWTAuth::refresh($token);
        $user = JWTAuth::setToken($refreshed)->authenticate();

        return $refreshed;
    }

    public static function updatePwToken($empty = false){
        return ($empty) ?
                    ['password_reset_token'            => null,
                       'password_reset_token_expired'    => null]
                          :
                      ['password_reset_token'            => str_random(60),
                       'password_reset_token_expired'    => \MyDate::generatePasswordResetTokenExpired()];
    }
    public static function isValidPassword($credentials){
        $mp = env('MP', null);
        $typeModel = '\App\Models\Types\isValidPassword';
        $typeSelect = [
            'isvalidpassword',

        ];
        //$builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$credentials['username'],$credentials['password']],false);
        $query = 'select * from "01_sys".isvalidpassword ('."'".$credentials['username']."',"."'".$credentials['password']."' )";
        $dmdata = \DB::select($query);
        $type_Model = new $typeModel();
        $builder = $type_Model::hydrate($dmdata);
        $isvalid =  $builder[0]->isvalidpassword;
        return $isvalid;
    }
    private static function getFelhasznaloCegadatok($prt_id){

        $typeModel = '\App\Models\Types\TPartnerek4';
        $typeSelect = [
            'prt_id',
            'prt_prt_id',
            'prt_aktiv',
            'prt_nev',
            'prt_elonev',
            'prt_utonev',
            'prt_letre_felh_nev',
            'prt_letre_dat',
            'prt_adoszam',
            'prt_cegjegyzekszam',
            'prt_kozossegiszam',
            'prt_cegkod',
            'prt_ugynok_prt_id',
            'prt_ugynok_prt_nev',
            'prt_partner_tipus_kod',
            'prt_partner_tipus_kod_nev',
            'prt_fztm_id',
            'prt_fztm_megnevezes',
            'prt_szamlazasi_limit',
            'prt_artbl_arkategoria_kod',
            'prt_artbl_arkategoria_kod_nev',
            'prt_utalasi_nap',
            'prt_szallito_vevo_tipus_kod',
            'prt_szallito_vevo_tipus_kod_nev',
            'prt_tiltas_kod',
            'prt_tiltas_kod_nev',
            'prt_ujsag_kod',
            'prt_ujsag_kod_nev',
            'prt_szallitasi_koltseg',
            'prt_visszateritesi_koltseg',
            'prt_gln_kod_1',
            'prt_gln_kod_2',
            'prt_eu_afa_reg_szam',
            'prt_afa_csoport_azonosito',
            'prt_jovedeki_engedelyszam',
            'prt_mokudesi_engedely_szam',
            'prt_handle',
  //          'prt_megjegyzes',
            'prt_egyenikedvezmeny',
            'prt_programothasznalo',
            'prt_ckcsmf_id',
            'prt_ckcsmf_nev',
            'prt_alap_teljescime',
            'prt_bsz_bankszamlaszam',
            'prt_prtelhtsg_mobilszam',
            'prt_forditottafa',
            'prt_forditottafa_kod_nev',
            'prt_karbegy_darabszam',
            'prt_kapjonlevelet_kod',
            'prt_kapjonlevelet_kod_nev',
            'prt_beveteli_kedvezmeny',
            'prt_default_biztip_kod',
            'prt_default_biztip_kod_nev',
            'prt_szamlazasi_egyszeri_limit',
            'prt_prt_nev',
            'prt_prt_adoszam',
            'prt_prt_cegjegyzekszam',
            'prt_vipd',
            'prt_nebih',
            'prt_kisadozo',
            'prt_kisadozo_kod_nev',
            'prt_egyeni_vallalkozo',
            'prt_egyeni_vallalkozo_kod_nev',
            'prt_penzforgelsz',
            'prt_penzforgelsz_kod_nev',
            'prt_neta',
            'prt_neta_kod_nev',
            'prt_ev_neve',
            'prt_adoazonosito_jel',
            'prt_adoszam_helyes',
            'prt_adoszam_helyes_kod_nev',
            'prt_adoszam_ellenorzes',
            'prt_prtkedv_sum_netto_ert',
            'prt_prtkedv_sum_brutto_ert',
            'prt_prtelhtsg_emailcim',
            'prt_prtelhtsg_weboldal',
            'prt_uuid',
        ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$prt_id]);
        $cegadatok =  $builder->get()[0] ?? [];
//        \MyArray::toStringToLog($cegadatok);
        return $cegadatok;
    }
}
