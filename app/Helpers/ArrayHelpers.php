<?php

namespace App\Helpers;

class ArrayHelpers
{

    public static function toString($array)
    {
        echo('<pre>');
        print_r($array);
        echo('</pre>');
    }

    public static function toStringToLog($array)
    {
        \Log::debug(print_r($array, true));
    }
    public static function toStringToLAll($array)
    {
        \Log::debug(print_r($array, true));
        dump($array);
    }


    public static function finObjectByProp($prop, $propVal, $objectArray)
    {
        foreach($objectArray as $object){
            if ($propVal == $object->{$prop}) { return $object;
            }
        }
        return false;
    }

    public static function finElementyByElement($key, $val, $Array)
    {
        foreach($Array as $element){
            if ($val == $element[$key]) { return $element;
            }
        }
        return false;
    }

    public static function findElementBypElements($props, $objectArray)
    {
        foreach($objectArray as $object) {
            $equel = 0;
            foreach ($props as $prop => $propVal) {
                if ($propVal != $object[$prop]) { continue(2);
                } else { $equel++;
                }
            }
            if($equel == count($props)) { return $object;
            }
        }
        return false;
    }

    public static  function element($key, $array, $default = false)
    {
        return (is_array($array) && array_key_exists($key, $array)) ? $array[$key] : $default;
    }

    public static  function addElement($key, $val, &$array, $forceNull = false)
    {
        if(!$forceNull && !is_null($val)) { $data[$key] = $val;
        }
    }

    public static function elements($keys, $array, $default = false)
    {
        $return = [];
        is_array($keys) or $keys = [$keys];

        foreach ($keys as $key) {
            $return[$key] = self::element($key, $array, $default);
        }

        return $return;
    }

    public static function unset(&$array, $elem)
    {
        if (($key = array_search($elem, $array)) !== false) {
            unset($array[$key]);
        }
    }

    public static function multiunset(&$array, $elems)
    {
        is_array($elems) or $elems = [$elems];
        foreach($elems as $elem) {
            self::unset($array, $elem);
        }
    }

    public static function isAssoc(array $arr)
    {
        if (array() === $arr) { return false;
        }
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public static function assocToArray($array)
    {
        $arr = [];
        foreach($array as $key => $value){
            if($value == null || $value == '') { continue;
            }
            $arr[] = [$key, $value];
        }
        return $arr;
    }
}
