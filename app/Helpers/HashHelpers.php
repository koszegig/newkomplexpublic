<?php

namespace App\Helpers;


use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class HashHelpers
{
	public static function reqSignhash($requestID, $timestamp, $signingKey, $base64 = null){
		$hash = $requestID . $timestamp['cleaned'] . $signingKey;
		if(!is_null($base64)){
			$hash .= self::reqSignCRC32($base64);
		}

		return strtoupper(hash('sha512', $hash));
	}

	public static function reqSignCRC32($base64){
		$hash = "";
		foreach($base64 as $value){
			$hash .= self::crc32($value);
		}

		return $hash;
	}

	public static function crc32($string){
		return sprintf("%u", crc32($string));
	}

	public static function decryptToken($token, $user = null){
		if(is_null($user)){
			$user = property('n_user',\MyUser::getCurrentUser());
		}
		$key = base64_decode($user->exchange_key); //die();

		return self::aes128_decrypt($token, $key);
	}

	public static function aes128_decrypt($string, $key){
		return openssl_decrypt($string,'AES-128-ECB', $key);
	}

	public static function genRequestID(){
		$uuid4 = Uuid::uuid4();
		$maxLength = 20;
		$numbers = self::getmiddle($maxLength);
		$uuid = \MyString::clean3($uuid4->toString());
		$uuidpref = substr($uuid, 0, $numbers[0]);
		$uuidsubf = substr($uuid, $numbers[1] * (-1));

		return $uuidpref . $uuidsubf;
	}

	private static function getmiddle($number){
		$middle = $number / 2;
		$low = floor($middle);
		$up = ceil($middle);

		return [$low, $up, $low + $up, $number];
	}
}
