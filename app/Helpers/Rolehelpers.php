<?php

namespace App\Helpers;
use Input;
use App\Role;
use App\UserRoles;

class Rolehelpers
{
 	/**
     * Get all roles and their corresponding permissions.
     *
     * @return array
     */
    public static function getRolesAbilities(){
        $abilities = [];
        $roles = Role::all();
        foreach($roles as $role){
            if(!empty($role->slug)){
                $abilities[$role->slug] = [];
                $rolePermission = $role->permissions()->get();

                foreach($rolePermission as $permission){
                    if(!empty($permission->slug)){
                        array_push($abilities[$role->slug], $permission->slug);
                    }
                }
            }
        }

        return $abilities;
    }
}
