<?php

namespace App\Helpers;
use Input;
use Artisan;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class ApiXmlFilehelpers {

	public static function writeXmltofile($prefix,$data,$invoiceNr,$requestID,$action){

		$writefile =  env('XMLTOFILE', false);
		$mode =  env('NAVMODE', 'test');
		if(!$writefile) return;
		$timestamp = self::getTimestamp();
		$uuid4 = Uuid::uuid4();
		$filename = "{$timestamp}_{$prefix}_{$action}_{$uuid4}.xml";
		$invoiceNr = \MyString::clean4($invoiceNr);
	    \MyFile::writetofile("xmls/{$mode}/{$invoiceNr}/{$action}/{$requestID}",$filename,$data);
  	}

  	private static function getTimestamp(){
    	$date = \MyDate::now();
        return $date->format('Y_m_d_H_i_s');
    }
}
