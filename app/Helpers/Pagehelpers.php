<?php

namespace App\Helpers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class Pagehelpers
{


    public static function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {
        $numberParameter = config('json-api-paginate.number_parameter');
        $page = $page ?: ((int)request()->input('page.' . $numberParameter) ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);
        $itemscount = isset($items[0]['totalpage']) ? $items[0]['totalpage'] : $items->count();

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $itemscount, $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap->values(),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'per_page' => $lap->perPage(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
}
