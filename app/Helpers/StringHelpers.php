<?php

namespace App\Helpers;

class StringHelpers
{

	// string helper functions

	// Splits a string on a given setpoint, then returns what is before
	// or after the setpoint. You can include or exclude the setpoint.
	public static function split_string($string, $setpoint, $beforaft, $incorexc)
	{
		$lowercasestring = strtolower($string);
		$marker = strtolower($setpoint);

		if ($beforaft == 'before') {  // Return text before the setpoint
			if ($incorexc == 'exclude') {
				// Return text without the setpoint
				$split_here = strpos($lowercasestring, $marker);
			} else {
				// Return text and include the setpoint
				$split_here = strpos($lowercasestring, $marker) + strlen($marker);
			}
			$result_string = substr($string, 0, $split_here);
		} else {  // Return text after the setpoint
			if ($incorexc == 'exclude') {
				// Return text without the setpoint
				$split_here = strpos($lowercasestring, $marker) + strlen($marker);
			} else {
				// Return text and include the setpoint
				$split_here = strpos($lowercasestring, $marker);
			}
			$result_string = substr($string, $split_here, strlen($string));
		}
		return $result_string;
	}

	// Finds a string between a given start and end point. You can include
	// or exclude the start and end point
	public static function find_between($string, $start, $end, $incorexc)
	{
		$temp = self::split_string($string, $start, 'after', $incorexc);
		return self::split_string($temp, $end, 'before', $incorexc);
	}

	// Uses a regular expression to find everything between a start
	// and end point.
	public static function find_all($string, $start, $end)
	{
		preg_match_all("($start(.*)$end)siU", $string, $matching_data);
		return $matching_data[0];
	}

	// Uses str_replace to remove any unwanted substrings in a string
	// Includes the start and end
	public static function delete($string, $start, $end)
	{
		// Get array of things that should be deleted from the input string
		$delete_array = self::find_all($string, $start, $end);

		// delete each occurrence of each array element from string;
		for ($i = 0; $i < count($delete_array); $i++)
			$string = str_replace($delete_array, "", $string);

		return $string;
	}

	public static function clean($string)
	{
		//$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		return $string;
		//return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}



	public static function clean3($string)
	{
		//$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
		return $string;
		//return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}

	public static function clean4($string)
	{
		//$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9]/', '_', $string); // Removes special chars.
		return $string;
		//return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}

	public static function clean1($string)
	{
		//$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^0-9]/', '', $string); // Removes special chars and letters.
		//return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
		return $string;
	}

	public static  function isJSON($string)
	{
		return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

	public static function getNumber($string)
	{
		return intval(preg_replace('/[^0-9]+/', '', $string), 10);
	}

	public static function dateParse($string)
	{
		return date_parse($string);
	}

	public static function changeVowelToShort($string)
	{
		$CHARMAP = array(
			'ö' => 'o',
			'Ö' => 'O',
			'ó' => 'o',
			'Ó' => 'O',
			'ő' => 'o',
			'Ő' => 'O',
			'ú' => 'u',
			'Ú' => 'U',
			'ű' => 'u',
			'Ű' => 'U',
			'ü' => 'u',
			'Ü' => 'U',
			'á' => 'a',
			'Á' => 'A',
			'é' => 'e',
			'É' => 'E',
			'í' => 'i',
			'Í' => 'I',
		);

		return strtr($string, $CHARMAP);
	}

	public static function clean2($string)
	{
		$CHARMAP = array(
			'kft' => '',
			'kft.' => '',
			'bt' => '',
			'bt.' => '',
			'zrt' => '',
			'zrt.' => '',
			'nyrt' => '',
			'nyrt.' => '',
		);

		return strtr($string, $CHARMAP);
	}

	public static function generatePattern($length = 3)
	{
		$letters = ['a', 'á', 'b', 'c', 'cs', 'd', 'dz', 'dzs', 'e', 'é', 'f', 'g', 'gy', 'h', 'i', 'í', 'j', 'k', 'l', 'ly', 'm', 'n', 'ny,o', 'ó', 'ö', 'ő', 'p', 'q', 'r', 's', 'sz', 't', 'ty', 'u', 'ú', 'ü', 'ű', 'v', 'w', 'x', 'y', 'z', 'zs', '1', '2', '3', '4', '5', '6', '7', '8', '9', '@', '.', '_', '-', '?', '!'];
		$_letters = [];
		$pattern = '';
		self::_generatePattern($letters, $_letters, $pattern, $length);
		return $_letters;
	}

	private static function _generatePattern($letters, &$_letters, $_pattern, $length)
	{
		$pattern = $_pattern;
		foreach ($letters as $letter) {
			$pattern .= $letter;
			//$_letters[] = $pattern; continue;
			//\Log::debug("Pattern:".$pattern."/length:".mb_strlen($pattern)."/maxlength:".$length);
			if (mb_strlen($pattern) >= $length) {
				if (mb_strlen($pattern) == $length && !in_array($pattern, $_letters)) $_letters[] = $pattern;
				$pattern = $_pattern;
				continue;
			}
			self::_generatePattern($letters, $_letters, $pattern, $length);
		}
		return;
	}

	public static function generateTokenString()
	{
		$string = str_random(40);
		return bcrypt($string);
	}

	public static function isUUID($string)
	{
		$UUIDv4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
		return (bool)preg_match($UUIDv4, $string);
	}

	public static function isEmail($string)
	{
		if (strpos($string, '@') !== false) {
			$split = explode('@', $string);
			return (strpos($split['1'], '.') !== false ? true : false);
		} else {
			return false;
		}
	}

	public static function byteToString($resource)
	{
		if (($resource instanceof string)) return $resource;
		$my_bytea = stream_get_contents($resource);
		$my_string = pg_unescape_bytea($my_bytea);
		return htmlspecialchars($my_string);
	}
}
