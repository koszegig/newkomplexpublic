<?php
namespace App\Helpers;

use Carbon\Carbon;

class DateHelpers
{

    public static  function now()
    {
        return Carbon::now();
    }

    public static  function today()
    {
        return Carbon::today();
    }

    public static  function date($date)
    {
        $date = \MyString::dateParse($date);
        //\MyArray::toStringToLog($date);
        return Carbon::create($date['year'], $date['month'], $date['day'], $date['hour'], $date['minute'], $date['second']);
    }

    public static function subWeek($date, $amount)
    {
        switch($amount){
          case 0:
                return $date;
          case 1:
                return $date->subWeek();
          default:
                return $date->subWeeks($amount);
        }
    }

    public static function addWeek($date, $amount)
    {
        switch($amount){
          case 0:
                return $date;
          case 1:
                return $date->addWeek();
          default:
                return $date->addWeeks($amount);
        }
    }

    public static function subDay($date, $amount)
    {
        switch($amount){
          case 0:
                return $date;
          case 1:
                return $date->subDay();
          default:
                return $date->subDays($amount);
        }
    }

    public static function addDay($date, $amount)
    {
        switch($amount){
          case 0:
                return $date;
          case 1:
                return $date->addDay();
          default:
                return $date->addDays($amount);
        }
    }

    public static function subMinute($date, $amount)
    {
        switch($amount){
          case 0:
                return $date;
          case 1:
                return $date->subMinute();
          default:
                return $date->subMinutes($amount);
        }
    }

    public static function addMinute($date, $amount)
    {
        switch($amount){
          case 0:
                return $date;
          case 1:
                return $date->addMinute();
          default:
                return $date->addMinutes($amount);
        }
    }



    public static function subHour($date, $amount)
    {
        switch($amount){
          case 0:
                return $date;
          case 1:
                return $date->subHour();
          default:
                return $date->subHours($amount);
        }
    }

    public static function addHour($date, $amount)
    {
        switch($amount){
          case 0:
                return $date;
          case 1:
                return $date->addHour();
          default:
                return $date->addHours($amount);
        }
    }

    public static function generateDate($date)
    {
        switch($date){
          case 'now':
                return self::now();
          case 'today':
                return self::today();
          default:
                return self::date($date);
        }
    }

    public static function calcDiffRange($from = 'now', $mode = 'add', $unite = 'minute', $amount = 1, $tobase = 'from')
    {
        $date1 = null;
        $date2 = null;
        $function = $mode.ucfirst($unite);
        $date = self::generateDate($from);
        $date1 = ($tobase == 'from') ? clone $date : self::generateDate($tobase);
        $date2 = call_user_func_array(['self', $function], [$date,$amount]);
        return ($mode == 'add') ? [$date1->toDateTimeString(),$date2->toDateTimeString()] : [$date2->toDateTimeString(),$date1->toDateTimeString()];
    }

    public static function comparsion($date1, $date2, $mode = 'equals')
    {
        switch($mode){
          case 'equals':
                return $date1->eq($date2);
          case 'notequals':
                return $date1->ne($date2);
          case 'greatherthan':
                return $date1->gt($date2);
          case 'greatherthanorequals':
                return $date1->gte($date2);
          case 'lessthan':
                return $date1->lt($date2);
          case 'lessthanorequals':
                return $date1->lte($date2);
        }
    }

    public static function compareTwoDate($date1, $date2, $mode = 'equals')
    {
        $date1 = self::generateDate($date1);
        $date2 = self::generateDate($date2);
        return self::comparsion($date1, $date2, $mode);
    }

    public static function generatePasswordResetTokenExpired()
    {
        $expired = env('PASSWORD_RESET_TOKEN_EXPIRED', 30);
        $range = self::calcDiffRange('now', 'add', 'minute', $expired);
        return $range[1];
    }

    public static function startOfYear($date)
    {
      $_date = clone $date;
      return $_date->startOfYear()->toDateString();
    }

    public static function endOfYear($date)
    {
      $_date = clone $date;
      return $_date->endOfYear()->toDateString();
    }

    public static function startOfMonth($date)
    {
      $_date = clone $date;
      return $_date->startOfMonth()->toDateString();
    }

    public static function endOfMonth($date)
    {
        $_date = clone $date;
        return $_date->endOfMonth()->toDateString();
    }
}
