<?php

namespace App\Helpers;
use Input;
use Artisan;
use App\Exceptions\FileNotExits;
class Filehelpers {

    public static function writetofile($dir,$filename,$data){
		self::createDir(storage_path($dir));
	    \File::put(storage_path("{$dir}/{$filename}"), $data);
  	}

  	private static function createDir($dir){
    	if (file_exists ( $dir )) return;
     	mkdir ($dir,0775,true);
  	}

  	public static function readFromfile($path){
		if (!file_exists ( $path )){
			throw FileNotExits::file($path);
        }
		return \File::get($path);
  	}
}
