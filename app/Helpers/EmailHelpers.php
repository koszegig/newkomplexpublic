<?php

namespace App\Helpers;

use Mail;

class EmailHelpers
{
	public static function sendEmail($mail, $param = null){
		if(env('SENDTOTEST', false)){
			$mail->to = env('TESTEMAIL');
		}

		Mail::send($mail->tpl, $param, function ($m) use ($mail) {
			$m->to($mail->to, $mail->toName);
			$m->from(env('MAIL_FROM', null), env('MAIL_FROM_NAME', null));
			$m->subject($mail->subject);
			if(env('SENDTOBCC', false)){
				$m->bcc(env('BCCEMAIL'), $mail->toName);
			}
		});
	}

	public static function sendTestEmail($email, $name){
		$email = (object)[
			'tpl'	 => 'emails.test',
			'to'	  => $email,
			'toName'  => $name,
			'subject' => 'Test Email',
		];

		return self::sendEmail($email);
	}

	public static function sendMessageEmail($to, $toName, $from){
		$email = (object)[
			'tpl'	 => 'emails.new_message',
			'to'	  => $to,
			'toName'  => $toName,
			'subject' => 'Antara új üzenet',
		];
		/*$adminurl = env('ADMIN_URL',null);
		$pwresetaction = env('PW_RESET_URL',null);
		$url = $adminurl.$pwresetaction.$token;*/
		$param = compact('toName','from');
		\MyArray::toStringToLog($param);

		return self::sendEmail($email, $param);
	}

	/*public static function sendPwEmail($_email, $users,$tologin){
		$email = (object)[
			'tpl'	 => 'emails.password_reset_new',
			'to'	  => $_email,
			'toName'  => '',
			'subject' => 'FiveUP jelszó visszaállítás megerősítése!',
		];
		$adminurl = env('ADMIN_URL',null);
		$pwresetaction = env('PW_RESET_URL',null);
		$url = $adminurl.$pwresetaction;
		$param = (object)['users' => $users, 'url' => $url,'tologin' => $tologin];

		return self::sendEmail($email, $param);
	}

	public static function sendForgottUsernameEmail($_email, $users){
		$email = (object)[
			'tpl'	 => 'emails.forgot_username',
			'to'	  => $_email,
			'toName'  => '',
			'subject' => 'FiveUP elfelejtett felhasználónév',

		];
		$param = (object)['users' => $users];

		return self::sendEmail($email, $param);
	}

	public static function sendContactEmail($from,$to,$toname,$message){
		$email = (object)[
			'tpl'	 => 'emails.contact',
			'to'	  => $to,
			'toName'  => $toname,
			'subject' => 'FiveUP elfelejtett felhasználónév',

		];
		$param = (object)['users' => $users];

		return self::sendEmail($email, $param);
	}*/
}
