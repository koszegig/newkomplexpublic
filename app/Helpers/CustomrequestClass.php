<?php


namespace App\Helpers;
use App\Helpers\CustomApiClass;

/**
 * Class CustomrequestClass
 * @package App\Helpers
 */
class CustomrequestClass
{
    /**
     * @var array
     */
    public $filters = [];
    /**
     * @var
     */
    public $api;

    /**
     * CustomrequestClass constructor.
     */
    public function __construct()
    {
         //$this->api = new CustomApiClass();
         $this->api['from'] = 0;
         $this->api['size'] = 0;
         $this->api['pageNumber'] = 0;
        $this->api['to'] = 0;
        $this->api['totalElements'] = 0;
        $this->api['totalPages'] = 0;
        $this->api['offset'] = 0;
    }
}
