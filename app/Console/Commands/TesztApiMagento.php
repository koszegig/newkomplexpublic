<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Base\MagentoRestApi;

class TesztApiMagento extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Test:MagentoApi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param $destFiles
     * @return mixed
     */
    public function handle()
    {
        $magento =  new MagentoRestApi();
        $magento->getAuthToken();
        //$response = $magento->get('/rest/V1/customers/search?searchCriteria[sortOrders][0][field]=email&searchCriteria[sortOrders][0][direction]=asc');
        //$response = $magento->Get("/rest/V1/products/attributes/color");
        ///$response = $magento->Get("/rest/V1/shipments/search?searchCriteria[sortOrders][0][field]=id&searchCriteria[sortOrders][0][direction]=asc");
        // $response = $magento->Get("/rest/V1/orders?searchCriteria=all");
        $entity_id = 11;
        //$response = $magento->Get("/rest/V1/shipments?searchCriteria[filter_groups][0][filters][0][field]=order_id&searchCriteria[filter_groups][0][filters][0][value]=". $entity_id);
        ///$response = $magento->Get("/V1/carts/".$entity_id."/shipping-methods");
        //$response = $magento->Get("/rest/V1/products/attributes/Color");
        //$response = $magento->Get("/rest/V1/products/?searchCriteria[sortOrders][0][field]=sku&searchCriteria[sortOrders][0][direction]=asc");
       $response = $magento->Get("/rest/V1/products/11000016");
       //$response = $magento->Get("/rest/V1/products/SM-R170NZSAXEH-teszt-Prizma-Fehér");
       //$response = $magento->Get("/rest/V1/products/A71A715SVIEWFLIPTOK");
        dd($response);
        //Arrayhelpers::toStringToLog($response,'$response ');
    }
}
