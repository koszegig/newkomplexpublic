<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\WebshopConnect\UnasRestApi;

class TesztApiUnas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Test:UnasApi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param $destFiles
     * @return mixed
     */
    public function handle()
    {
        $unas =  new UnasRestApi();
        $response=$unas->getAuthToken();
        $request='<Params>
			</Params>';
        $response =$unas->Get('getProduct',$request);
        foreach ($response['Product'] as $key=>$itam) {
            dump($itam);
        }

    }
}
