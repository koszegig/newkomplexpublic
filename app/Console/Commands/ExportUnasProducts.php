<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExportUnasProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:export:unasproducts
                            {limit=0 : limit Product count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export products to Magento';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $_argumentum = $this->argument();
        $options = $this->option();

         \App\Jobs\ExportUnasProducts::dispatch($_argumentum,$options);
        //return \App\Jobs\ExportProducts::dispatch();
    }
}
