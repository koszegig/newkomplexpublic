<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImportUnasCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'connect:import:unascustomers
                            {limit=0 : limit Product count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $_argumentum = $this->argument();
        $options = $this->option();

        \App\Jobs\ImportUnasCustomers::dispatch($_argumentum,$options);
        //return \App\Jobs\ExportProducts::dispatch();
    }
}
