<?php

namespace App\Routing;

use \Dingo\Api\Routing\ResourceRegistrar as OriginalRegistrar;

class ExtendedRouter extends OriginalRegistrar
{

    protected $resourceDefaults = array('index', 'store', 'show', 'update', 'destroy','activate','inactivate','listForDn','list','listprint','runcommand','export');

    /**
     * Add the activate method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceActivate($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/activate'.'/{'.$base.'}';

        return $this->router->get($uri, $this->getResourceAction($name, $controller, 'activate', $options));
    }

    /**
     * Add the inactivate method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceInactivate($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/inactivate'.'/{'.$base.'}';

        return $this->router->get($uri, $this->getResourceAction($name, $controller, 'inactivate', $options));
    }

    /**
     * Add the dropdown list method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceListForDn($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/list/dropdown';

        return $this->router->post($uri, $this->getResourceAction($name, $controller, 'listForDn', $options));
    }


    /**
     * Add the list method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceList($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/list';
        return $this->router->post($uri, $this->getResourceAction($name, $controller, 'list', $options));
    }
    /**
     * Add the list method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceListprint($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/listprint';

        return $this->router->post($uri, $this->getResourceAction($name, $controller, 'listprint', $options));
    }
    /**
     * Add the list method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceRuncommand($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/runcommand';

        return $this->router->post($uri, $this->getResourceAction($name, $controller, 'runcommand', $options));
    }
    /**
     * Add the list method for a resourceful route.
     *
     * @param  string $name
     * @param  string $base
     * @param  string $controller
     * @return void
     */
    protected function addResourceExport($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/export';

        return $this->router->post($uri, $this->getResourceAction($name, $controller, 'export', $options));
    }
}
