<?php

namespace App\Libraries\ResponseMacros\Macros;

use Appstract\ResponseMacros\ResponseMacroInterface;

class Message implements ResponseMacroInterface
{
    public function run($factory)
    {
        $factory->macro('message', function ($message, $status) use ($factory) {
            return $factory->make([
            	'status' => 'message',
                'data' => $message
            ], $status);
        });
    }
}
