<?php

namespace App\Libraries;

class Field
{
  private $name;
  private $fillable;
  private $primary;
  private $type;
  private $cast;

  public function __construct($name, $type, $fillable, $primary, $cast=null)
  {
    $this->name = $name;
    $this->fillable = $fillable;
    $this->primary = $primary;
    $this->type = $type;
    $this->cast = $cast;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
      $this->name = $name;
  }

  public function getFillable() {
      return $this->fillable;
  }

  public function setFillable($fillable) {
      $this->fillable = $fillable;
  }

  public function getPrimary() {
      return $this->primary;
  }

  public function setPrimary($primary) {
      $this->primary = $primary;
  }

  public function getType() {
      return $this->type;
  }

  public function setType($type) {
      $this->type = $type;
  }

  public function isPrimary() {
    return $this->type == 'a_id';
    //return $this->primary == true;
  }

  public function isFillable() {
    return $this->fillable == true;
  }

  public function getCast() {
    return $this->cast;
  }

  public function setCast($cast) {
      $this->cast = $cast;
  }
}
