<?php

namespace App\Libraries\Database;

use App\Libraries\Database\Where;
use App\Libraries\Database\From;
use App\Libraries\Database\Select;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class QueryBuilderSP
 * @package App\Libraries\Database
 */
class QueryBuilderSP
{
    /**
     * @var
     */
    private $query;
    /**
     * @var array
     */
    private $condition;
    /**
     * @var mixed
     */
    private $model;
    /**
     * @var
     */
    private $filters;
    /**
     * @var
     */
    private $macro;
    /**
     * @var
     */
    private $paginate;
    /**
     * @var \App\Libraries\Database\Select
     */
    private $select;
    /**
     * @var
     */
    private $condstring;
    /**
     * @var \App\Libraries\Database\From
     */
    private $from;
    /**
     * @var array|mixed
     */
    private $params;
    /**
     * @var
     */
    private $size;
    /**
     * @var
     */
    private $offset;
    /**
     * @var
     */
    private $pageto;
    /**
     * @var
     */
    private $basemacro;
    /**
     * @var
     */
    private $current_page;
    /**
     * @var
     */
    private $pagefrom;
    /**
     * @var mixed|string
     */
    private $orderBy;

    /**
     * @var string[]
     */
    public $types = [
    'uuid',
    'Varchar',
    'Bool',
    'timestamp',
    'Int4',
    'text',
    'date','a_integer_30',
    'a_integer_40',
    'a_integer_20',
    'a_integer_10',
    'a_integer_1',
    'a_vc_25',
    'a_c13',
    'a_vc_1200',
    'a_json',
    'a_xml',
    'a_n_13_2',
    'a_n_4_3',
    'a_n_12_2',
    'a_cd8',
    'a_c70',
    'a_c50',
    'a_c30',
    'a_c20',
    'a_c10',
    'a_vc_8000',
    'a_vc_200',
    'a_vc_120',
    'a_vc_32',
    'a_n_30',
    'a_vc_28',
    'a_n_12_4',
    'a_tobberteku_kod_mezo_vc30',
    'a_c7',
    'a_n_12_2',
    'a_oid',
    'a_n_2_4',
    'a_vc_30_nev_rovid',
    'a_vc_15_kod',
    'a_vc_80',
    'a_vc_27',
    'a_n_10_3',
    'a_vc_22',
    'a_vc_11',
    'a_kodok_user_c3',
    'a_n_6',
    'a_vc_64',
    'a_n_20',
    'a_n_5',
    'a_n_12_3',
    'a_id',
    'a_n_26',
    'a_vc_14',
    'a_uuid_id_mut',
    'a_uuid_id',
    'a_n_6_3',
    'a_n_6_2',
    'a_vc_56',
    'a_n_2_6',
    'a_vc_36',
    'a_vc_800',
    'a_id_mut_t',
    'a_vc_44',
    'a_n_10',
    'a_id_mut_t2',
    'a_n_12',
    'a_integer_4',
    'a_vc_20',
    'a_vc_1000',
    'a_blob_kep',
    'a_vc_50_nev_cikk',
    'a_vc_70',
    'a_n_4',
    'a_vc_50_nev_partner',
    'a_vc_150',
    'a_vc_45',
    'a_n_11_2',
    'a_c3',
    'a_text',
    'a_n_3_2',
    'a_igen_nem_c1',
    'a_datum',
    'a_pfjszk',
    'anyelement',
    'a_n_18_4_ertek',
    'a_vc_15',
    'a_vc_50_nev_hosszu_t',
    'a_vc_37',
    'a_vc_52',
    'a_c8',
    'a_vc_48',
    'a_vc_30_tobberteku',
    'a_n_14_4',
    'a_n_9',
    'a_c2',
    'a_n_7',
    'a_sql',
    'a_n_6_1',
    'a_vc_40',
    'a_n_38',
    'a_n_9_3',
    'a_n',
    'a_n_12_1',
    'a_n_13_2',
    'a_text_t',
    'a_vc_500',
    'a_vc_254',
    'a_c6',
    'a_vc_34',
    'a_blob',
    'a_vc_35',
    'a_n_2',
    'a_n_14_2',
    'a_n_10_2',
    'a_vc_25',
    'a_integer_8',
    'a_text_t2',
    'a_vc_50_nev_hosszu',
    'a_n_4_2',
    'a_vc_55',
    'a_n_8',
    'a_vc_62',
    'a_rdar',
    'a_kodtipus_c10',
    'a_ido',
    'a_vc_26',
    'a_kodok_c1',
    'a_datum_ido',
    'a_id_mut',
    'a_vc_100_nev_partner',
    'a_n_8_4',
    'a_vc_10',
    'a_n_3_0',
    'a_n_7_4',
    'a_integer_2',
    'a_vc_13',
    'a_c9',
    'a_c5',
    'a_vc_18',
    'a_vc_12',
    'a_vc_16',
    'a_n_11_3',
    'a_vc_24',
    'a_vc_60',
    'a_n_3_4',
    'a_n_1',
    'a_n_5_2',
    'a_vc_128_ean',
    'a_n_7_3',
    'a_vc_41',
    'a_c4',
    'a_c1',
    'a_aktiv_boolean',
    'a_cim_id',
    'a_vc_100_szoveg_rovid',
    'a_n_10_4',
    'a_n_7_2',
    'a_n_5_3',

  ];

    /**
     * QueryBuilderSP constructor.
     * @param $model
     * @param $request
     * @param $paginate
     * @param $select
     * @param array $params
     * @param bool $isAddId
     * @param string $orderBy
     */
    public function __construct($model, $request, $paginate, $select, $params = [], $isAddId= true, $orderBy= '')
  {
    $this->model = new $model();
    $this->paginate = $paginate;
    $this->params = $params;
    $this->orderBy = $orderBy;
    if ($isAddId) {
        $select[] = $this->model->getPrimaryKey().' as id';
    }
    $this->select = new Select($select);
    $this->condition = [];
    $this->buildlop = true;
    $this->from = new From($this->model);
    $this->initialFilters($request);
    $this->initialMacro($request);
    $this->initialOffset($request);
    $this->processFilters();
    array_push($this->condition, new where('1', '1', '='));
  }

    /**
     * @param int $size
     * @return $this
     */
    public function paginate($size = 25){
    $this->paginate = $size;
    return $this;
  }

    /**
     * @param $request
     */
    private function initialOffset($request){
    $this->size = '' ;
    $this->offset = '' ;
    $this->pagefrom = 0;
    if (isset($request->api)) {
     // \MyArray::toStringToLog('initialOffset----');
     // \MyArray::toStringToLog($request->api);
      $this->pagefrom = (int)$request->api['offset'];

     // \MyArray::toStringToLog('pageto----');
      $this->pageto = (int)$request->api['offset']+(int)$request->api['size'];
     // \MyArray::toStringToLog($this->pageto);
      $this->current_page = $this->pageto/(int)$request->api['size'];
     // \MyArray::toStringToLog($this->current_page);
      $this->size = ' limit '. $request->api['size'];
     /// \MyArray::toStringToLog($this->size);
      $this->offset = ' offset '. $request->api['offset'];
     /// \MyArray::toStringToLog('----');
    }
  }

    /**
     * @param $request
     */
    private function initialFilters($request){
    $this->filters =  isset($request->filters) ? $request->filters : [];
  }

    /**
     * @param $request
     */
    private function initialMacro($request){
        $this->macro =  isset($request->macro) ? $request->macro : '';
       $this->macro = (is_array($this->macro)) ? implode("", $this->macro) : $this->macro;
  }

    /**
     *
     */
    private function processFilters(){
    /*\MyArray::toStringToLog('processFilters----');
    \MyArray::toStringToLog($this->filters);
    \MyArray::toStringToLog('----');*/
    if(empty($this->filters)) return;
    foreach($this->filters as $filter){
      $this->processFilter($filter);
    }
  }

    /**
     * @param $filter
     */
    private function processFilter($filter){
    $this->where($filter);
  }

    /**
     * @param $filter
     */
    private function where($filter){
    $where = (is_array($filter['field'])) ? $this->subwhere($filter) : $this->__where($filter);
    array_push($this->condition, $where);
  }

    /**
     * @param $filter
     * @return \App\Libraries\Database\Where
     */
    private function  subwhere($filter){
    $this->basemacro = '';
    $where = new Where(null, null, null, $filter['lop']);
    foreach($filter['field'] as $field){
      $type = $this->getType($field['field'],$filter);
      /*\MyArray::toStringToLog('if(!$this->isValidFilter($field, $type))----');
      \MyArray::toStringToLog($field);
      \MyArray::toStringToLog($type);
      \MyArray::toStringToLog('----');*/
      if ($field['field'] == 'basemacro') {
        $this->basemacro =  $filter['value'];
        //\MyArray::toStringToLog($this->basemacro);
      } else {
        if(!$this->isValidFilter($field, $type)) continue;
         $where->pushSub($field['field'], $filter['value'], $field['op'], $field['lop'], $type->isLower(), $type->quoted());
      }
    }
    return $where;
  }

    /**
     * @param $_field
     * @param $filter
     * @return false|mixed
     */
    private function getType($_field, $filter){
    $field = $this->field($_field)->first();
    /*\MyArray::toStringToLog('getType($_field, $filter)----');
    \MyArray::toStringToLog($field);
    \MyArray::toStringToLog($filter);
    \MyArray::toStringToLog('----');    */
    $type = ($field == null) ? $filter['fieldtype'] : $field->getType();
    return $this->initialTypeClass($type);
  }

    /**
     * @param $filter
     * @param $type
     * @return bool
     */
    private function isValidFilter($filter, $type){
    return ($type !== false && $type->isValidOperator($filter['op']));
  }

    /**
     * @param $filter
     * @return \App\Libraries\Database\Where|void
     */
    private function __where($filter){
    $type = $this->getType($filter['field']);
    if(!$this->isValidFilter($filter, $type)) return;
    return new Where($filter['field'], $filter['value'], $filter['op'], $filter['lop'], $type->isLower(), $type->quoted());
  }

    /**
     * @param $_field
     * @return mixed
     */
    private function field($_field){
     return $this->model->getFields()->filter(function ($field) use ($_field) {
      return $field->getName() == $_field;
    });
  }

    /**
     * @param $type
     * @return false|mixed
     */
    private function initialTypeClass($type){
    /*\MyArray::toStringToLog('initialTypeClass---Start-');
    \MyArray::toStringToLog($type);*/
    if(!$this->isValidType($type)) return false;
    $__type = 'App\Libraries\Database\Types\\'.ucfirst($type).'Type';
    /*\MyArray::toStringToLog('initialTypeClass----');
    \MyArray::toStringToLog($__type);
    \MyArray::toStringToLog('----');    */
    return new $__type();
  }


    /**
     * @return array
     */
    public function get(){
    $this->build();
    $dmdata = \DB::select($this->query, $this->generateparams());
    $dmdata = $this->model::hydrate($dmdata);
    if(!is_null($this->paginate)){
      $dmdata = \MyPage::paginateWithoutKey($dmdata, $this->paginate);
      $dmdata['from'] =$this->pagefrom;
      $dmdata['current_page'] =$this->current_page;
      $dmdata['to'] =$this->pageto;
    }
    return $dmdata;
  }
    /**
     * @param $page
     * @param $size
     * @return LengthAwarePaginator
     */
    public function Paginator($page, $size){
       // $this->select->setSelectsize($page,$size);
        $this->build();
        \MyArray::toStringToLog($this->query);
        $dmdata = \DB::select($this->query, $this->generateparams());
        $dmdata = $this->model::hydrate($dmdata);
        $collect = collect($dmdata);

        $paginationData = new LengthAwarePaginator(
            $collect->forPage($page, $size),
            $collect->count(),
            $size,
            $page
        );
        return $paginationData;
    }

    /**
     * @return array
     */
    public function all(){
        $this->build();
        $dmdata = \DB::select($this->query, $this->generateparams());
        /*$dmdata = $this->model::hydrate($dmdata);
        if(!is_null($this->paginate)){
            $dmdata = \MyPage::paginateWithoutKey($dmdata, $this->paginate);
            $dmdata['from'] =$this->pagefrom;
            $dmdata['current_page'] =$this->current_page;
            $dmdata['to'] =$this->pageto;
        }*/
        return $dmdata;
    }

    /**
     * @return array
     */
    private function generateparams(){
   // $array = [env('DB_PREFIX', null)];
    $array = [];
    if(!empty($this->params)) $array = array_merge($array, $this->params);
    //array_push($array, $this->condstring);
     // \MyArray::toStringToLog($array);
    return $array;
  }


    /**
     *
     */
    private function build(){
    $this->query = $this->select->build();
    $this->query .= $this->from->build();
    $this->condstring = $this->buildCond();
    /*\MyArray::toStringToLog('-----------------------------------');
    \MyArray::toStringToLog($this->condstring);
    \MyArray::toStringToLog('-----------------------------------');*/
    IF (trim($this->condstring) =='AND 1 = 1') {
      $this->query .=  ' where 1 = 1 '.$this->condstring;
    }
     else {
      IF ($this->condstring !='1 = 1') {
       // $this->condstring = ' 1 = 1 '.$this->condstring;
      }
        $this->query .=  ' where '.$this->condstring;
    }
        $this->query .=' '.$this->macro;
    $this->query .=' '.$this->basemacro;
     $this->query .= $this->orderBy;
    $this->query .= $this->size;
    $this->query .= $this->offset;

 //   \MyArray::toStringToLog($this->query);
  }


    /**
     * @return string
     */
    private function buildCond(){
   // \MyArray::toStringToLog($this->condition);
    $where = '';
    foreach($this->condition as $condition){
      $where.= $condition->build();
    }
    return $where;
  }

    /**
     * @param $type
     * @return bool
     */
    private function isValidType($type){
    return in_array($type, $this->types);
  }
}
