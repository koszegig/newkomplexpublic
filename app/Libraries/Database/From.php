<?php

namespace App\Libraries\Database;

class From
{
  private $model;

  public function __construct($model = null)
  {
    $this->model = $model;
  }

  /**
   * Get the value of model
   */
  public function getModel()
  {
    return $this->model;
  }

  /**
   * Set the value of model
   *
   * @return  self
   */
  public function setModel($model)
  {
    $this->model = $model;

    return $this;
  }

  public function build(){
    $sp = $this->model->getStoredProcedure();
    return ' FROM ' . $this->model->getStoredProcedure().' ';
  }
}
