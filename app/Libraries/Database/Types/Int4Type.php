<?php

namespace App\Libraries\Database\Types;

class Int4Type extends QueryFieldType
{
  public function __construct()
  {
    $this->operators = ['=', '<', '>', '<=', '>=', '<>'];
    $this->quote = false;
    $this->lower = false;
  }
}
