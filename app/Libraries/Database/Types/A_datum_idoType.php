<?php

namespace App\Libraries\Database\Types;

class A_datum_idoType extends QueryFieldType
{
  public function __construct($model)
  {
    $this->operators = [ '=', '<', '>', '<=', '>=', '<>', 'between'];
    $this->quote = true;
  }
}
