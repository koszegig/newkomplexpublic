<?php

namespace App\Libraries\Database\Types;

class UuidType extends QueryFieldType
{
  public function __construct()
  {
    $this->operators = ['=', '<>'];
    $this->quote = true;
    $this->lower = false;
  }
}
