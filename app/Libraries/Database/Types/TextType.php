<?php

namespace App\Libraries\Database\Types;

class TextType extends QueryFieldType
{
  public function __construct()
  {
    $this->operators = ['=', '<>','like', 'not like', 'ilike', 'not ilike'];
    $this->quote = true;
    $this->lower = true;
  }
}
