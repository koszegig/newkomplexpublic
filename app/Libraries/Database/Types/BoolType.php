<?php

namespace App\Libraries\Database\Types;

class BoolType extends QueryFieldType
{
  public function __construct()
  {
    $this->operators = ['=', '<>'];
    $this->quote = false;
    $this->lower = false;
  }
}
