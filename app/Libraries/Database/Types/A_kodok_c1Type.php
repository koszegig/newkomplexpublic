<?php

namespace App\Libraries\Database\Types;

class A_kodok_c1Type  extends QueryFieldType
{
  public function __construct()
  {
    $this->operators = ['=', '<>','like', 'not like', 'ilike', 'not ilike'];
    $this->quote = true;
    $this->lower = false;
  }
}
