<?php

namespace App\Libraries\Database\Types;

class A_nType extends QueryFieldType
{
  public function __construct($model)
  {
    $this->operators = ['=', '<', '>', '<=', '>=', '<>'];
    $this->quote = false;
  }
}
