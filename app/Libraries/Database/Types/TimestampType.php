<?php

namespace App\Libraries\Database\Types;

class TimestampType extends QueryFieldType
{
  public function __construct()
  {
    $this->operators = [ '=', '<', '>', '<=', '>=', '<>', 'between'];
    $this->quote = false;
    $this->lower = false;
  }
}
