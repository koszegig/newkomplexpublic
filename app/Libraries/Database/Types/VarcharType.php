<?php

namespace App\Libraries\Database\Types;

class VarcharType extends QueryFieldType
{
  public function __construct()
  {
    $this->operators = ['=', '<>','like', 'not like', 'ilike', 'not ilike'];
    $this->quote = true;
    $this->lower = true;
  }
}
