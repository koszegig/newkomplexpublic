<?php

namespace App\Libraries\Database\Types;

class QueryFieldType
{
  protected $operators;
  protected $quote;
  protected $lower;

  public function isValidOperator($operator){
    return in_array($operator, $this->operators);
  }

  public function quoted(){
    return $this->quote;
  }

  public function isLower(){
    return $this->lower;
  }


}
