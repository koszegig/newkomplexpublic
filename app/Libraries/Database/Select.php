<?php

namespace App\Libraries\Database;

/**
 * Class Select
 * @package App\Libraries\Database
 */
class Select
{
    /**
     * @var mixed|null
     */
    private $select;

    /**
     * Select constructor.
     * @param null $select
     */
    public function __construct($select = null)
  {
    $this->select = $select;
    array_push($this->select , 'count(*) over() as totalpage');
  }

  /**
   * Get the value of select
   */
  public function getSelect()
  {
    return $this->select;
  }

  /**
   * Set the value of select
   *
   * @return  self
   */
  public function setSelect($select)
  {
    $this->select = $select;

    return $this;
  }

    /**
     * @return string
     */
    public function build(){
    return "SELECT " . join(",", $this->select). " ";
  }
    /**
     * Set the value of select
     *
     * @return  self
     */
    public function setSelectsize($page,$size)
    {
        array_push($this->select , '(count(*) over()/'.$size.') as totalpage');
        array_push($this->select , $size.' as size');
        array_push($this->select , $page.' as page');
    }

}
