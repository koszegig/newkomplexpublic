<?php

namespace App\Libraries\NodeConnector;

use App\Libraries\XmlToArray;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;

class NodeConnector
{

  protected $data;
  protected $action;
  protected $config;

  /**
   *
   * @param Config  $config
   */
  public function __construct($config,$action,$data) {
      $this->data = $data;
      $this->action = $action;
      $this->config = $config;
       $this->initialclient();
  }

  private function initialclient(){
    $this->client = new Client();
  }

  public function post($print=true){
      return $this->call('POST',$print=true);
  }
    public function get($print=true,$convert=true){
        return $this->call('GET',$print,$convert);
    }

  private function generateURL(){
      $ip =  Config::get('NAV.config.'.$this->config.'.ip');
      $port = Config::get('NAV.config.'.$this->config.'.port');
      $host = "{$ip}:{$port}";//ipcimn;
      $ssl = false;
      $url = 'http';
      if($ssl) $url.='s';
      $url .= '://'.$host;
      return $url.'/'.$this->action;
   }

 	private function call($method,$print,$convert){
      $url = $this->generateURL($this->action);
        \MyArray::toStringToLog($url);
      $options = $this->options($method,$this->data);
      $content = null;
      try {
        $response = $this->client->request($method,$url,$options);
        $content = $this->convertResponse($response,$print);
      }
      catch (Guzzle\Http\Exception\ClientErrorResponseException $e) {
        $response = $e->getRequest();
        $content = $this->convertResponse($response,$print);
      }
      catch (Guzzle\Http\Exception\ServerErrorResponseException $e) {
        $response = $e->getRequest();
        $content = $this->convertResponse($response,$print);
      }
      catch (Guzzle\Http\Exception\ServerException $e) {
        $response = $e->getRequest();
        $content = $this->convertResponse($response,$print);
      }
      catch (Guzzle\Http\Exception\BadResponseException $e) {
        $response = $e->getRequest();
        $content = $this->convertResponse($response,$print);
      }
      catch( \Exception $e){
        \MyArray::toStringToLog($e->getMessage());
      }
      return $content;
      return true;
   }

   private function options(){
      $headers = $this->headers();
      $options = [
          'json' => $this->data,
      ];
      return $options;
   }

   private function headers(){
      $headers = [
            'Content-Type' => 'application/json"'
        ];
      return $headers;
   }

    private function convertResponse($response,$print){
         $content = $response->getBody()->getContents();
         $output = env('RESPONSEOUTPUT','array');
         if($print && $output == 'json')
            $content = json_encode($content,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
         else if($output == 'xml'){
            $content = XmlToArray::convert($content);
            $this->_converResponse($content);
         }
         return $content;
    }


}
?>
