<?php

//declare(strict_types=1);

namespace App\Libraries;

use Vyuldashev\XmlToArray\XmlToArray as XmlToArrayBase;
use DOMText;
use DOMElement;
use DOMCdataSection;

class XmlToArray extends XmlToArrayBase
{

    public function __construct(string $xml)
    {
        parent::__construct($xml);
    }

   protected function convertDomElement(DOMElement $element)
    {
        $result = $this->convertAttributes($element->attributes);
        $die = false;
        foreach ($element->childNodes as $node) {
            if ($node instanceof DOMCdataSection) {
                $result['_cdata'] = $node->data;

                continue;
            }
            if ($node instanceof DOMText) {
                $result = $node->textContent;

                continue;
            }
            if ($node instanceof DOMElement) {
                if($node->nodeName == 'businessValidationMessages') $die = false;
                if(isset($result[$node->nodeName])){
                    if(!isset($result[$node->nodeName][0]))
                        $result[$node->nodeName] = [$result[$node->nodeName]];
                    $result[$node->nodeName][] = $this->convertDomElement($node);
                }
                else
                    $result[$node->nodeName] = $this->convertDomElement($node);

                continue;
            }
        }
        return $result;
    }
}
