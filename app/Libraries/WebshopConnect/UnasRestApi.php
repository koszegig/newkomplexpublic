<?php


namespace App\Libraries\WebshopConnect;
use GuzzleHttp\Client;
use App\Models\Types\TUnasToken1;
use App\Models\Tables\UnasToken;
use Vyuldashev\XmlToArray\XmlToArray;
use Log;

class UnasRestApi
{
    public $token = "";

    public $request_counter = 0;
    const MAX_TOKEN_REQUESTS = 100;

    public function __construct()
    {
//        ini_set('memory_limit', '1024M');
    }


    public function getAuthToken()
    {
        $userData = array("username" => env("MAGENTO_USER"), "password" => env("MAGENTO_PASS"));
        //       dump($userData);
//        dump(env("MAGENTO_HOST"));
        $databaseToken = TUnasToken1::getToken(env("UNAS_API"));
        if ($databaseToken) {
            $this->token = $databaseToken;
            Log::info("Database New token: " . $this->token);
            echo "Database New token: {$this->token}  \n\t";
        } else {
        $ch = curl_init("https://api.unas.eu/shop/login");
        //
        $request = '<?xml version="1.0" encoding="UTF-8" ?>
			<Params>
				<ApiKey>' . env("UNAS_API") . '</ApiKey>
			</Params>';
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $response = curl_exec($ch);

        //$xml=simplexml_load_string($response);

        $result = $this->convertXMLtoArray($response, 'Login');
        if ($result['isSuccess']) {
            $xml = $result['data'];
            $this->token = $xml['Token'];
            $tokenrekord = [];
            $tokenrekord['unstkn_token'] = $this->token;
            $tokenrekord['unstkn_api'] = env("UNAS_API");
            $tokenrekord['unstkn_until'] = $xml['Expire'];
            $tokenrekord['unstkn_permissions'] = json_encode($xml['Permissions']);
            $sqlStocksQty = UnasToken::UpdateOrCreate(['unstkn_api' => $tokenrekord['unstkn_api']], $tokenrekord);
        }

        echo "New token: {$this->token} ";
    }
        return $this->token;
    }
    public function renewToken() {
        $this->request_counter++;
        if ( $this->request_counter > self::MAX_TOKEN_REQUESTS) {
            $this->request_counter = 0;
            Log::info("Request limit reached. Renewing token.");
            $this->getAuthToken();
            Log::info("New token: " . $this->token);
        }
    }
    public function Post($url, $data) {
        $this->renewToken();
        $curl = curl_init();
        $headers=array();
        $headers[]="Authorization: Bearer ".$this->token;
        $request='<?xml version="1.0" encoding="UTF-8" ?>'.$data;


        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, "https://api.unas.eu/shop/".$url);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$request);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION,true);

        $response = curl_exec($curl);
        return $this->convertXMLtoArray($response)['data'];
    }
    public function Get($url, $data) {
        $this->renewToken();
        $curl = curl_init();
        $headers=array();
        $headers[]="Authorization: Bearer ".$this->token;
        $request='<?xml version="1.0" encoding="UTF-8" ?>'.$data;


        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, "https://api.unas.eu/shop/".$url);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$request);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION,true);

        $response = curl_exec($curl);
        return $this->convertXMLtoArray($response)['data'];
    }
    /**
     * @param $title
     * @param $dictionary
     * @return string
     */
    public function createUrlKey($title, $dictionary)
    {
        if (function_exists('iconv')) {
            $url = iconv('UTF-8', 'us-ascii//TRANSLIT//IGNORE', $title);
            $url = preg_replace('#[^0-9a-z]+#i', '-', $url); // fixme: ???
        } else {
            die("iconv missing.");
        }

        $urlKey = strtolower($url);

        $isUnique = $this->checkUrlKeyDuplicates($urlKey, $dictionary);
        if ($isUnique) {
            return $urlKey;
        } else {
            return $urlKey . '-' . time();
        }
    }


    /**
     * @param $key
     * @param $dictionary
     * @return bool
     */
    private function checkUrlKeyDuplicates($key, $dictionary)
    {
        $isUnique = true;

        if ( array_key_exists($key,$dictionary) ) {
            $isUnique = false;
        }

        return $isUnique;
    }

    public function get_attribute_value_from_product($magento_product, $attribute_code) {
        if ( is_object($magento_product) && property_exists($magento_product, "id") ) {
            foreach ($magento_product->custom_attributes as $attr) {
                if ($attr->attribute_code == $attribute_code) {
                    return $attr->value;
                }
            }
        }
        return false;
    }
    /**
     * @param $key
     * @param $dictionary
     * @return bool
     */
    public function convertXMLtoArray($content, $itamPath = 'Item', $keyFieldName = '')
    {
        $result = [ 'isSuccess' => true,
            'message' => '',
            'data' => []

        ];
        if( $content == ''){
            Log::error('Not valid XML.');
            $result['isSuccess']  = false;
            $result['message']  = 'XML üres';
            return $result;
        }
     /*   $content= str_replace('<?xml version="1.0" encoding="UTF-8" ?>',"",$content);*/
        /*$content = str_replace('<?xml version="1.0" encoding="utf-8" ?>', '', $content);
        content = str_replace('<?xml version="1.0" encoding="utf-8" ?>', '', $content);*/

        $validateResult =$this->validate($content);
        if( $validateResult !== true){
            Log::error('Not valid XML.');
            $result['isSuccess']  = false;
            $result['message']  = 'Not valid XML.';
            return $result;
        } else{
            if($content){
                $xml = simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_PARSEHUGE);
                if(!$xml)  {
                    Log::error('XML is empty.');
                    $result['isSuccess']  = false;
                    $result['message']  = 'XML is empty.';
                    return $result;

                };
                $array = json_decode(json_encode((array)$xml, JSON_UNESCAPED_UNICODE), true);
            }

            $data = [];
            if (isset($array[$itamPath][0])) {
                foreach ($array[$itamPath] as $item)
                {
                    $object = new \stdClass();
                    foreach ($item as $key => $value) {
                        $object->$key = $value;
                    }
                    if ( $keyFieldName == '') {
                        $data [] = $object;
                    } else {
                        $data [$item [$keyFieldName]] = $object;
                    }
                }
            }

            if (isset($array[$itamPath])) {
                if (!isset($array[$itamPath][0])) {
                    $object = new \stdClass();
                    ///Arrayhelpers::toStringToLog($item,'$item - xmlmanager',__FILE__, __METHOD__, __LINE__);
                    foreach ($array[$itamPath] as $key => $value) {
                        $object->$key = $value;
                    }
                    if ( $keyFieldName == '') {
                        $data [] = $object;
                    } else {
                        $data [$array [$keyFieldName]] = $object;
                    }
                }
                ksort($data);
            } else{
                $data = $array;
            }

            $result['data']  =$data;
            //Arrayhelpers::toStringToLog($data,'$data - xmlmanager',__FILE__, __METHOD__, __LINE__);
        return $result;
        }
    }
    /**
     * @param $content
     * @param string $version
     * @param string $encoding
     * @return array|bool
     */
    public function validate($content, $version = '1.0', $encoding = 'utf-8'){
        if(trim($content) == '') return false;

        libxml_use_internal_errors(true);
        libxml_disable_entity_loader(false);

        $doc = new \DOMDocument($version, $encoding);
        $doc->loadXML($content, LIBXML_PARSEHUGE);

        $errors = libxml_get_errors();
        libxml_clear_errors();
        if(!empty($errors)) return $errors;

        return true;
    }
}
