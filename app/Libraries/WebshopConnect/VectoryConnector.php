<?php


namespace App\Libraries\WebshopConnect;
use App\Aion\Library\XmlToArray;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Aion\Helpers\ArrayHelpers;

class VectoryConnector
{

    protected $data;
    protected $action;
    protected $token;
    protected $output;
    protected $apipoint ='';

    /**
     *
     * @param Config  $config
     */
    public function __construct($action,$data) {

        $this->action = $action;
        $this->output = env('RESPONSEOUTPUT','array');
        $this->initialclient();
        $this->data = $data;
        $this->apipoint ='';
    }

    private function initialclient(){
//        $this->client = new Client();
        $this->client = new Client();
       // $this->token = $this->getToken();
        //dd($this->token);
    }

    public function post($print=true){
        return $this->call('GET',$print=true);
    }
    public function get($apiend='',$print=true){
        $this->apipoint = $apiend;
        $url = $this->generateURL($this->action);
        $headers = $this->headers();
       // dump(env('VECTORYUSER'));
       // dump(env('VECTORYPASSWORD'));
       /// dump($headers);
        //$credentials = base64_encode(env('VECTORYUSER').':'.env('VECTORYPASSWORD'));
        /*$response = $this->client->request('GET',$url,[
            'headers' => $headers,
            'Authorization' => ['Basic '.$credentials]
        ]);*/
                $response = $this->client->request('GET',$url,[
            'headers' => $headers,
            'auth' => [
            env('VECTORYUSER'),
            env('VECTORYPASSWORD')
                    ]
        ]);

        $content = $this->convertResponse($response,$print);
        return $content;
//        return $this->call('GET',$print=true);
    }
    private function getToken($print=true){
        $this->data =[
                    'email' => env('VECTORYEMAIL'),
                    'password' => env('VECTORYPASSWORD')
        ];
        $this->apipoint = '/api/auth/login';
        $result=$this->call('POST',$print=true);
        if ($result['auth']){
            return  $result['token'];
        } else {
            return '';
        }

    }
    public function getData(){
        return $this->data;
    }
    public function setData($data =[]){
        $this->data = $data;
        return $this->data;
    }
    private function generateURL(){
        $ip = env('VECTORYSERVERIP');
        $port = env('VECTORYSERVERPORT');
        $host = "{$ip}:{$port}";//ipcimn;
        $ssl = env('SSL');;
        $url = 'http';
        if($ssl) $url.='s';
        $url .= '://'.$host;
        if ($this->apipoint !='') {
            return $url.$this->apipoint;
        }

        return $url;
    }

    private function call($method,$print){
        $url = $this->generateURL($this->action);
        $options = $this->options($method,$this->data);
        $content = null;
        try {
            $response = $this->client->request($method,$url,$options);

            $content = $this->convertResponse($response,$print);
        }
        catch (Guzzle\Http\Exception\ClientErrorResponseException $e) {
            $response = $e->getRequest();
            $content = $this->convertResponse($response,$print);
        }
        catch (Guzzle\Http\Exception\ServerErrorResponseException $e) {
            $response = $e->getRequest();
            $content = $this->convertResponse($response,$print);
        }
        catch (Guzzle\Http\Exception\ServerException $e) {
            $response = $e->getRequest();
            $content = $this->convertResponse($response,$print);
        }
        catch (Guzzle\Http\Exception\BadResponseException $e) {
            $response = $e->getRequest();
            $content = $this->convertResponse($response,$print);
        }
        catch( \Exception $e){
            Arrayhelpers::toStringToAll($e->getMessage(), 'VectoryConnector error ', __FILE__, __METHOD__, __LINE__);
        }
        return $content;
        return true;
    }

    private function options(){
        $headers = $this->headers();
        $options = [
            'header' =>$headers,
            'json' => $this->data,

        ];
        /*[
            'auth' => ['username', 'password', 'digest']
        'auth' => [ env('VECTORYUSER'),          env('VECTORYPASSWORD')]
        ]*/
        return $options;
    }

    private function headers(){
        //$credentials = base64_encode(env('VECTORYUSER').':'.env('VECTORYPASSWORD')  );
        $headers = [
            'Content-Type' => 'application/json"',
            'Accept' => 'application/json',
            //'Content-Type' => 'application/xml"'
           // 'Authorization', ['Basic '.$credentials]
        ];
      /*  if ($this->token !='') {
            $headers['x-access-token'] = $this->token;
        }*/

        return $headers;
    }

    private function convertResponse($response,$print){
        $content = $response->getBody()->getContents();
        if($print && $this->output == 'json')
            $content = json_encode($content,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
        else if($this->output == 'array'){
            $content = json_decode($content, true);
//            $this->_converResponse($content);
        } if($this->output == 'xml'){
            $content = XmlToArray::convert($content);
            $this->_converResponse($content);
        }
        return $content;
    }


}
