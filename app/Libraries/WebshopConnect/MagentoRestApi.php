<?php

namespace App\Libraries\WebshopConnect;

use GuzzleHttp\Client;
use App\Models\Types\TMagentoToken1;
use Log;

class MagentoRestApi
{
    public $token = "";

    public $request_counter = 0;
    const MAX_TOKEN_REQUESTS = 100;

    public function __construct()
    {
//        ini_set('memory_limit', '1024M');
    }


    public function getAuthToken() {
        $userData = array("username" => env("MAGENTO_USER"), "password" => env("MAGENTO_PASS"));
 //       dump($userData);
//        dump(env("MAGENTO_HOST"));
        $databaseToken = MagentoToken::getToken(env("MAGENTO_USER"));
        if ($databaseToken) {
            $this->token = $databaseToken;
            Log::info("Database New token: " . $this->token);
            echo "Database New token: {$this->token}  \n\t";
        } else {
        $ch = curl_init(env("MAGENTO_HOST")."/rest/V1/integration/admin/token");

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Length: " . strlen(json_encode($userData))));

          $this->token = curl_exec($ch);
            $tokenrekord = [];
            $tokenrekord['username'] = env("MAGENTO_USER");
//            $tokenrekord['until'] = \Carbon\Carbon::parse(date('Y-m-d'))->addDays(3);
            $tokenrekord['until'] = \Carbon::parse(date('Y-m-d'))->addDays(3);
            $tokenrekord['rp_token'] = $this->token;

            $sqlStocksQty = MagentoToken::UpdateOrCreate(['username' => $tokenrekord['username']],$tokenrekord);
            echo "New token: {$this->token}  \n\t";
            Log::info("New token: " . $this->token);

        }
        // FIXME: figyelni hogy sikerul-e tokent szerezni
    }
    public function renewToken() {
        $this->request_counter++;
        if ( $this->request_counter > self::MAX_TOKEN_REQUESTS) {
            $this->request_counter = 0;
            Log::info("Request limit reached. Renewing token.");
            $this->getAuthToken();
            Log::info("New token: " . $this->token);
        }
    }
    public function Post($url, $data) {
        $this->renewToken();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => env("MAGENTO_HOST").$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "content-type: application/json",
                "authorization: Bearer " . json_decode($this->token)
            ]
        ]);


        $response = json_decode(curl_exec($curl));

        return $response;
    }


    public function Put($url, $data) {
        $this->renewToken();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => env("MAGENTO_HOST").$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "content-type: application/json",
                "authorization: Bearer " . json_decode($this->token)
            ]
        ]);

        $response = json_decode(curl_exec($curl));

        return $response;
    }


    public function Delete($url, $data) {
        $this->renewToken();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => env("MAGENTO_HOST").$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "content-type: application/json",
                "authorization: Bearer " . json_decode($this->token)
            ]
        ]);

        $response = json_decode(curl_exec($curl));

        return $response;
    }


    public function Get($url) {
        $this->renewToken();

        $ch = curl_init( env("MAGENTO_HOST").$url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($this->token)));

        return json_decode(curl_exec($ch));
    }

    public function getMessage($response) {
        if ( is_object($response) && property_exists($response, "message") ) {
            return $response->message;
        } else {
            return false;
        }
    }

    /**
     * @param $title
     * @param $dictionary
     * @return string
     */
    public function createUrlKey($title, $dictionary)
    {
        if (function_exists('iconv')) {
            $url = iconv('UTF-8', 'us-ascii//TRANSLIT//IGNORE', $title);
            $url = preg_replace('#[^0-9a-z]+#i', '-', $url); // fixme: ???
        } else {
            die("iconv missing.");
        }

        $urlKey = strtolower($url);

        $isUnique = $this->checkUrlKeyDuplicates($urlKey, $dictionary);
        if ($isUnique) {
            return $urlKey;
        } else {
            return $urlKey . '-' . time();
        }
    }


    /**
     * @param $key
     * @param $dictionary
     * @return bool
     */
    private function checkUrlKeyDuplicates($key, $dictionary)
    {
        $isUnique = true;

        if ( array_key_exists($key,$dictionary) ) {
            $isUnique = false;
        }

        return $isUnique;
    }

    public function get_attribute_value_from_product($magento_product, $attribute_code) {
        if ( is_object($magento_product) && property_exists($magento_product, "id") ) {
            foreach ($magento_product->custom_attributes as $attr) {
                if ($attr->attribute_code == $attribute_code) {
                    return $attr->value;
                }
            }
        }
        return false;
    }
}
