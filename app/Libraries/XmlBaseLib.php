<?php

namespace App\Libraries;

use Spatie\ArrayToXml\ArrayToXml;
use XmlValidator\XmlValidator;
use App\Http\Controllers\InvoiceStatusController;
use App\Libraries\NAV\Connector\Connector;

class XmlBaseLib
{

  protected $raw;
  protected $source;
  protected $config;
  protected $xml;
  protected $xsd;
  protected $requestID;
  private $sendApi;

  public function __construct()
  {
      $this->source = [];
      $this->sendApi = env('SENDAPI',false);
  }

  public function generateXml(){
      $this->xml = ArrayToXml::convert($this->source,['rootElementName' => $this->config['rootElementName'],'_attributes' => $this->config['attr']], false, $this->config['encode']);
      return $this;
  }

  public function getXml($isencoded = true){
    if($isencoded) return base64_encode($this->xml);
    return $this->xml;
  }


  public function validateXml(){
      $xmlValidator = new XmlValidator($this->xml, $this->xsd);
      try{
          $xmlValidator->validate($this->xml,$this->xsd);

          // Check if is valid
          if(!$xmlValidator->isValid()){
              // Do whatever with the errors.
              $errors = [];
              foreach ($xmlValidator->errors as $error) {
                  $errors[] = sprintf('[%s %s] %s (in %s - line %d, column %d)',
                      $error->level, $error->code, $error->message,
                      $error->file, $error->line, $error->column
                  );
              }
              return ['isvalid' => false, 'errors' => $errors];
          }
          else{
            return ['isvalid' => true, 'errors' => null];
          }
      } catch (\InvalidArgumentException $e){
         return ['isvalid' => false, 'errors' => null];;
      }
  }



  private function getClass(){
    $class = get_called_class ();
    $class = explode('\\', $class);
    return array_last($class);
  }

  public function send(){
    $class =  lcfirst($this->getClass());
    if(!$this->sendApi) return ['sent' => false, 'error' => false, 'data'=> null];
    $connector = new Connector($this->xml,$class);
    return ['sent' => true, 'error' => false, 'data'=> $connector->post()];
  }

    /**
     * @return mixed
     */
    public function getRequestId()
    {
        return $this->requestID;
    }


    protected function createTimestamp(){
      $date = \MyDate::now();
        $date->setTimezone('UTC');
        return ['full' => $date->format("Y-m-d\TH:i:s.000\Z"),'cleaned' => $date->format("YmdHis")];
    }
}
?>
