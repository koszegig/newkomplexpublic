<?php

namespace App\Logging;

use Monolog\Handler\RotatingFileHandler;

class CustomFilenames
{
    /**
     * Customize the given logger instance.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        foreach ($logger->getHandlers() as $handler) {
            if ($handler instanceof RotatingFileHandler) {
                 if (function_exists('posix_getpwuid')) {
                    $processUser = posix_getpwuid( posix_geteuid() );
                    $processName= $processUser[ 'name' ];
                } else {
                    $processName = 'windows';
                }
                $sapi = php_sapi_name();
                $handler->setFilenameFormat("{filename}-$sapi-$processName-{date}", 'Y-m-d');
            }
        }
    }
}
