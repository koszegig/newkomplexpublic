<?php

namespace App\Models\Types;
use App\Libraries\Database\QueryBuilderSP;
use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;
class TRendszerKodok1 extends BaseModel01Sys  {
   protected function setStoredProcedure(){
        //$this->storedProcedure = '  "01_sys".get_rendszer_kodok_1 (null) where kod_mezo= ?::public.a_vc_250';
        $this->storedProcedure = '  "01_sys".get_rendszer_kodok_2 (null,?::public.a_vc_30_nev_rovid)';
    }
      public function setFields(){
          $this->fields = collect([
									new Field("kodf_id", "a_id", true, false,null),
									new Field("kodf_fgl_id", "a_id", true, false,null),
									new Field("kod_mezo", "a_vc_30_nev_rovid", true, false,null),
									new Field("kod_mezo_nev", "a_vc_50_nev_hosszu", true, false,null),
									new Field("kod_mezo_leiras", "a_blob", true, false,null),
									new Field("kod_id", "a_id", true, false,null),
									new Field("kod_fgl_id", "a_id", true, false,null),
									new Field("kod_kod", "a_kodok_c1", true, false,null),
									new Field("kod_nev", "a_vc_50_nev_hosszu", true, false,null),
									new Field("kod_nev_leiras", "a_blob", true, false,null),
									new Field("kod_elo_nev", "a_vc_30_nev_rovid", true, false,null),
									new Field("kod_uto_nev", "a_vc_30_nev_rovid", true, false,null),
									new Field("kod_hosszu_nev", "a_vc_120", true, false,null),

                  ]);
      }
}
