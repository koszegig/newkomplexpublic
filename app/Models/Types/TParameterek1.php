<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class TParameterek1 extends BaseModel01Sys
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '  "01_sys".get_parameterek_1 () ';
    }

    public function setFields(){
      $this->fields = collect([
        new Field("par_id", "a_id", true, true),
        new Field("par_hiv_fgl_id", "a_id_mut", true, false),
        new Field("par_hiv_fogalom_nev", "a_vc_50_nev_hosszu", true, false),
        new Field("par_hiv_fogalom_kategoria_kod", "a_kodok_c1", true, false),
        new Field("par_fnk_id", "a_id_mut", true, false),
        new Field("par_megnevezes", "a_vc_254", true, false),
        new Field("par_ertek", "a_vc_254", true, false),
        new Field("par_opcionalis_in", "a_igen_nem_c1", true, false),
        new Field("par_opc_kod_nev", "a_vc_50_nev_hosszu", true, false),
        new Field("par_tipus_kod", "a_kodok_c1", true, false),
        new Field("par_tipus_kod_nev", "a_vc_50_nev_hosszu", true, false),
        new Field("par_fgl_id", "a_id_mut", true, false),
        new Field("par_fgl_fogalom_nev", "a_vc_50_nev_hosszu", true, false),
        new Field("par_fleir_nev", "a_vc_50_nev_hosszu", true, false),
        new Field("par_fleir_leiras", "a_blob", true, false),
        new Field("par_letre_felh_nev", "a_kodtipus_c10", true, false),
        new Field("par_letre_dat", "a_datum_ido", true, false),
      ]);
    }


}
