<?php

namespace App\Models\Types;
use App\Libraries\Field;
use App\Models\Base\BaseModel14nav;
class Tnavtranzakciotetelek1 extends BaseModel14nav  {
   protected function setStoredProcedure(){
        $this->storedProcedure = '  "14_nav".get_navtranzakcio_tetelek_1 (null)';
    }
      public function setFields(){
          $this->fields = collect([
						new Field("nvtrt_id", "a_id", true, false,null),
						new Field("nvtrt_nvtrf_id", "a_id_mut", true, false,null),
						new Field("nvtrt_inv_id", "a_id_mut", true, false,null),
						new Field("nvtrt_inv_bizf_id", "a_id_mut", true, false,null),
						new Field("nvtrt_inv_bizf_azon", "a_vc_30_nev_rovid", true, false,null),
						new Field("nvtrt_inv_bizf_esed_datum", "a_datum_ido", true, false,null),
						new Field("nvtrt_inv_bizf_telj_datum", "a_datum_ido", true, false,null),
						new Field("nvtrt_inv_status_kod", "a_kodok_c1", true, false,null),
						new Field("nvtrt_inv_status_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
						new Field("nvtrt_inv_invoicedata", "a_xml", true, false,null),
						new Field("nvtrt_inv_filename", "a_vc_254", true, false,null),
						new Field("nvtrt_index", "a_integer_2", true, false,null),
						new Field("nvtrt_letre_felh_nev", "a_kodtipus_c10", true, false,null),
						new Field("nvtrt_letre_dat", "a_datum_ido", true, false,null),
						new Field("nvtrt_inv_status", "a_vc_200", true, false,null),
						new Field("nvtrt_nvtrf_transactionid", "a_vc_30_nev_rovid", true, false,null),
						new Field("nvtrt_nvtrf_eles", "a_aktiv_boolean", true, false,null),
						new Field("nvtrt_nvtrf_version", "a_vc_15", true, false,null),
						new Field("nvtrt_nvtrf_aktive", "a_aktiv_boolean", true, false,null),
						new Field("nvtrt_nvtrf_senddatetime", "a_datum_ido", true, false,null),
						new Field("nvtrt_nvtrf_recevedatetime", "a_datum_ido", true, false,null),

                  ]);
      }
}

