<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class TMenu2 extends BaseModel01Sys
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '  "01_sys".get_menu_web_1 ( ?::public.a_uuid_id_mut)  ';
 //     $this->storedProcedure = '  "01_sys".get_menu_web_1 (null) ';
    }

    public function setFields(){
      $this->fields = collect([
        new Field("menu_id", "a_id", true, true),
        new Field("menu_gyoker", "a_integer_2", true, false),
        new Field("menu_gyokersorrend", "a_integer_2", true, false),
        new Field("menu_title", "a_vc_250", true, false),
        new Field("menu_link", "a_vc_200", true, false),
        new Field("menu_parentid", "a_id_mut", true, false),
        new Field("menu_letre_felh_nev", "a_felhasznalo_c10", true, false),
        new Field("menu_letre_dat", "a_datum_ido", true, false),
        new Field("menu_action", "a_vc_120", true, false),
        new Field("menu_sorrend", "a_integer_2", true, false),
        new Field("menu_image_index", "a_integer_2", true, false),
        new Field("menu_shortcut", "a_c8", true, false),
        new Field("menu_hlevel", "a_integer_2", true, false),
        new Field("szrpfnk_uj_eng", "a_aktiv_boolean", true, false),
        new Field("szrpfnk_modositas_eng", "a_aktiv_boolean", true, false),
        new Field("szrpfnk_torles_eng", "a_aktiv_boolean", true, false),
        new Field("szrpfnk_visible_eng", "a_aktiv_boolean", true, false),
      ]);
    }


}
