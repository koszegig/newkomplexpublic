<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel04Rak;
use App\Libraries\Field;

class TAzonositok1 extends BaseModel04Rak
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '  "04_rak".get_azonositok_3 ()';
    }

    public function setFields(){
      $this->fields = collect([
            new Field("azon_id", "a_id", true, true,null),
            new Field("azon_egyseg_prt_id", "a_id_mut", true, false,null),
            new Field("azon_prt_nev", "a_vc_100_nev_partner", true, false,null),
            new Field("azon_biztip_kod", "a_kodok_c1", true, false,null),
            new Field("azon_biztip_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("azon_ev", "a_c4", true, false,null),
            new Field("azon_nyito", "a_integer_1", true, false,null),
            new Field("azon_kezdo", "a_integer_1", true, false,null),
            new Field("azon_befejezo", "a_integer_1", true, false,null),
            new Field("azon_atadott", "a_integer_1", true, false,null),
            new Field("azon_letre_felh_nev", "a_kodtipus_c10", true, false,null),
            new Field("azon_letre_dat", "a_datum_ido", true, false,null),
            new Field("azon_rovidneve", "a_vc_20", true, false,null),
            new Field("azon_default", "a_aktiv_boolean", true, false,'string'),
            new Field("azon_nyilvar_hatas_in", "a_igen_nem_c1", true, false,null),
            new Field("azon_nyilvar_hatas_in_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("azon_rovidmegjegyzes", "a_vc_254", true, false,null),
            new Field("azon_megjegyzes", "a_blob", true, false,null),
            new Field("azon_nyomtatasipeldanyszam", "a_integer_2", true, false,null),
            new Field("azon_handle", "a_id_mut", true, false,null),
            new Field("azon_lezart", "a_aktiv_boolean", true, false,'string'),
            new Field("azon_nullakszama", "a_integer_2", true, false,null),
            new Field("azon_jovedeki_termek", "a_igen_nem_c1", true, false,null),
            new Field("azon_jovedeki_termek_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("azon_uuid", "a_uuid_id", true, false,null),
      ]);
    }


}
