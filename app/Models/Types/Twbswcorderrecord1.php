<?php

namespace App\Models\Types;
use App\Libraries\Database\QueryBuilderSP;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;
class Twbswcorderrecord1 extends BaseModel13webshops  {
    protected function setStoredProcedure(){
        $this->storedProcedure =   ' "13_webshops".get_wbs_wcorder_record_1(null) ';
    }
    public function setFields(){
        $this->fields = collect([
            new Field("wbswco_id", "a_id", true, false,null),
			new Field("wbswco_org_id", "a_id_mut", true, false,null),
			new Field("wbswco_order_number", "a_integer_10", true, false,null),
			new Field("wbswco_status", "a_vc_254", true, false,null),
			new Field("wbswco_currency", "a_c8", true, false,null),
			new Field("wbswco_total", "a_n_12_4", true, false,null),
			new Field("wbswco_subtotal", "a_n_12_4", true, false,null),
			new Field("wbswco_total_tax", "a_n_12_4", true, false,null),
			new Field("wbswco_total_shipping", "a_n_12_4", true, false,null),
			new Field("wbswco_cart_tax", "a_n_12_4", true, false,null),
			new Field("wbswco_shipping_tax", "a_n_12_4", true, false,null),
			new Field("wbswco_shipping_methods", "a_vc_254", true, false,null),
			new Field("wbswco_payment_method_id", "a_c3", true, false,null),
			new Field("wbswco_payment_method_title", "a_vc_254", true, false,null),
			new Field("wbswco_payment_paid", "a_aktiv_boolean", true, false,null),
			new Field("wbswco_note", "a_vc_1200", true, false,null),
			new Field("wbswco_customer_ip", "a_vc_254", true, false,null),
			new Field("wbswco_customer_org_id", "a_id_mut", true, false,null),
			new Field("wbswco_bizf_id", "a_id_mut", true, false,null),
			new Field("wbswco_bizf_azon", "a_vc_30_nev_rovid", true, false,null),
			new Field("wbswco_modosult", "a_aktiv_boolean", true, false,null),
			new Field("wbswco_letre_felh_nev", "a_kodtipus_c10", true, false,null),
			new Field("wbswco_letre_dat", "a_datum_ido", true, false,null),
			new Field("wbswco_uuid", "a_uuid_id", true, false,null),
			new Field("wbswco_wbswcob_id", "a_id", true, false,null),
			new Field("wbswco_wbswcob_wbswco_id", "a_id_mut", true, false,null),
			new Field("wbswco_wbswcob_org_id", "a_id_mut", true, false,null),
			new Field("wbswco_wbswcob_first_name", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcob_last_name", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcob_company", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcob_address_1", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcob_address_2", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcob_city", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcob_state", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcob_postcode", "a_vc_1200", true, false,null),
			new Field("wbswco_wbswcob_country", "a_c2", true, false,null),
			new Field("wbswco_wbswcob_email", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcob_phone", "a_vc_120", true, false,null),
			new Field("wbswco_wbswcob_uuid", "a_uuid_id", true, false,null),
			new Field("wbswco_wbswcos_id", "a_id", true, false,null),
			new Field("wbswco_wbswcos_wbswco_id", "a_id_mut", true, false,null),
			new Field("wbswco_wbswcos_org_id", "a_id_mut", true, false,null),
			new Field("wbswco_wbswcos_first_name", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcos_last_name", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcos_company", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcos_address_1", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcos_address_2", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcos_city", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcos_state", "a_vc_254", true, false,null),
			new Field("wbswco_wbswcos_postcode", "a_vc_1200", true, false,null),
			new Field("wbswco_wbswcos_country", "a_c2", true, false,null),
			new Field("wbswco_wbswcos_uuid", "a_uuid_id", true, false,null),
			new Field("wbswco_active", "a_aktiv_boolean", true, false,null),

        ]);
    }

}

