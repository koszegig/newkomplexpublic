<?php


namespace App\Models\Types;
use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class isValidPassword  extends BaseModel01Sys
{
    protected function setStoredProcedure()
    {
        $this->storedProcedure = '   "01_sys".isvalidpassword ( ?::public.a_vc_254, ?::public.a_vc_254 )';
    }

    public function setFields()
    {
        $this->fields = collect([
            new Field("isvalidpassword", "a_aktiv_boolean", true, true, null),
        ]);
    }

}
