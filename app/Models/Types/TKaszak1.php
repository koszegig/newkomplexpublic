<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;

class TKaszak1 extends BaseModel02Com
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '   "02_com".get_kaszak_1 ()';
    }

    public function setFields(){
      $this->fields = collect([
        new Field("kszk_id", "a_id", true, false,null),
        new Field("kszk_prt_id", "a_id_mut", true, false,null),
        new Field("kszk_kod", "a_c5", true, false,null),
        new Field("kszk_nev", "a_vc_50_nev_partner", true, false,null),
        new Field("kszk_aktiv", "a_aktiv_boolean", true, false,null),
        new Field("kszk_default", "a_aktiv_boolean", true, false,null),
        new Field("kszk_partner_kod", "a_kodok_c1", true, false,null),
        new Field("kszk_partner_tipus_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("kszk_letre_felh_nev", "a_kodtipus_c10", true, false,null),
        new Field("kszk_letre_dat", "a_datum_ido", true, false,null),
        new Field("kszk_prt_nev", "a_vc_100_nev_partner", true, false,null),
        new Field("kszk_pnztr_id", "a_id_mut", true, false,null),
        new Field("kszk_pnztr_nev", "a_vc_50_nev_partner", true, false,null),
        new Field("kszk_uuid", "a_uuid_id", true, false,null),
        new Field("kszk_uuid", "a_uuid_id", true, false,null),
      ]);
    }


}
