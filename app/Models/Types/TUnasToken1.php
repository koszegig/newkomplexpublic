<?php

namespace App\Models\Types;
use App\Libraries\Database\QueryBuilderSP;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;
class TUnasToken1 extends BaseModel13webshops  {
   protected function setStoredProcedure(){
        $this->storedProcedure = '  "13_webshops".get_unastoken_1 (?::public.a_vc_800)';
    }
      public function setFields(){
          $this->fields = collect([
              new Field("unstkn_id", "a_id", true, false,null),
              new Field("unstkn_uuid", "a_uuid_id", true, false,null),
              new Field("unstkn_token", "a_vc_800", true, false,null),
              new Field("unstkn_until", "a_datum_ido", true, false,null),
              new Field("unstkn_permissions", "a_vc_8000", true, false,null),
              new Field("unstkn_api", "a_vc_800", true, false,null),
              new Field("unstkn_letre_felh_nev", "a_kodtipus_c10", true, false,null),
              new Field("unstkn_letre_dat", "a_datum_ido", true, false,null),
          ]);
      }
     public static function getToken($unstknAPI){
            $typeModel = '\App\Models\Types\TUnasToken1';
            $typeSelect = [
                'unstkn_token'
            ];
            $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$unstknAPI]);
            $Token =  $builder->get()[0] ?? [];
            return $Token->unstkn_token ?? false;
    }
}
