<?php

namespace App\Models\Types;
use App\Models\Base\BaseModel06imprt;
use App\Libraries\Field;

class TKulcsszoftCikkek1 extends BaseModel06imprt  {

    protected function setStoredProcedure(){
        $this->storedProcedure = ' "06_imprt".get_kulcsszoft_cikkek_1(
				  null,
				  null,
				  null
				) ';
    }
    public function setFields()
    {
        $this->fields = collect([
            new Field("sorszam", "a_id", true, true, null),
            new Field("kod", "a_vc_200", true, false, null),
            new Field("megnevezes", "a_vc_200", true, false, null),
            new Field("afakod", "a_vc_200", true, false, null),
            new Field("afaszazlek", "a_vc_200", true, false, null),
            new Field("ar6", "a_vc_200", true, false, null),
            new Field("ar5", "a_vc_200", true, false, null),
            new Field("ar4", "a_vc_200", true, false, null),
            new Field("ar3", "a_vc_200", true, false, null),
            new Field("ar2", "a_vc_200", true, false, null),
            new Field("ar1", "a_vc_200", true, false, null),
            new Field("egyseg", "a_vc_200", true, false, null),
            new Field("csopkod", "a_vc_200", true, false, null),
            new Field("csoport", "a_vc_200", true, false, null),
            new Field("gyartokod", "a_vc_200", true, false, null),
            new Field("gyartonev", "a_vc_200", true, false, null),
            new Field("idegennev", "a_vc_200", true, false, null),
            new Field("idegenmerteke", "a_vc_200", true, false, null),
            new Field("tkodgyartonal", "a_vc_200", true, false, null),
            new Field("besorolas", "a_vc_200", true, false, null),
            new Field("vonalkod", "a_vc_200", true, false, null),
            new Field("vtsz", "a_vc_200", true, false, null),
            new Field("csoport2", "a_vc_200", true, false, null),
            new Field("csoport3", "a_vc_200", true, false, null),
            new Field("mennyiseg", "a_vc_200", true, false, null),
            new Field("tipus", "a_vc_200", true, false, null),
            new Field("jovedekitermek", "a_vc_200", true, false, null),
            new Field("keszletmod", "a_vc_200", true, false, null),
            new Field("szavidos", "a_vc_200", true, false, null),
            new Field("kiszereles", "a_vc_200", true, false, null),
            new Field("alkoholfok", "a_vc_200", true, false, null),
            new Field("forditottafa", "a_vc_200", true, false, null),
            new Field("uuid", "a_vc_200", true, false, null),
        ]);
    }
}
