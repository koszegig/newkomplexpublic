<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel04Rak;
use App\Libraries\Field;

class TAzonositokbymenuid2 extends BaseModel04Rak
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '  "04_rak".get_azonositokbymenuid_2 (
        ?::public.a_id,
        ?::public.a_id
      )';
    }

    public function setFields(){
      $this->fields = collect([
            new Field("get_azonositokbymenuid_2", "a_id", true, true,null)
      ]);
    }


}
