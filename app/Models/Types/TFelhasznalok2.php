<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class TFelhasznalok2 extends BaseModel01Sys
{
    protected function setStoredProcedure(){
        $this->storedProcedure = '  "01_sys".get_felhasznalo_1 ( ?::public.a_id)';
    }

    public function setFields(){
        $this->fields = collect([
            new Field("felh_userid", "a_id", true, true),
            new Field("felh_felhnev", "a_felhasznalo_c10", true, false),
            new Field("felh_jelszo", "a_kodtipus_c10", true, false),
            new Field("felh_ervenyesseg", "a_datum", true, false),
            new Field("felh_prt_id", "a_id_mut", true, false),
            new Field("felh_prt_prt_id", "a_id_mut", true, false),
            new Field("felh_prt_prt_nev", "a_vc_100_nev_partner", true, false),
            new Field("felh_prt_nev", "a_vc_100_nev_partner", true, false),
            new Field("felh_jogszintid", "a_id_mut", true, false),
            new Field("felh_pcname", "a_vc_250", true, false),
            new Field("felh_visible", "a_aktiv_boolean", true, false),
            new Field("felh_bejelentkezve", "a_aktiv_boolean", true, false),
            new Field("felh_jelszocsere", "a_aktiv_boolean", true, false),
            new Field("felh_letre_felh_nev", "a_felhasznalo_c10", true, false),
            new Field("felh_letre_dat", "a_datum_ido", true, false),
            new Field("felh_superkod", "a_c7", true, false),
            new Field("felh_usesysid", "a_id_mut", true, false),
            new Field("felh_attributes", "a_vc_200", true, false),
            new Field("felh_rakt_id", "a_id_mut", true, false),
            new Field("felh_rakt_nev", "a_vc_50_nev_partner", true, false),
            new Field("felh_aktiv", "a_aktiv_boolean", true, false),
            new Field("felh_cikk_kereses", "a_kodok_c1", true, false),
            new Field("felh_cikk_kereses_kod_nev", "a_vc_50_nev_hosszu", true, false),
            new Field("felh_kszk_id", "a_id_mut", true, false),
            new Field("felh_kszk_kod", "a_c5", true, false),
            new Field("felh_kszk_nev", "a_vc_50_nev_partner", true, false),
            new Field("felh_bizonylatidoszak_kod", "a_kodok_c1", true, false),
            new Field("felh_bizonylatidoszak_kod_nev", "a_vc_50_nev_hosszu", true, false),
            new Field("felh_cikk_szum_keszlet_kod", "a_kodok_c1", true, false),
            new Field("felh_cikk_szum_keszlet_kod_nev", "a_vc_50_nev_hosszu", true, false),
            new Field("felh_auto_r_nyomtatasa", "a_aktiv_boolean", true, false),
            new Field("felh_modositasimod_kod", "a_kodok_c1", true, false),
            new Field("felh_modositasimod_kod_nev", "a_vc_50_nev_hosszu", true, false),
            new Field("felh_hibas_bejelentkezes", "a_integer_2", true, false),
            new Field("felh_volserialnum", "a_vc_254", true, false),
            new Field("felh_ertekesites_kod", "a_kodok_c1", true, false),
            new Field("felh_ertekesites_kod_nev", "a_vc_50_nev_hosszu", true, false),
            new Field("felh_web_users_id", "a_uuid_id_mut", true, false),
            new Field("felh_users_name", "a_vc_254", true, false),
            new Field("felh_users_email", "a_vc_254", true, false),
            new Field("felh_rtmbl_id", "a_id_mut", true, false),
            new Field("felh_rtmbl_nev", "a_vc_50_nev_partner", true, false),
        ]);
    }


}
