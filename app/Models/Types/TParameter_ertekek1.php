<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class TParameter_ertekek1 extends BaseModel01Sys
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '  "01_sys".get_parameter_ertekek_1 (null)';
    }

    public function setFields(){
      $this->fields = collect([
        new Field("parert_id", "a_id", true, true),
        new Field("parert_par_id", "a_id_mut", true, false),
        new Field("parert_par_megnevezes", "a_vc_254", true, false),
        new Field("parert_nev", "a_vc_250", true, false),
        new Field("parert_ertek", "a_vc_254", true, false),
        new Field("parert_letre_felh_nev", "a_kodtipus_c10", true, false),
        new Field("parert_letre_dat", "a_datum_ido", true, false),
      ]);
    }


}
