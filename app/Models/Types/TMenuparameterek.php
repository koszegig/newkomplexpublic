<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class TMenuparameterek extends BaseModel01Sys
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '   "01_sys".get_menuparameterek_1 (?::public.a_id_mut) ';
    }

    public function setFields(){
      $this->fields = collect([

       	new Field("menuprm_id", "a_id", true, false,null),
		new Field("menuprm_menu_id", "a_id_mut", true, false,null),
        new Field("menuprm_name", "a_vc_20", true, false,null),
        new Field("menuprm_value", "a_vc_120", true, false,null),
        new Field("menuprm_letre_felh_nev", "a_kodtipus_c10", true, false,null),
        new Field("menuprm_letre_dat", "a_datum_ido", true, false,null),
      ]);
    }


}
