<?php

namespace App\Models\Types;
use App\Libraries\Database\QueryBuilderSP;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;
class TWebTasks1 extends BaseModel13webshops  {
    protected function setStoredProcedure(){
        $this->storedProcedure = '  ""13_webshops".get_webtasks_1 (?::public.a_vc_254)';
    }
    public function setFields(){
        $this->fields = collect([
            new Field("wbtsks_id", "a_id", true, false,null),
            new Field("wbtsks_tasktype", "a_vc_254", true, false,null),
            new Field("wbtsks_start", "a_datum_ido", true, false,null),
            new Field("wbtsks_lasttick", "a_datum_ido", true, false,null),
            new Field("wbtsks_stop", "a_datum_ido", true, false,null),
            new Field("wbtsks_pid", "a_integer_40", true, false,null),
            new Field("wbtsks_statuscode", "a_integer_40", true, false,null),
            new Field("wbtsks_stopme", "a_aktiv_boolean", true, false,null),
            new Field("wbtsks_limit", "a_integer_40", true, false,null),
            new Field("wbtsks_letre_felh_nev", "a_kodtipus_c10", true, false,null),
            new Field("wbtsks_letre_dat", "a_datum_ido", true, false,null),
            new Field("wbtsks_uuid", "a_uuid_id", true, false,null),
            new Field("wbtsks_cron", "a_vc_254", true, false,null),
            new Field("wbtsks_kasszazaraskor", "a_aktiv_boolean", true, false,null),
            new Field("wbtsks_indulhat", "a_aktiv_boolean", true, false,null),
        ]);
    }
    public static function getTask($tasktype){
        $typeModel = '\App\Models\Types\TWebTasks1';
        $typeSelect = [
            'wbtsks_id'
        ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$tasktype]);
        $Task =  $builder->get()[0] ?? [];
        return $Token->wbtsks_id ?? false;
    }
}
