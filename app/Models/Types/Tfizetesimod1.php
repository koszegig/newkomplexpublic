<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;

class Tfizetesimod1 extends BaseModel02Com
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '  "02_com".get_fizetesimod_1 ( )';
    }

    public function setFields(){
      $this->fields = collect([
        new Field("fztm_id", "a_id", true, false,null),
        new Field("fztm_megnevezes", "a_vc_50_nev_hosszu", true, false,null),
        new Field("fztm_tipus_kod", "a_kodok_c1", true, false,null),
        new Field("fztm_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("fztm_fizetesinapok", "a_integer_2", true, false,null),
        new Field("fztm_felh_nev", "a_kodtipus_c10", true, false,null),
        new Field("fztm_letre_dat", "a_datum_ido", true, false,null),
        new Field("fztm_kassza", "a_aktiv_boolean", true, false,null),
        new Field("fztm_kassza_sorrend", "a_integer_2", true, false,null),
        new Field("fztm_alap", "a_aktiv_boolean", true, false,null),
        new Field("fztm_kerekites", "a_aktiv_boolean", true, false,null),
        new Field("fztm_szamlan_a_megnevezes", "a_vc_30_nev_rovid", true, false,null),
        new Field("fztm_nyomtatasipeldanyszam", "a_integer_2", true, false,null),
        new Field("fztm_azonalifizetes", "a_aktiv_boolean", true, false,null),
        new Field("fztm_vegosszegkerekitesmerteke", "a_integer_2", true, false,null),
        new Field("fztm_paymentmethod_kod", "a_kodok_c1", true, false,null),
        new Field("fztm_paymentmethod_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("fztm_uuid", "a_uuid_id", true, false,null),
      ]);
    }


}
