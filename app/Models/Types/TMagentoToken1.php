<?php

namespace App\Models\Types;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;
class TMagentoToken1 extends BaseModel13webshops  {
   protected function setStoredProcedure(){
        $this->storedProcedure = '  "13_webshops".get_magentotoken_1 ( ?::public.a_vc_100_nev_partner
                                     )';
    }
      public function setFields(){
          $this->fields = collect([
              new Field("mgtkn_id", "a_id", true, false,null),
              new Field("mgtkn_uuid", "a_uuid_id", true, false,null),
              new Field("mgtkn_username", "a_vc_100_nev_partner", true, false,null),
              new Field("mgtkn_rp_token", "a_vc_800", true, false,null),
              new Field("mgtkn_until", "a_datum_ido", true, false,null),
              new Field("mgtkn_letre_felh_nev", "a_kodtipus_c10", true, false,null),
              new Field("mgtkn_letre_dat", "a_datum_ido", true, false,null),
          ]);
      }
}
