<?php

namespace App\Models\Types;
use App\Libraries\Database\QueryBuilderSP;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;
class TWebQueues1 extends BaseModel13webshops  {
   protected function setStoredProcedure(){
        $this->storedProcedure = '  "13_webshops".get_webqueues_1 (
                                      ?::public.a_vc_254,
                                      ?::public.a_vc_254
                                    )';
    }
      public function setFields(){
          $this->fields = collect([
              new Field("wbque_id", "a_id", true, false,null),
              new Field("wbque_tasktype", "a_vc_254", true, false,null),
              new Field("wbque_queue", "a_vc_254", true, false,null),
              new Field("wbque_ready", "a_aktiv_boolean", true, false,null),
              new Field("wbque_jsonmessage", "a_json", true, false,null),
              new Field("wbque_xmlmessage", "a_xml", true, false,null),
              new Field("wbque_letre_felh_nev", "a_kodtipus_c10", true, false,null),
              new Field("wbque_letre_dat", "a_datum_ido", true, false,null),
              new Field("wbque_uuid", "a_uuid_id", true, false,null),
          ]);
      }
    public static function getQueue($tasktype,$queue){
        $typeModel = '\App\Models\Types\TWebQueues1';
        $typeSelect = [
            'wbque_id',
            'wbque_queue',
            'wbque_ready',
            'wbque_jsonmessage',
            'wbque_xmlmessage',
        ];
        $builder = new QueryBuilderSP($typeModel, [],null, $typeSelect,[$tasktype,$queue]);

        $Queue =  $builder->get()->toArray();
        return $Queue;
    }
}
