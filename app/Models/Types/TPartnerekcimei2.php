<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;

class TPartnerekcimei2 extends BaseModel02Com
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '   "02_com".get_partnerek_cimei_2 ( null)';
    }

    public function setFields(){
      $this->fields = collect([
        new Field("prt_cim_id", "a_id", true, false,null),
        new Field("prt_cim_prt_id", "a_id_mut", true, false,null),
        new Field("prt_cim_tipus_kod", "a_kodok_c1", true, false,null),
        new Field("prt_cim_tipus_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("prt_cim_helys_id", "a_id_mut", true, false,null),
        new Field("prt_cim_orsz_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("prt_cim_helys_megye_nev", "a_vc_30_nev_rovid", true, false,null),
        new Field("prt_cim_helys_irsz", "a_c7", true, false,null),
        new Field("prt_cim_helys_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("prt_cim_kozterulet_jelleg", "a_kodok_c1", true, false,null),
        new Field("prt_cim_kozterulet_jelleg_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("prt_cim_kozterulet_nev", "a_vc_120", true, false,null),
        new Field("prt_cim_hazszam", "a_c7", true, false,null),
        new Field("prt_cim_epulet", "a_c7", true, false,null),
        new Field("prt_cim_lepcsohaz", "a_c7", true, false,null),
        new Field("prt_cim_emelet", "a_c7", true, false,null),
        new Field("prt_cim_ajto", "a_c4", true, false,null),
        new Field("prt_letre_felh_nev", "a_felhasznalo_c10", true, false,null),
        new Field("prt_letre_dat", "a_datum_ido", true, false,null),
        new Field("prt_valasztott", "a_aktiv_boolean", true, false,null),
        new Field("prt_teljescim", "a_vc_800", true, false,null),
        new Field("prt_telephely_megnevezese", "a_vc_100_nev_partner", true, false,null),
        new Field("prt_telephelyszam", "a_vc_20", true, false,null),
        new Field("prt_kerulet", "a_c3", true, false,null),
        new Field("prt_cim_kozterulet_jelleg_id", "a_id_mut", true, false,null),
        new Field("prt_cim_helyrajziszam", "a_vc_50_nev_hosszu", true, false,null),
        new Field("prt_cim_uuid", "a_uuid_id", true, false,null),
      ]);
    }


}
