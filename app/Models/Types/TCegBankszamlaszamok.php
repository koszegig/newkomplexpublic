<?php


namespace App\Models\Types;
use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;

class TCegBankszamlaszamok  extends BaseModel02Com
{
    protected function setStoredProcedure()
    {
        $this->storedProcedure = '   "02_com".get_bankszamlaszamok_2 ( ? )';
    }

    public function setFields()
    {
        $this->fields = collect([
            new Field("bsz_id", "a_id", true, false, null),
            new Field("bsz_prt_id", "a_id_mut", true, false, null),
            new Field("bsz_pfjszk", "a_c1", true, false, null),
            new Field("bsz_bankszamlaszam", "a_vc_50_szamlaszam", true, false, null),
            new Field("bsz_atfutasi_napok", "a_integer_2", true, false, null),
            new Field("bsz_iban", "a_vc_50_szamlaszam", true, false, null),
            new Field("bsz_letre_felh_nev", "a_kodtipus_c10", true, false, null),
            new Field("bsz_letre_dat", "a_datum_ido", true, false, null),
            new Field("bsz_tipus_kod", "a_kodok_c1", true, false, null),
            new Field("bsz_bank_id", "a_id_mut", true, false, null),
            new Field("bsz_valasztott", "a_aktiv_boolean", true, false, null),
            new Field("bsz_uuid", "a_uuid_id", true, false, null),
        ]);
    }


}

