<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel03Cik;
use App\Libraries\Field;


class TMennyisegiegysegek1 extends BaseModel03Cik
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '  "03_cik".get_mennyisegiegysegek_1()';
    }

    public function setFields(){
      $this->fields = collect([
        new Field("me_id", "a_id", true, false,null),
        new Field("me_rovidites", "a_c5", true, false,null),
        new Field("me_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("me_letre_felh_nev", "a_kodtipus_c10", true, false,null),
        new Field("me_letre_dat", "a_datum_ido", true, false,null),
        new Field("me_aktiv", "a_aktiv_boolean", true, false,null),
        new Field("me_decimal", "a_n_12_4", true, false,null),
        new Field("me_navme_id", "a_id_mut", true, false,null),
        new Field("me_navme_megnevezes", "a_vc_25", true, false,null),
        new Field("navme_unitofmeasuretype", "a_vc_25", true, false,null),
      ]);
    }


}
