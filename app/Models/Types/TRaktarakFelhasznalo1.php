<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;

class TRaktarakFelhasznalo1 extends BaseModel02Com
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '   "02_com".get_raktarak_2( ?::public.a_uuid_id ) ';
    }

    public function setFields(){
      $this->fields = collect([

        new Field("rakt_id", "a_id", true, true,null),
        new Field("rakt_prt_id", "a_id_mut", true, false,null),
        new Field("rakt_prt_nev", "a_vc_100_nev_partner", true, false,null),
        new Field("rakt_nev", "a_vc_50_nev_partner", true, false,null),
        new Field("rakt_letre_felh_nev", "a_kodtipus_c10", true, false,null),
        new Field("rakt_letre_dat", "a_datum_ido", true, false,null),
        new Field("rakt_aktiv", "a_aktiv_boolean", true, false,null),
        new Field("rakt_jovedeki_termek", "a_igen_nem_c1", true, false,null),
        new Field("rakt_jovedeki_termek_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("rakt_rendezo", "a_integer_2", true, false,null),
        new Field("rakt_status_ertekesites_kod", "a_kodok_c1", true, false,null),
        new Field("rakt_status_ertekesites_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("rakt_uuid", "a_uuid_id", true, false,null),
      ]);
    }


}
