<?php

namespace App\Models\Types;
use App\Libraries\Field;
use App\Models\Base\BaseModel14nav;
class Tnavtasks1 extends BaseModel14nav  {
    public $table = '14_nav.navtasks';
    protected $alias = 'bizf';
   protected function setStoredProcedure(){
        $this->storedProcedure = '  "14_nav".get_navtasks_1 (null)';
    }
      public function setFields(){
          $this->fields = collect([
									new Field("nvtsks_id", "a_id", true, false,null),
									new Field("nvtsks_tasktype", "a_vc_254", true, false,null),
									new Field("nvtsks_start", "a_datum_ido", true, false,null),
									new Field("nvtsks_lasttick", "a_datum_ido", true, false,null),
									new Field("nvtsks_stop", "a_datum_ido", true, false,null),
									new Field("nvtsks_pid", "a_integer_40", true, false,null),
									new Field("nvtsks_statuscode", "a_integer_40", true, false,null),
									new Field("nvtsks_stopme", "a_aktiv_boolean", true, false,null),
									new Field("nvtsks_limit", "a_integer_40", true, false,null),
									new Field("nvtsks_letre_felh_nev", "a_kodtipus_c10", true, false,null),
									new Field("nvtsks_letre_dat", "a_datum_ido", true, false,null),
									new Field("nvtsks_uuid", "a_uuid_id", true, false,null),
									new Field("nvtsks_cron", "a_vc_254", true, false,null),
									new Field("nvtsks_kasszazaraskor", "a_aktiv_boolean", true, false,null),
									new Field("nvtsks_indulhat", "a_aktiv_boolean", true, false,null),
                  ]);
      }
}
