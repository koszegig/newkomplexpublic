<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel03Cik;
use App\Libraries\Field;

class TCikkek3 extends BaseModel03Cik
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '  "03_cik".get_cikkek_3()';
    }

    public function setFields(){
      $this->fields = collect([
        new Field("cik_id", "a_id", true, false),
        new Field("cik_kod", "a_vc_15", false, false),
        new Field("cik_nev", "a_vc_200", false, false),
        new Field("cik_vtsz_id", "a_id_mut", false, false),
        new Field("cik_vtszafa_afak_kod", "a_c3", false, false),
        new Field("cik_vtszafa_afa_nev", "a_vc_30_nev_rovid", false, false),
        new Field("cik_vtszafa_vtsz_szam", "a_vc_15", false, false),
        new Field("cik_vtszafa_vtsz_nev", "a_vc_30_nev_rovid", false, false),
        new Field("cik_vtszafa_vtsz_szavidos", "a_kodok_c1", false, false),
        new Field("cik_me_id", "a_id_mut", false, false),
        new Field("cik_me_rovidites", "a_c5", false, false),
        new Field("cik_me_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_aktiv", "a_aktiv_boolean", false, false),
       // new Field("cik_megjegyzes", "a_blob", false, false),
        new Field("cik_kep", "a_vc_200", false, false),
        new Field("cik_tipus_kod", "a_kodok_c1", false, false),
        new Field("cik_tipus_kod_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_keszlet_mod_kod", "a_kodok_c1", false, false),
        new Field("cik_keszlet_mod_kod_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_jovedeki_termek_kod", "a_kodok_c1", false, false),
        new Field("cik_jovedeki_termek_kod_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_arlistas_kod", "a_kodok_c1", false, false),
        new Field("cik_arlistas_kod_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_szavidos_kod", "a_kodok_c1", false, false),
        new Field("cik_szavidos_kod_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_letre_felh_nev", "a_kodtipus_c10", false, false),
        new Field("cik_letre_dat", "a_datum_ido", false, false),
        new Field("cik_afa_id", "a_id_mut", false, false),
        new Field("cik_afa_nev", "a_vc_30_nev_rovid", false, false),
        new Field("cikk_handle", "a_id_mut", false, false),
        new Field("cik_suly", "a_n_6_5", false, false),
        new Field("cik_suly_me_id", "a_id_mut", false, false),
        new Field("cik_sulyme_rovidites", "a_c5", false, false),
        new Field("cik_sulyme_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_fixaras_kod", "a_kodok_c1", false, false),
        new Field("cik_fixaras_kod_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_beszerzersinettoar", "a_n_12_3", false, false),
        new Field("cik_beszerzersibruttoar", "a_n_12_3", false, false),
        new Field("cik_egyseg_ar", "a_n_12_3", false, false),
        new Field("cik_cgy_gyariszam", "a_vc_30_nev_rovid", false, false),
        new Field("cik_kiskernettoar", "a_n_12_3", false, false),
        new Field("cik_kiskerbruttoar", "a_n_12_3", false, false),
        new Field("cik_forditottafa", "a_kodok_c1", false, false),
        new Field("cik_forditottafa_kod_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_netadotk_id", "a_id_mut", false, false),
        new Field("cik_netadotk_megnevezes", "a_vc_254", false, false),
        new Field("cik_trmkdj_id", "a_id_mut", false, false),
        new Field("cik_trmkdj_megnevezes", "a_vc_254", false, false),
        new Field("cik_armeny_szorzo", "a_integer_8", false, false),
        new Field("cik_armeny_me_id", "a_id_mut", false, false),
        new Field("cik_armeny_me_rovidites", "a_c5", false, false),
        new Field("cik_armeny_me_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_csatolt_dokumentum_path", "a_vc_800", false, false),
        new Field("cik_csatolt_tervrajz", "a_vc_800", false, false),
        new Field("cikkek_cikcsop_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_kozv_szolgaltatas", "a_kodok_c1", false, false),
        new Field("cik_kozv_szolgaltatas_kod_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_szigoruszamadasu", "a_aktiv_boolean", false, false),
        new Field("cik_lapszam", "a_integer_8", false, false),
        new Field("cik_beszmodidopontja", "a_datum_ido", false, false),
        new Field("cik_vonal_azon", "a_vc_20", false, false),
        new Field("cik_kiszereles", "a_n_3_2", false, false),
        new Field("cik_alkoholfok", "a_n_3_2", false, false),
        new Field("cik_be_rakt_id", "a_id_mut", false, false),
        new Field("cik_be_rakt_nev", "a_vc_50_nev_partner", false, false),
        new Field("cik_ki_rakt_id", "a_id_mut", false, false),
        new Field("cik_ki_rakt_nev", "a_vc_50_nev_partner", false, false),
        new Field("cik_nagykernettoar", "a_n_12_3", false, false),
        new Field("cik_nagykerbruttoar", "a_n_12_3", false, false),
        new Field("cik_kiszereles_me_id", "a_id_mut", false, false),
        new Field("cik_kiszereles_me_rovidites", "a_c5", false, false),
        new Field("cik_kiszereles_me_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_kiskernettoegysregar", "a_n_12_3", false, false),
        new Field("cik_kiskerbruttoegysregiar", "a_n_12_3", false, false),
        new Field("cik_nagykernettoegysregar", "a_n_12_3", false, false),
        new Field("cik_nagykerbruttoegysregiar", "a_n_12_3", false, false),
        new Field("cik_egysegar_me_id", "a_id_mut", false, false),
        new Field("cik_egysegar__me_rovidites", "a_c5", false, false),
        new Field("cik_egysegar__me_nev", "a_vc_50_nev_hosszu", false, false),
        new Field("cik_uuid", "a_uuid_id", false, false),
      ]);
    }


}
