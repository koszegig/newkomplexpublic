<?php

namespace App\Models\Types;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;

class TPenznem1 extends BaseModel02Com
{
    protected function setStoredProcedure(){
      $this->storedProcedure = '   "02_com".get_penznem_1() ';
    }

    public function setFields(){
      $this->fields = collect([

        new Field("pnzn_id", "a_id", true, true,null),
        new Field("pnzn_rovidites", "a_c5", true, false,null),
        new Field("pnzn_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("pnzn_letre_felh_nev", "a_kodtipus_c10", true, false,null),
        new Field("pnzn_letre_dat", "a_datum_ido", true, false,null),
        new Field("pnzn_aktiv", "a_aktiv_boolean", true, false,null),
        new Field("pnzn_default", "a_aktiv_boolean", true, false,null),
        new Field("pnzn_kerekit", "a_aktiv_boolean", true, false,null),
        new Field("pnzn_szamlaszam", "a_vc_50_szamlaszam", true, false,null),
        new Field("pnzn_jel", "a_c5", true, false,null),
        new Field("pnzn_kerekitesmerteke", "a_integer_2", true, false,null),
        new Field("pnzn_uuid", "a_uuid_id", true, false,null),
      ]);
    }


}
