<?php


namespace App\Models\Types;
use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;


class TPartnerek4 extends BaseModel02Com
{
    protected function setStoredProcedure(){
        $this->storedProcedure = '   "02_com".get_partnerek_4( ? ::public.a_id)';
    }

    public function setFields(){
        $this->fields = collect([
            new Field("prt_id", "a_id", true, true,null),
            new Field("prt_prt_id", "a_id_mut", true, false,null),
            new Field("prt_aktiv", "a_aktiv_boolean", true, false,null),
            new Field("prt_nev", "a_vc_100_nev_partner", true, false,null),
            new Field("prt_elonev", "a_vc_50_nev_partner", true, false,null),
            new Field("prt_utonev", "a_vc_50_nev_partner", true, false,null),
            new Field("prt_letre_felh_nev", "a_felhasznalo_c10", true, false,null),
            new Field("prt_letre_dat", "a_datum_ido", true, false,null),
            new Field("prt_adoszam", "a_vc_15", true, false,null),
            new Field("prt_cegjegyzekszam", "a_vc_20", true, false,null),
            new Field("prt_kozossegiszam", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_cegkod", "a_vc_20", true, false,null),
            new Field("prt_ugynok_prt_id", "a_id_mut", true, false,null),
            new Field("prt_ugynok_prt_nev", "a_vc_100_nev_partner", true, false,null),
            new Field("prt_partner_tipus_kod", "a_kodok_c1", true, false,null),
            new Field("prt_partner_tipus_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_fztm_id", "a_id_mut", true, false,null),
            new Field("prt_fztm_megnevezes", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_szamlazasi_limit", "a_n_9_3", true, false,null),
            new Field("prt_artbl_arkategoria_kod", "a_kodok_c1", true, false,null),
            new Field("prt_artbl_arkategoria_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_utalasi_nap", "a_integer_2", true, false,null),
            new Field("prt_szallito_vevo_tipus_kod", "a_kodok_c1", true, false,null),
            new Field("prt_szallito_vevo_tipus_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_tiltas_kod", "a_igen_nem_c1", true, false,null),
            new Field("prt_tiltas_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_ujsag_kod", "a_igen_nem_c1", true, false,null),
            new Field("prt_ujsag_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_szallitasi_koltseg", "a_n_9_3", true, false,null),
            new Field("prt_visszateritesi_koltseg", "a_n_5_3", true, false,null),
            new Field("prt_gln_kod_1", "a_vc_20", true, false,null),
            new Field("prt_gln_kod_2", "a_vc_20", true, false,null),
            new Field("prt_eu_afa_reg_szam", "a_vc_20", true, false,null),
            new Field("prt_afa_csoport_azonosito", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_jovedeki_engedelyszam", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_mokudesi_engedely_szam", "a_vc_50_nev_partner", true, false,null),
            new Field("prt_handle", "a_id_mut", true, false,null),
            new Field("prt_megjegyzes", "a_blob", true, false,null),
            new Field("prt_egyenikedvezmeny", "a_n_5_3", true, false,null),
            new Field("prt_programothasznalo", "a_aktiv_boolean", true, false,null),
            new Field("prt_ckcsmf_id", "a_id_mut", true, false,null),
            new Field("prt_ckcsmf_nev", "a_vc_200", true, false,null),
            new Field("prt_alap_teljescime", "a_vc_254", true, false,null),
            new Field("prt_bsz_bankszamlaszam", "a_vc_50_szamlaszam", true, false,null),
            new Field("prt_prtelhtsg_mobilszam", "a_vc_20", true, false,null),
            new Field("prt_forditottafa", "a_kodok_c1", true, false,null),
            new Field("prt_forditottafa_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_karbegy_darabszam", "a_integer_8", true, false,null),
            new Field("prt_kapjonlevelet_kod", "a_igen_nem_c1", true, false,null),
            new Field("prt_kapjonlevelet_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_beveteli_kedvezmeny", "a_n_5_3", true, false,null),
            new Field("prt_default_biztip_kod", "a_kodok_c1", true, false,null),
            new Field("prt_default_biztip_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_szamlazasi_egyszeri_limit", "a_n_9_3", true, false,null),
            new Field("prt_prt_nev", "a_vc_100_nev_partner", true, false,null),
            new Field("prt_prt_adoszam", "a_vc_15", true, false,null),
            new Field("prt_prt_cegjegyzekszam", "a_vc_20", true, false,null),
            new Field("prt_vipd", "a_vc_20", true, false,null),
            new Field("prt_nebih", "a_vc_20", true, false,null),
            new Field("prt_kisadozo", "a_igen_nem_c1", true, false,null),
            new Field("prt_kisadozo_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_egyeni_vallalkozo", "a_igen_nem_c1", true, false,null),
            new Field("prt_egyeni_vallalkozo_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_penzforgelsz", "a_igen_nem_c1", true, false,null),
            new Field("prt_penzforgelsz_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_neta", "a_igen_nem_c1", true, false,null),
            new Field("prt_neta_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_ev_neve", "a_vc_100_nev_partner", true, false,null),
            new Field("prt_adoazonosito_jel", "a_vc_15", true, false,null),
            new Field("prt_adoszam_helyes", "a_kodok_c1", true, false,null),
            new Field("prt_adoszam_helyes_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("prt_adoszam_ellenorzes", "a_aktiv_boolean", true, false,null),
            new Field("prt_prtkedv_sum_netto_ert", "a_n_12_3", true, false,null),
            new Field("prt_prtkedv_sum_brutto_ert", "a_n_12_3", true, false,null),
            new Field("prt_prtelhtsg_emailcim", "a_vc_120", true, false,null),
            new Field("prt_prtelhtsg_weboldal", "a_vc_120", true, false,null),
            new Field("prt_uuid", "a_uuid_id", true, false,null),
        ]);
    }


}
