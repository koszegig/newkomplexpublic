<?php

namespace App\Models\Tables;
use DB;
use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;
class DMMezok extends BaseModel01Sys
{
    //
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_rendszer_tabla_mezok_2 (null)';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

    public static function getMezoLeirasok($p_table_name='')
    {
        //$resultDatas = DB::select("SELECT mezoneve as dataField, displaylabel as caption FROM public.get_mezolista_postgres_1('{$p_table_name}') ");
        $resultDatas = DB::select("SELECT * FROM public.get_json_getfields('{$p_table_name}') ")[0];
	//$resultDatas=DMMezok::objecttoArray($resultDatas);
	return $resultDatas;
    }
}
