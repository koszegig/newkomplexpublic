<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;
class WebTasks extends BaseModel13webshops  {
    public function initialTable(){
        $this->table =  '13_webshops.webtasks';
    }
    public function setFields(){
        $this->fields = collect([
            new Field("wbtsks_id", "a_id", true, false,null),
            new Field("wbtsks_tasktype", "a_vc_254", true, false,null),
            new Field("wbtsks_start", "a_datum_ido", true, false,null),
            new Field("wbtsks_lasttick", "a_datum_ido", true, false,null),
            new Field("wbtsks_stop", "a_datum_ido", true, false,null),
            new Field("wbtsks_pid", "a_integer_40", true, false,null),
            new Field("wbtsks_statuscode", "a_integer_40", true, false,null),
            new Field("wbtsks_stopme", "a_aktiv_boolean", true, false,null),
            new Field("wbtsks_limit", "a_integer_40", true, false,null),
            new Field("wbtsks_letre_felh_nev", "a_kodtipus_c10", true, false,null),
            new Field("wbtsks_letre_dat", "a_datum_ido", true, false,null),
            new Field("wbtsks_uuid", "a_uuid_id", true, false,null),
            new Field("wbtsks_cron", "a_vc_254", true, false,null),
            new Field("wbtsks_kasszazaraskor", "a_aktiv_boolean", true, false,null),
            new Field("wbtsks_indulhat", "a_aktiv_boolean", true, false,null),
        ]);
    }
    /**
     * @param $taskType
     * @return mixed
     */
    public static function start($taskType){
        $record = [
            'wbtsks_tasktype' => $taskType,
            'wbtsks_start' => date('Y-m-d H:i:s'),
            'wbtsks_statuscode' => 0,
            'wbtsks_pid'			=> getmypid(),
            'wbtsks_stopme'	=> 0
        ];
        //$task = Task::where("task_type", $taskType)->first();
        //dd($task);
        return  Self::UpdateOrCreate(['wbtsks_tasktype' => $taskType],$record);
    }
    /**
     * @param     $taskType
     * @param int $new
     * @param int $refresh
     * @return mixed
     */
    public static function stop($taskType, $new = 0, $refresh =0, $statusCode = 400 ){
        if ($new<0) {
            $new =0;
        }
        $record = [
            'wbtsks_tasktype' => $taskType,
            'wbtsks_stop' => date('Y-m-d H:i:s'),
            'wbtsks_statuscode' => 500,
            'wbtsks_stopme'	=> 0
        ];


        return  Self::UpdateOrCreate(['wbtsks_tasktype' => $taskType],$record);
    }
    /**
     * @param string $taskType
     * @param integer $statusCode
     * @return string (numeric) | integer
     */
    public static function updateLastTicket($taskType, $statusCode=0){
        $record = [
            'wbtsks_lasttick'	=> date('Y-m-d H:i:s')
            , 'wbtsks_statuscode'	=> $statusCode
            , 'wbtsks_pid'			=> getmypid()
        ];
        return  Self::UpdateOrCreate(['wbtsks_tasktype' => $taskType],$record);
    }

}
