<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class DMmenuparameterek extends BaseModel01Sys
{
	public function initialTable(){
		$this->table =   '01_sys.menuparameterek';
	  }
	 //   protected $MASTERFN ='get_menu_web_1 (?)';
	 //   public $alias = 'menu';
	//    public $table = '01_sys.menu menu';
	//    public $MASTERCOMPOSITTYPE = 't_menu_2';


		  public function setFields()
		  {
			return [
				new Field("menuprm_id", "a_id", true, false,null),
				new Field("menuprm_menu_id", "a_id_mut", true, false,null),
				new Field("menuprm_name", "a_vc_20", true, false,null),
				new Field("menuprm_value", "a_vc_120", true, false,null),
				new Field("menuprm_letre_felh_nev", "a_kodtipus_c10", true, false,null),
				new Field("menuprm_letre_dat", "a_datum_ido", true, false,null),
			  ];

		  }

	const
	   MASTERSQL='select
					menuprm_name,
					menuprm_value
				  from
				  "01_sys".menuparameterek
					where 1=1';
    //
	//$this->shemaname='01_sys';
	protected $connection = 'pgsql';
	public $alias = 'me';
	protected $table = '01_sys.menuparameterek';
	//public $schema = '03_cik';
	protected $primaryKey = 'mmenuprm_id';
	 /**
     * The table associated with the model.
     *
     * @var string
     */

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	//public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

//	 protected $fillable =  ['name'];
}
