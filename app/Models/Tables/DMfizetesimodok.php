<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;

class DMfizetesimodok extends BaseModel02Com
{

  public function initialTable(){
    $this->table =  '02_com.fizetesimod';
  }
  public function setFields(){
    $this->fields = [
          new Field("fztm_id", "a_id", true, false,null),
          new Field("fztm_megnevezes", "a_vc_50_nev_hosszu", true, false,null),
          new Field("fztm_tipus_kod", "a_kodok_c1", true, false,null),
          new Field("fztm_fizetesinapok", "a_integer_2", true, false,null),
          new Field("fztm_felh_nev", "a_kodtipus_c10", true, false,null),
          new Field("fztm_letre_dat", "a_datum_ido", true, false,null),
          new Field("fztm_kassza", "a_aktiv_boolean", true, false,null),
          new Field("fztm_kassza_sorrend", "a_integer_2", true, false,null),
          new Field("fztm_alap", "a_aktiv_boolean", true, false,null),
          new Field("fztm_kerekites", "a_aktiv_boolean", true, false,null),
          new Field("fztm_szamlan_a_megnevezes", "a_vc_30_nev_rovid", true, false,null),
          new Field("fztm_nyomtatasipeldanyszam", "a_integer_2", true, false,null),
          new Field("fztm_azonalifizetes", "a_aktiv_boolean", true, false,null),
          new Field("fztm_vegosszegkerekitesmerteke", "a_integer_2", true, false,null),
          new Field("fztm_paymentmethod_kod", "a_kodok_c1", true, false,null),
          new Field("fztm_uuid", "a_uuid_id", true, false,null),
        ];
  }



 public static function get_bizf_kif_datuma($bizf_fztm_id, $bizf_telj_datum, $partnerid)
    {
        $sql = 'select get_bizf_kif_datuma::public.a_datum from "04_rak".get_bizf_kif_datuma('.$bizf_fztm_id."::public.a_id,'".$bizf_telj_datum."'::public.a_datum_ido,".$partnerid."::public.a_id)";
        $result = \DB::select($sql);
        return $result;
    }
 public static function get_azonali_fizetes($fztm_id)
    {
        $sql = 'select fztm_azonalifizetes from "02_com".fizetesimod where fztm_id='.$fztm_id;
        $result = \DB::select($sql);
        return $result;
    }

}
