<?php

namespace App\Models\Tables;

use App\Libraries\Field;
use App\Models\Base\BaseModel01Sys;

class DMMennyisegiegyseg extends BaseModel01Sys
{
  /*  const

       MASTERSQL='select

       from
       "03_cik".get_mennyisegiegysegek_1() as me';
    //
    //$this->shemaname='01_sys';
     protected $connection = 'pgsql';
    public $alias = 'me';
    protected $table = '01_sys.felh_kodok';
    protected $primaryKey = 'me_id';

*/

    public function initialTable(){
        $this->table =  '01_sys.felh_kodok';
      }
    public function setFields(){
        $this->fields = [
            new Field("fkod_id", "a_id", true, false,null),
            new Field("fkod_fkodf_id", "a_id_mut", true, false,null),
            new Field("fkod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("fkod_kod", "a_c5", true, false,null),
            new Field("fkod_letre_felh_nev", "a_kodtipus_c10", true, false,null),
            new Field("fkod_letre_dat", "a_datum_ido", true, false,null),
            new Field("fkod_aktiv", "a_aktiv_boolean", true, false,null),
            new Field("fkod_default", "a_aktiv_boolean", true, false,null),
            new Field("fkod_megjegyzes", "a_blob", true, false,null),
            new Field("fkod_second_kod", "a_kodok_c1", true, false,null),
            new Field("fkod_path", "a_vc_800", true, false,null),
            new Field("fkod_netto_osszeg", "a_n_12_4", true, false,null),
            new Field("fkod_hkod", "a_vc_20", true, false,null),
            new Field("fkod_hkod_1", "a_vc_200", true, false,null),
            new Field("fkod_mut_id", "a_id_mut", true, false,null),
        ];
      }

}
