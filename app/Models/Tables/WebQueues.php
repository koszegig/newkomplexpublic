<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;

class WebQueues extends BaseModel13webshops  {
    public function initialTable(){
        $this->table =  '13_webshops.webqueues';
    }
    public function setFields(){
        $this->fields = collect([
            new Field("wbque_id", "a_id", true, false,null),
            new Field("wbque_tasktype", "a_vc_254", true, false,null),
            new Field("wbque_queue", "a_vc_254", true, false,null),
            new Field("wbque_ready", "a_aktiv_boolean", true, false,null),
            new Field("wbque_jsonmessage", "a_json", true, false,null),
            new Field("wbque_xmlmessage", "a_xml", true, false,null),
            new Field("wbque_letre_felh_nev", "a_kodtipus_c10", true, false,null),
            new Field("wbque_letre_dat", "a_datum_ido", true, false,null),
            new Field("wbque_uuid", "a_uuid_id", true, false,null),
        ]);
    }
}
