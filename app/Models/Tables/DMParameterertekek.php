<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class DMParameterertekek extends BaseModel01Sys  {

    public function initialTable(){
      $this->table =   '01_sys.menu parameter_ertekek';
    }

      public function setFields()
      {
        return [
          new Field("parert_id", "a_id", true, false),
          new Field("parert_par_id", "a_id_mut", true, false),
          new Field("parert_nev", "a_vc_250", true, false),
          new Field("parert_ertek", "a_vc_254", true, false),
          new Field("parert_letre_felh_nev", "a_kodtipus_c10", true, false),
          new Field("parert_letre_dat", "a_datum_ido", true, false),
        ];
      }
}
