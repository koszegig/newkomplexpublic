<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel05mnk;
use App\Libraries\Field;
class DMmunkalapfejek extends BaseModel05mnk  {
    public function initialTable(){
        $this->table =  '05_mnk.munkalap_fejek';
    }
      public function setFields(){
          $this->fields = collect([
              new Field("mulapf_id", "a_id", true, false,null),
              new Field("mulapf_mnklpazon_id", "a_id_mut", true, false,null),
              new Field("mulapf_munhely_id", "a_id_mut", true, false,null),
              new Field("mulapf_munhely_megnevezes", "a_vc_254", true, false,null),
              new Field("mulapf_munhely_cim", "a_vc_254", true, false,null),
              new Field("mulapf_munkaszam", "a_integer_4", true, false,null),
              new Field("mulapf_rogzito_felh_nev", "a_kodtipus_c10", true, false,null),
              new Field("mulapf_rogz_dat", "a_datum_ido", true, false,null),
              new Field("mulapf_letre_felh_nev", "a_kodtipus_c10", true, false,null),
              new Field("mulapf_letre_dat", "a_datum_ido", true, false,null),
              new Field("mulapf_feldolg_status_kod", "a_kodok_c1", true, false,null),
              new Field("mulapf_kezdes", "a_datum_ido", true, false,null),
              new Field("mulapf_befejezes", "a_datum_ido", true, false,null),
              new Field("mulapf_megjegyzes", "a_blob", true, false,null),
              new Field("mulapf_feldolg_status_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
              new Field("mulapf_megnevezese", "a_vc_200", true, false,null),
              new Field("mulapf_munhely_prt_id", "a_id_mut", true, false,null),
              new Field("mulapf_munhely_prt_nev", "a_vc_100_nev_partner", true, false,null),
              new Field("mulapf_azon", "a_vc_30_nev_rovid", true, false,null),
              new Field("mulapf_karbegy_id", "a_id_mut", true, false,null),
              new Field("mulapf_karbegy_nev", "a_vc_200", true, false,null),
              new Field("mulapf_csatolt_munkalap", "a_vc_254", true, false,null),
              new Field("mulapf_fajta_id", "a_id_mut", true, false,null),
              new Field("mnkfjt_nev", "a_vc_50_nev_hosszu", true, false,null),
              new Field("mulapf_limit", "a_n_12_4", true, false,null),
              new Field("mulapftip_kod", "a_kodok_c1", true, false,null),
              new Field("mulapftip_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
              new Field("mulapf_rakt_id", "a_id_mut", true, false,null),
              new Field("mulapf_rakt_nev", "a_vc_50_nev_partner", true, false,null),
                  ]);
      }
}
