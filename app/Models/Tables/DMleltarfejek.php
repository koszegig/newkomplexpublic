<?php

namespace App\Models\Tables;

use App\basemodel;

class DMleltarfejek extends basemodel
{
    	const

	   MASTERTOTALCOUNTSQL='select count(leltf.leltf_id) as  totalcount
	   				  from
				  "09_leltar".get_leltar_fejek_1 () leltf
            where
             1=1',
	   MASTERSQL='select
				-- DISTINCT on(leltf.leltf_id)
				  1 as ssz,
				  count(leltf.leltf_id) over() as totalcount,
              leltf.leltf_id,
              leltf.leltf_ertekelesi_sorszam,
              leltf.leltf_ertekelesi_kod,
              leltf.leltf_ertekelesi_kod_nev,
              leltf.leltf_datuma,
              leltf.leltf_rakt_id,
              leltf.leltf_rakt_nev,
              leltf.leltf_felvetel_eszkoz_kod,
              leltf.leltf_felvetel_eszkoz_kod_nev,
              leltf.leltf_leltar_felveteli_adatkor_kod,
              leltf.leltf_lelttar_felveteli_adatkor_kod_nev,
              leltf.leltf_felvetel_modja_kod,
              leltf.leltf_felvetel_modja_kod_nev,
              leltf.leltf_teljes_kod,
              leltf.leltf_teljes_kod_nev,
              leltf.leltf_egyes_cikcsop_id,
              leltf.leltf_egyes_cikcsop_nev,
              leltf.leltf_kettes_cikcsop_id,
              leltf.leltf_kettes_cikcsop_nev,
              leltf.leltf_cik_szuro,
              leltf.leltf_status_kod,
              leltf.leltf_status_kod_nev,
              leltf.leltf_letre_felh_nev,
              leltf.leltf_letre_dat,
              leltf_lezaras_idopontja,
              leltf_afa_id,
              leltf_afa_szazalek,
              leltf_netto,
              leltf_brutto,
              leltf_netto_hiany,
              leltf_brutto_hiany,
              leltf_netto_tobblet,
              leltf_brutto_tobblet,
              ---eltf_megjegyzes,
              leltf_netto_konyszerinti_eladas,
              leltf_brutto_konyszerinti_eladas,
              leltf_netto_konyszerinti_atlagbeszerzes,
              leltf_brutto_konyszerinti_atlagbeszerzes,
              leltf_netto_konyszerinti_beszerzes,
              leltf_brutto_konyszerinti_beszerzes,
              leltf_raktkeszlet_tipusa,
              leltf_raktkeszlet_tipusa_kod_nev

            from
             "09_leltar".get_leltar_fejek_1 () leltf
            where
             1=1
            ',
	   DETAILSQL='Select
                      leltk.leltk_id,
                      leltk.leltk_leltf_id,
                      leltk.leltk_leltf_id as leltf_id,
                      leltk.leltk_ertekelesi_sorszam,
                      leltk.leltk_cik_id,
                      leltk.leltk_cik_kod,
                      leltk.leltk_cik_nev,
                      leltk.leltk_vtsz_szam,
                      leltk.leltk_afak_kod,
                      leltk.leltk_afa_nev,
                      leltk.leltk_me_id,
                      leltk.leltk_me_rovidites,
                      leltk.leltk_me_nev,
                      leltk.leltk_konyv_keszl,
                      leltk.leltk_ivek,
                      leltk.leltk_felvett_menny,
                      leltk.leltk_egyseg_ar,
                      leltk.leltk_beszerzesi_ar,
                      leltk.leltk_afa_szorzo,
                      leltk.leltk_arfolyam,
                      leltk.leltk_alap_ar,
                      leltk.leltk_tobblet_menny,
                      leltk.leltk_hiany_menny,
                      leltk.leltk_elteres_menny,
                      leltk.leltk_felvett_elad_ertek,
                      leltk.leltk_tobblet_elad_ertek,
                      leltk.leltk_hiany_elad_ertek,
                      leltk.leltk_elteres_elad_ertek,
                      leltk.leltk_felvett_beszerzesi_ertek,
                      leltk.leltk_tobblet_beszerzesi_ertek,
                      leltk.leltk_hiany_beszerzesi_ertek,
                      leltk.leltk_elteres_beszerzesi_ertek,
                      leltk.leltk_felvett_alap_ertek,
                      leltk.leltk_tobblet_alap_ertek,
                      leltk.leltk_hiany_alap_ertek,
                      leltk.leltk_elteres_alap_ertek,
                      leltk.leltk_letre_felh_nev,
                      leltk.leltk_letre_dat,
                      leltk.leltk_leltf_status_kod,
                      leltk.leltk_elteres_menny_szazalek,
                      leltk.leltk_elteres_elad_ertek_szazalek,
                      leltk.leltk_tobblet_menny_szazalek,
                      leltk.leltk_hiany_menny_szazalek,
                      leltk.leltk_elteres_beszerzesi_ertek_szazalek,
                      leltk.leltk_leltf_datuma,
                      leltk.leltk_leltf_rakt_id,
                      leltk.leltk_leltf_rakt_nev

                    from
                     "09_leltar".get_leltar_kiertekelesei_1 (
                       ';
    //
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_biz_fejek_1 ()';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];

}
