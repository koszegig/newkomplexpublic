<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;
use Auth;
use View;
class DMVonalkodok extends BaseModel
{
    const
       MASTERTOTALCOUNTSQL='select count(vonal.vonal_id) as  totalcount
                    from
                    "03_cik".get_vonalkodok_1 (null) as vonal
                  where 1=1 ',

       MASTERSQL='
                select
                  1 as ssz,
                  count(vonal.vonal_id) over() as  totalcount,
                  vonal.vonal_id,
                  vonal.vonal_cik_id,
                  vonal.vonal_cik_id as id,
                  vonal.vonal_azon as name,
                  vonal.vonal_azon,
                  vonal.vonal_meny_id,
                  vonal.vonal_me_rovidites,
                  vonal.vonal_me_nev,
                  vonal.vonal_armeny_szorzo,
                  vonal.vonal_tipus_kod,
                  vonal.vonal_tipus_kod_nev,
                  vonal.vonal_letre_felh_nev,
                  vonal.vonal_letre_dat,
                  vonal.vonal_default
                  from
                  "03_cik".get_vonalkodok_1 (null) as vonal

       ';
    //
    //$this->shemaname='01_sys';
     protected $connection = 'pgsql';
     /**
     * The table associated with the model.
     *
     * @var string
     */
       public $alias = 'vonal';
    public $table = 'vonalkodok';
    public $schema = '03_cik';
    protected $primaryKey = 'vonal_id';




    public static function quRaktarak($param = '')
    {
        $sql = ' select
                  rakt_id,
                  rakt_prt_id,
                  rakt_prt_nev,
                  rakt_nev,
                  rakt_letre_felh_nev,
                  rakt_letre_dat,
                  rakt_aktiv,
                  rakt_jovedeki_termek,
                  rakt_jovedeki_termek_kod_nev
                  from
                  "02_com".get_raktarak_1 ()
                    where
                    rakt_aktiv = true '.$param;
         ///$resultDatas = DMRaktarak::objecttoArray(DB::select($sql));
        //return $resultDatas;
    }



}
