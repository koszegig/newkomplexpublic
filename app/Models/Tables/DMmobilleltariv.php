<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;
use Auth;
use View;
class DMmobilleltariv extends basemodel
{
    const
       MASTERTOTALCOUNTSQL='select count(livfpda_id) as  totalcount
                  from
                  "06_imprt".get_leltarivosszeszodo_1 ( ',

       MASTERSQL='
                select
                  livfpda_id,
                  livfpda_livf_id,
                  livfpda_rtmbl_id,
                  livfpda_rtmbl_device_id,
                  livfpda_rtmbl_nev,
                  livfpda_cik_id,
                  livfpda_cik_vonal_azon,
                  livfpda_cik_kod,
                  livfpda_cik_nev,
                  livfpda_cik_meny,
                  livfpda_cik_meny_szorzo,
                  --livfpda_cik_megj,
                  livfpda_status_kod,
                  livfpda_letre_felh_nev,
                  livfpda_letre_dat
                 From
                    "06_imprt".get_leltarivosszeszodo_1 (';
    //
    //$this->shemaname='01_sys';
     protected $connection = 'pgsql';
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '"06_imprt".get_leltarivosszeszodo_1()';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */

     protected $fillable =  ['name'];

    public static function quaktualiskasza()
    {
        $userID=DMKaszak::getCurrentUserID();
        $sql = 'select
                    kszk.*
                from
                 "02_com".get_kaszak_1 ()  as kszk
                 where

                 kszk.kszk_id = (Select
                                    felh_kszk_id
                                from
                                    "01_sys".felhasznalok
                                    where
                                      felh_web_users_id=:userID )';
         $resultDatas = DMfizetesimodok::objecttoArray(DB::select($sql, ['userID' =>Auth::id()]));
        return $resultDatas[0];
    }



}
