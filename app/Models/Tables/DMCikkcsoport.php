<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Model;

class DMCikkcsoport extends BaseModel03Cik  {

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [];
    const
       MASTERTOTALCOUNTSQL='Select select count(cikcsop_id) as  totalcount
       from
       "03_cik".get_cikkekcsoportok_1() ',

       MASTERSQL='Select
            cikcsop.cikcsop_id,
            cikcsop.cikcsop_szulo_id,
            cikcsop.cikcsop_nev,
            cikcsop.cikcsop_rendezo,
            cikcsop.cikcsop_image_index,
            cikcsop.cikcsop_letre_felh_nev,
            cikcsop.cikcsop_letre_dat
         from
          "03_cik".get_cikkekcsoportok_1() as cikcsop
          where
            1=1
       ',
              EDITSELECTSQL='
                            select


';

    //
    /**
     * The database table Alias.
     *
     * @var string
     */

    public $alias = 'cikcsop';
    /**
     * The database table used by the model.
     *
     * @var string
     */

    public $table = 'cikk_csoportok';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;


}
