<?php

namespace App\Models\Tables;

use DB;
use Log;
use View;
use App\Models\Base\BaseModel04Rak;
use App\Libraries\Field;

class DMbizonylatfejmozgasnem extends BaseModel04Rak
{

    protected function setStoredProcedure(){
        $this->storedProcedure = '  "04_rak".get_bizonylatfejmozgasnem_1()';
    }
    public function setFields(){
        $this->fields = [
            new Field("bizfmgsn_id", "a_id", true, true,null),
            new Field("bizfmgsn_rovidites", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizfmgsn_nev", "a_kodok_c1", true, false,null),
            new Field("bizfmgsn_aktiv", "a_kodok_c1", true, false,null),

        ];
    }

}
