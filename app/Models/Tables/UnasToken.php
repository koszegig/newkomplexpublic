<?php


namespace App\Models\Tables;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;

class UnasToken extends BaseModel13webshops  {
    public function initialTable(){
        $this->table =  '13_webshops.unastoken';
    }
    public function setFields(){
        $this->fields = collect([
            new Field("unstkn_id", "a_id", true, false,null),
            new Field("unstkn_uuid", "a_uuid_id", true, false,null),
            new Field("unstkn_token", "a_vc_800", true, false,null),
            new Field("unstkn_until", "a_datum_ido", true, false,null),
            new Field("unstkn_permissions", "a_vc_8000", true, false,null),
            new Field("unstkn_api", "a_vc_800", true, false,null),
            new Field("unstkn_letre_felh_nev", "a_kodtipus_c10", true, false,null),
            new Field("unstkn_letre_dat", "a_datum_ido", true, false,null),
        ]);
    }
}
