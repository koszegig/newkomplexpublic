<?php

namespace App\Models\Tables;
use DB;
use View;
use Illuminate\Database\Eloquent\Model;
namespace App\Models\Tables;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class DMCommon extends BaseModel01Sys
{


    const
        DATEFORMAT="Y.m.d",
        DATELABELFORMAT="yyyy mm dd",
        DATETIMEFORMAT="Y.m.d H:i:s",
        DATELABELTIMEFORMAT="yyyy mm dd - HH:ii pp",
        COMMONMENNYISEGIEGYSEGEK ='select
				me_id,
				me_rovidites,
				me_nev,
				me_letre_felh_nev,
				me_letre_dat,
				me_aktiv
			   from
				"03_cik".get_mennyisegiegysegek_1()
				order by me_rovidites',
        COMMONVTSZSZAMOK ='select
				vtsz_id,
				vtsz_szam,
				vtsz_nev,
				vtsz_szj_kod,
				vtsz_szavidos
			   from
				"03_cik".get_vtsz_1()
				order by vtsz_szam';

	public static function get_aktualisarfolyam($pnzn_id=0,$p_datum)
    {

//		DMCommon::initialshema('02_com');
        $resultDatas = DB::select("select
                                    pnza_arfolyam
                                    from
                                    ".'"02_com"'.".get_arfolyam_2('".$p_datum."'::public.a_datum_ido) where pnza_pnzn_id=:pnzn_id",
                                  ['pnzn_id' => $pnzn_id ]);
  //          return isset(DMCommon::objecttoArray($resultDatas)[0]['pnza_arfolyam'])  ? DMCommon::objecttoArray($resultDatas)[0]['pnza_arfolyam'] : 1;
    }

	public static function getqu_igen_nem_kod($id=0,$mezonev='',$displaylabel='')
    {
		return self::get_rendszer_kodok($id,'igen_nem',$mezonev,$displaylabel);

    }
	public static function GetSQLServerDate()
    {
		$resultDatas = DB::select("SELECT CURRENT_DATE AS DATUM");
        $serverDate = $resultDatas[0]->datum;
        return date(DMCommon::DATEFORMAT, strtotime($serverDate));
    }
	public static function GetSQLServerDateTime($mode = 'rawArray')
    {
		//return DB::select("SELECT now() AS DATUM");
		$resultDatas = DB::select("SELECT now() AS DATUM");

         $serverDate = $resultDatas[0]->datum;
        return date(DMCommon::DATETIMEFORMAT, strtotime($serverDateTime));

    }
    public static function getAlapertekek(){
        $resultDatas=DB::select('select trim(par_megnevezes) as par_megnevezes, par_ertek from  "01_sys".parameterek ');
        return $resultDatas;
        //print_r($resultDatas);
		//return basemodel::objecttoArray($resultDatas);
	}
    public static function AlapertekParameterbol($Parazon){
        $resultDatas=DB::select('select par_ertek from  "01_sys".parameterek where trim(par_megnevezes) = trim('."'".$Parazon."')");
        return $resultDatas[0]->par_ertek;
		//return basemodel::objecttoArray($resultDatas)[0]['par_ertek'];
	}
	public static function get_rendszer_kodok($kod_mezo)
    {

		$resultDatas = \DB::select('select
 				kod_kod as id,
				kod_nev as name
			   from
				"01_sys".get_rendszer_kodok_1(null)
				where kod_mezo = '."'".$kod_mezo."'");
         return $resultDatas;

    }
   	public static function getqubizonylatfejmozgasnem($id=0,$mezonev='',$displaylabel='',$mode = 'rawArray')
    {

		DMCommon::initialshema('04_rak');
		$resultDatas = DB::select('select
                            bizfmgsn_id,
                            bizfmgsn_rovidites,
                            bizfmgsn_nev

                            from
                            "04_rak".get_bizonylatfejmozgasnem_1()
                        where
                           bizfmgsn_aktiv=true
			  ');

		switch($mode){
			case "json" : return json_encode($resultDatas, true);
			case "rawArray" : return DMCommon::objecttoArray($resultDatas);
			case "html" : return View::make('component.dblookupbizonylatfejmozgasnem')
					->with('mezonev',$mezonev)
					->with('displaylabel',$displaylabel)
					->with('select_id',$id)
				    ->with('bizonylatfejmozgasnemek',DMCommon::objecttoArray($resultDatas));

		}
    }
}
