<?php

namespace App\Models\Tables;
use DB;
use Illuminate\Database\Eloquent\Model;

class DMReport extends basemodel
{
    //
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '"01_sys".get_reports_1 ()';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];
    public static function Findreport($ClassName='')
    {

		DMReport::initialshema('01_sys');
		$resultDatas = DB::select(" Select
              rpt_id ,
              rpt_classname,
              rpt_title ,
              rpt_sql::public.a_vc_8000 as rpt_sql,
              rpt_design::public.a_vc_8000 as rpt_design,
              rpt_blokk_nyomtatora,
              rpt_letre_felh_nev ,
              rpt_letre_dat,
              rpt_vonalkod_nyomtatora,
              rpt_printmode,
              rpt_printonsheet,
              rpt_copies,
              rpt_colllate,
              rpt_szamla_nyomtatora,
              rpt_szallito_nyomtatora,
              rpt_rtny_id,
              rpt_rtny_nyomtatomegnevezese,
              rpt_rtny_nyomtatohalozatineve,
              rpt_rtny_rnytps_id,
              rpt_rtny_rnytps_megnevezes,
              rpt_rtny_rnytps_meret_kod,
              rpt_rtny_rnytps_meret_kod_nev,
              rpt_html
			   from
				".'"'.'01_sys'.'"'.".get_reports_1 ()
			   where
			upper(trim(rpt_classname))='upper(trim(".$ClassName."))'
			");
		return DMReport::objecttoArray($resultDatas);
    }
}
