<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel03Cik;
use App\Libraries\Field;

class DMAfak extends BaseModel03Cik
{
  public function initialTable(){
    $this->table =  '03_cik.afak';
  }
  public function setFields(){
    $this->fields = [
      new Field("afa_id", "a_id", true, true,null),
      new Field("afa_kod", "a_c3", true, false,null),
      new Field("afa_nev", "a_vc_30_nev_rovid", true, false,null),
      new Field("afa_szazalek", "a_n_5_3", true, false,null),
      new Field("afa_szorzo", "a_n_5_3", true, false,null),
      new Field("afa_aktiv", "a_aktiv_boolean", true, false,null),
      new Field("afa_letre_felh_nev", "a_kodtipus_c10", true, false,null),
      new Field("afa_letre_dat", "a_datum_ido", true, false,null),
      new Field("afa_kezdete", "a_datum", true, false,null),
      new Field("afa_vege", "a_datum", true, false,null),
      new Field("afa_forditottafa", "a_kodok_c1", true, false,null),
      new Field("afa_biztip_kod", "a_kodok_c1", true, false,null),
      new Field("afa_mid", "a_id_mut", true, false,null),
      new Field("afa_mplu", "a_vc_20", true, false,null),
      new Field("afa_mgyujto", "a_integer_2", true, false,null),
      new Field("afa_mpluszam", "a_integer_2", true, false,null),
      new Field("afa_mbillid", "a_id_mut", true, false,null),
      new Field("afa_mvonalkod", "a_vc_15", true, false,null),
      new Field("afa_kszlaket_afakod", "a_integer_2", true, false,null),
      new Field("afa_uuid", "a_uuid_id", true, false,null),
    ];
  }


}
