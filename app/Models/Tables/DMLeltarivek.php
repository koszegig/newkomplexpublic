<?php

namespace App\Models\Tables;

use App\basemodel;

class DMLeltarivek extends basemodel
{
    	const

	   MASTERTOTALCOUNTSQL='select count(livf.livf_id) as  totalcount
	   				  from
				  "09_leltar".get_leltariv_fejek_1 (
                        null
                        ) livf
                where
                1=1',
	   MASTERSQL='select
				-- DISTINCT on(livf.livf_id)
				  1 as ssz,
				  count(livf.livf_id) over() as totalcount,
                  livf.livf_id,
                  livf.livf_leltf_id,
                  livf.livf_leltf_datuma,
                  livf.livf_leltf_rakt_id,
                  livf.livf_leltf_rakt_nev,
                  livf.livf_iker_sorszam,
                  livf.livf_ertekelesi_sorszam,
                  livf.livf_livszam,
                  livf.livf_prt_id,
                  livf.livf_prt_nev,
                  livf.livf_tetel_szam,
                  livf.livf_leltf_status_kod,
                  livf.livf_leltf_status_kod_nev,
                  livf.livf_letre_felh_nev,
                  livf.livf_letre_dat,
                  livf.livf_leltf_ertekelesi_kod,
                  livf.livf_leltf_ertekelesi_kod_nev,
                  livf.livf_leltf_felvetel_eszkoz_kod,
                  livf.livf_leltf_felvetel_eszkoz_kod_nev,
                  livf.livf_rtmbl_id,
                  livf.livf_rtmbl_device_id,
                  livf.livf_rtmbl_nev,
                  livf.livf_statuskod,
                  livf.livf_statuskod_kod_nev
                from
                 "09_leltar".get_leltariv_fejek_1 (
                    null
                   ) livf
                 where
                  1=1
            ',
	   DETAILSQL='select
                  livt.livt_id,
                  livt.livt_livf_id,
                  livt.livt_livf_id as livf_id,
                  livt.livt_livf_leltf_id,
                  livt.livt_livf_iker_sorszam,
                  livt.livt_livf_ertekelesi_sorszam,
                  livt.livt_livf_livszam,
                  livt.livt_livf_csopvez_szem_prt_id,
                  livt.livt_livf_szem_prt_nev,
                  livt.livt_livf_tetel_szam,
                  livt.livt_vonal_azon,
                  livt.livt_cik_id,
                  livt.livt_cik_kod,
                  livt.livt_cik_nev_rovid,
                  livt.livt_cik_nev,
                  livt.livt_me_szorzo,
                  livt.livt_menny,
                  livt.livt_nyilv_menny,
                  livt.livt_jeloles_kod,
                  livt.livt_jeloles_kodnev,
                  --livt.livt_megj,
                  livt.livt_tet_sorszam,
                  livt.livt_szav_ido,
                  livt.livt_letre_felh_nev,
                  livt.livt_letre_dat,
                  livt.livt_livf_leltf_rakt_id,
                  livt.livt_livf_leltf_rakt_nev,
                  livt.livt_livf_leltf_datuma,
                  livt.livt_cik_afa_id,
                  livt.livt_cik_afa_nev,
                  livt.livt_cik_vtszafa_vtsz_szam,
                  livt.livt_cik_me_id,
                  livt.livt_cik_me_rovidites,
                  livt.livt_cik_me_nev,
                  livt.livt_cik_armeny_me_id,
                  livt.livt_cik_armeny_me_rovidites,
                  livt.livt_cik_armeny_me_nev,
                  livt.livt_elteres_menny
                from
                 "09_leltar".get_leltariv_tetelei_1 (';
    //
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_leltariv_fejek_1 ()';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];

}
