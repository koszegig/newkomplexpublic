<?php

namespace App\Models\Tables;

class DMmp_rakt_keszletek extends BaseModel04Rak
{
  protected $MASTERFN ='get_mp_rakt_keszletek_1 ()';
  public $alias = 'mpraktkeszl';
  public $table = 'mp_rakt_keszletek';
  public $MASTERCOMPOSITTYPE = 't_mp_rakt_keszletek_1';
  protected $fields = [];


    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
      parent::__construct($attributes);
    }

    public function __getFields()
    {
      return [
        new Field(false, 'count(mpraktkeszl_id) over()', 'totalcount', false, ['master'], false),
        new Field(true,'mpraktkeszl_id','mpraktkeszl_id', false, ['master'], true),
        new Field(true,'mpraktkeszl_cik_id','mpraktkeszl_cik_id', false, ['master'], false),
        new Field(true,'mpraktkeszl_cik_kod','mpraktkeszl_cik_kod', false, ['master'], false),
        new Field(true,'mpraktkeszl_cik_nev','mpraktkeszl_cik_nev', false, ['master'], false),
        new Field(true,'mpraktkeszl_rakt_id','mpraktkeszl_rakt_id', false, ['master'], false),
        new Field(true,'mpraktkeszl_rakt_nev','mpraktkeszl_rakt_nev', false, ['master'], false),
        new Field(true,'mpraktkeszl_akt_keszl','mpraktkeszl_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_fogl_keszl','mpraktkeszl_fogl_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_min_keszl','mpraktkeszl_min_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_max_keszl','mpraktkeszl_max_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_nyilv_ar','mpraktkeszl_nyilv_ar', false, ['master'], false),
        new Field(true,'mpraktkeszl_ut_besz_dat','mpraktkeszl_ut_besz_dat', false, ['master'], false),
        new Field(true,'mpraktkeszl_ut_besz_ar','mpraktkeszl_ut_besz_ar', false, ['master'], false),
        new Field(true,'mpraktkeszl_ut_elad_dat','mpraktkeszl_ut_elad_dat', false, ['master'], false),
        new Field(true,'mpraktkeszl_ut_elad_ar','mpraktkeszl_ut_elad_ar', false, ['master'], false),
        new Field(true,'mpraktkeszl_letre_felh_nev','mpraktkeszl_letre_felh_nev', false, ['master'], false),
        new Field(true,'mpraktkeszl_letre_dat','mpraktkeszl_letre_dat', false, ['master'], false),
        new Field(true,'mpraktkeszl_szabad_keszl','mpraktkeszl_szabad_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_kolcsonzott_keszlet','mpraktkeszl_kolcsonzott_keszlet', false, ['master'], false),
        new Field(true,'mpraktkeszl_brutto_nyilv_ar','mpraktkeszl_brutto_nyilv_ar', false, ['master'], false),
        new Field(true,'mpraktkeszl_netto_keszlet_ertek','mpraktkeszl_netto_keszlet_ertek', false, ['master'], false),
        new Field(true,'mpraktkeszl_brutto_keszlet_ertek','mpraktkeszl_brutto_keszlet_ertek', false, ['master'], false),
        new Field(true,'mpraktkeszl_vonal_azon','mpraktkeszl_vonal_azon', false, ['master'], false),
        new Field(true,'mpraktkeszl_cik_aktiv','mpraktkeszl_cik_aktiv', false, ['master'], false),
        new Field(true,'mpraktkeszl_1_akt_keszl','mpraktkeszl_1_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_2_akt_keszl','mpraktkeszl_2_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_3_akt_keszl','mpraktkeszl_3_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_4_akt_keszl','mpraktkeszl_4_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_5_akt_keszl','mpraktkeszl_5_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_6_akt_keszl','mpraktkeszl_6_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_7_akt_keszl','mpraktkeszl_7_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_8_akt_keszl','mpraktkeszl_8_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_9_akt_keszl','mpraktkeszl_9_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_10_akt_keszl','mpraktkeszl_10_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_11_akt_keszl','mpraktkeszl_11_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkeszl_12_akt_keszl','mpraktkeszl_12_akt_keszl', false, ['master'], false),
        new Field(true,'mpraktkesz_cik_jovedeki_termek','mpraktkesz_cik_jovedeki_termek', false, ['master'], false),
        new Field(true,'mpraktkeszl_el_besz_ar','mpraktkeszl_el_besz_ar', false, ['master'], false),
        new Field(true,'mpraktkeszl_el_besz_dat','mpraktkeszl_el_besz_dat', false, ['master'], false),
        new Field(true,'mpraktkeszl_el_keszlet_ertek','mpraktkeszl_el_keszlet_ertek', false, ['master'], false),
        new Field(true,'mpraktkeszl_el_brutto_keszlet_ertek','mpraktkeszl_el_brutto_keszlet_ertek', false, ['master'], false),
        new Field(true,'mpraktkeszl_ut_keszlet_ertek','mpraktkeszl_ut_keszlet_ertek', false, ['master'], false),
        new Field(true,'mpraktkeszl_ut_brutto_keszlet_ertek','mpraktkeszl_ut_brutto_keszlet_ertek', false, ['master'], false),

      ];
    }
}
