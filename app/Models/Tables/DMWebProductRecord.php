<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel13webshops;
use App\Libraries\Field;


class DMWebProductRecord extends BaseModel13webshops  {
    public function initialTable(){
        $this->table =  '13_webshops.wbs_wcproduct_record';
    }
    public function setFields(){
        $this->fields = collect([
            new Field("wbswcp_id", "a_id", true, false,null),
            new Field("wbswcp_org_id", "a_id_mut", true, false,null),
            new Field("wbswcp_title", "a_vc_200", true, false,null),
            new Field("wbswcp_type", "a_vc_15", true, false,null),
            new Field("wbswcp_status", "a_vc_15", true, false,null),
            new Field("wbswcp_description", "a_vc_8000", true, false,null),
            new Field("wbswcp_short_description", "a_vc_8000", true, false,null),
            new Field("wbswcp_sku", "a_vc_15", true, false,null),
            new Field("wbswcp_letre_felh_nev", "a_kodtipus_c10", true, false,null),
            new Field("wbswcp_letre_dat", "a_datum_ido", true, false,null),
            new Field("wbswcp_price", "a_n_9_3", true, false,null),
            new Field("wbswcp_regular_price", "a_n_9_3", true, false,null),
            new Field("wbswcp_sale_price", "a_n_9_3", true, false,null),
            new Field("wbswcp_weight", "a_n_9_3", true, false,null),
            new Field("wbswcp_tax_status", "a_vc_15", true, false,null),
            new Field("wbswcp_tax_class", "a_vc_120", true, false,null),
            new Field("wbswcp_stock_quantity", "a_n_9_3", true, false,null),
            new Field("wbswcp_dimensions_length", "a_n_5_3", true, false,null),
            new Field("wbswcp_dimensions_width", "a_n_5_3", true, false,null),
            new Field("wbswcp_dimensions_height", "a_n_5_3", true, false,null),
            new Field("wbswcp_rakt_id", "a_id_mut", true, false,null),
            new Field("wbswcp_cik_id", "a_id_mut", true, false,null),
            new Field("wbswcp_modosult", "a_aktiv_boolean", true, false,null),
            new Field("wbswcp_magento_id", "a_id_mut", true, false,null),
            new Field("wbswcp_frissiteni", "a_aktiv_boolean", true, false,null),
            new Field("wbswcp_unas_id", "a_id_mut", true, false,null),
            new Field("wbswcp_unit", "a_vc_30_nev_rovid", true, false,null),
            new Field("wbswcp_minimumqty", "a_integer_20", true, false,null),
            new Field("wbswcp_maximumqty", "a_integer_20", true, false,null),
            new Field("wbswcp_wbque_id", "a_id_mut", true, false,null),
        ]);
    }
}
