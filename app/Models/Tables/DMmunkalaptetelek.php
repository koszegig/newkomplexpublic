<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel05mnk;
use App\Libraries\Field;
class DMmunkalaptetelek extends BaseModel05mnk  {
    public function initialTable(){
        $this->table =  '05_mnk.munkalap_tetelek';
    }
      public function setFields(){
          $this->fields = collect([
          new Field("mulapt_id", "a_id", true, false,null),
new Field("mulapt_mulapf_id", "a_id_mut", true, false,null),
new Field("mulapt_vrtm_id", "a_id_mut", true, false,null),
new Field("mulapt_letre_felh_nev", "a_kodtipus_c10", true, false,null),
new Field("mulapt_letre_dat", "a_datum_ido", true, false,null),
new Field("mulapt_foly_kezd", "a_datum_ido", true, false,null),
new Field("mulapt_foly_vege", "a_datum_ido", true, false,null),
new Field("mulapt_raforditott_ora", "a_n_3_2", true, false,null),
new Field("mulapt_feldolg_statusz_kod", "a_kodok_c1", true, false,null),
new Field("mulapt_szrl_id", "a_id_mut", true, false,null),
new Field("mulapt_jarmu_id", "a_id_mut", true, false,null),
new Field("mulapt_kilometerallas", "a_integer_8", true, false,null),
new Field("mulapt_tervezet_kezd", "a_datum_ido", true, false,null),
new Field("mulapt_tervezet_vege", "a_datum_ido", true, false,null),
new Field("mulapt_osszeg", "a_n_12_3", true, false,null),
new Field("mulapt_pnzn_id", "a_id_mut", true, false,null),
new Field("mulapt_elvegzettmunka", "a_blob", true, false,null),
new Field("mulapt_oradij", "a_n_12_3", true, false,null),
new Field("mulapt_csatoltdokumentum", "a_vc_254", true, false,null),
new Field("mulapt_tenyleges_osszeg", "a_n_12_3", true, false,null),
new Field("mulapt_munkalapkeziszam", "a_integer_8", true, false,null),
new Field("mulapt_dlgpnzjogc_id", "a_id_mut", true, false,null),
                  ]);
      }
}
