<?php

namespace App\Models\Tables;;

use App\basemodel;

class DMgyartasiarajanlatfejektetelek extends basemodel
{
    	const

	   MASTERTOTALCOUNTSQL='select count(gyajkf.gyajkf_id) as  totalcount
	   				  from
				  "11_gyartas".get_gyartasi_arajanlatfejek_1 (',
	   MASTERSQL='select
				-- DISTINCT on(bizf.bizf_id)
				  1 as ssz,
				  count(gyajkf.gyajkf_id) over() as totalcount,
                  gyajkf.gyajkf_id,
                  gyajkf.gyajkf_megnevezes,
                  gyajkf.gyajkf_prt_id,
                  gyajkf.gyajkf_prt_nev,
                  gyajkf.gyajkf_vsz_prt_id,
                  gyajkf.gyajkf_vsz_prt_nev,
                  gyajkf.gyajkf_ugynok_prt_id,
                  gyajkf.gyajkf_ugynok_prt_nev,
                  gyajkf.gyajkf_status_kod,
                  gyajkf.gyajkf_status_kod_nev,
                  gyajkf.gyajkf_ervenyes,
                 -- gyajkf.gyajkf_megjegyzes,
                  gyajkf.gyajkf_nettoertek,
                  gyajkf.gyajkf_bruttoertek,
                  gyajkf.gyajkf_letre_felh_nev,
                  gyajkf.gyajkf_letre_dat,
                  gyajkf.gyajkf_szam,
                  gyajkf.gyajkf_afa_id,
                  gyajkf.gyajkf_afa_kod,
                  gyajkf.gyajkf_afa_nev,
                  gyajkf.gyajkf_afa_szorzo,
                  gyajkf.gyajkf_afa_szazalek,
                  gyajkf.gyajkf_pnzn_id,
                  gyajkf.gyajkf_arfolyam,
                  gyajkf.gyajkf_handle,
                  gyajkf.gyajkf_penznem_nev,
                  gyajkf_gyrtazon_id,
                  gyajkf_azon,
                  gyajkf_terv_vege,
                  gyajkf_kifizetett_brutto_ert,
                  gyajkf_kifizetes_datuma,
                  gyajkf_arkategoria_kod,
                  gyajkf_artbl_arkategoria_kod_nev
                from
                    "11_gyartas".get_gyartasi_arajanlatfejek_1 (',
	   DETAILSQL='Select
                 gyajkt.gyajkt_id,
                 gyajkt.gyajkt_gyajkf_id,
                 gyajkt.gyajkt_gyajkf_id as gyajkf_id,
                 gyajkt.gyajkt_ttrmkf_id,
                 gyajkt.gyajkt_ttrmkf_megnevezes,
                 gyajkt.gyajkt_wall_x,
                 gyajkt.gyajkt_wall_y,
                 gyajkt.gyajkt_wall_z,
                 gyajkt.gyajkt_nytk_id,
                 gyajkt.gyajkt_nytk_megnevezes,
                 gyajkt.gyajkt_handle,
                 gyajkt.gyajkt_letre_felh_nev,
                 gyajkt.gyajkt_letre_dat,
                 gyajkt.gyajkt_wall_me_id,
                 gyajkt.gyajkt_wall_me_nev,
                 gyajkt.gyajkt_nettoertek,
                 gyajkt.gyajkt_afa_szazalek,
                 gyajkt.gyajkt_bruttoertek,
                 --gyajkt.gyajkt_megjegyzes,
                 gyajkt_mert_meret_tipus_kod,
                 gyajkt_mert_meret_tipus_kod_nev,
                 gyajkt.gyajkt_egyeb_felar,
                 gyajkt.gyajkt_gyelklk_nev,
                 gyajkt.gyajkt_gymntk_nev,
                 gyajkt.gyajkt_gysznk_nev,
                 gyajkt.gyajkt_type,
                 gyajkt.gyajkt_style,
                 gyajkt.gyajkt_border

                from
                  "11_gyartas".get_gyartasi_arajanlatteteleiuj_1 (
                       ';
    //
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_biz_fejek_1 ()';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];

}
