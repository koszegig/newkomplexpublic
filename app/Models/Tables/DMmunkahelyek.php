<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;
class DMmunkahelyek extends BaseModel02Com  {
    public function initialTable(){
        $this->table =  '02_com.munkahelyek';
    }
      public function setFields(){
          $this->fields = collect([
              new Field("munhely_id", "a_id", true, false,null),
              new Field("munhely_prt_id", "a_id_mut", true, false,null),
              new Field("munhely_prt_nev", "a_vc_100_nev_partner", true, false,null),
              new Field("munhely_megnevezes", "a_vc_254", true, false,null),
              new Field("munhely_cim", "a_vc_254", true, false,null),
              new Field("munhely_prt_cim_id", "a_id_mut", true, false,null),
              new Field("munhely_prt_teljescim", "a_vc_800", true, false,null),
              new Field("munhely_limit", "a_n_12_3", true, false,null),
              new Field("munhely_handle", "a_id_mut", true, false,null),
              new Field("munhely_letre_felh_nev", "a_kodtipus_c10", true, false,null),
              new Field("munhely_letre_dat", "a_datum_ido", true, false,null),
              new Field("munhely_prtelhtsg_id", "a_id_mut", true, false,null),
              new Field("munhely_prtelhtsg_mobilszam", "a_vc_20", true, false,null),
              new Field("munhely_default", "a_aktiv_boolean", true, false,null),
          ]);
      }
}
