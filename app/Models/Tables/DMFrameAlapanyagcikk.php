<?php

namespace App\Models\Tables;
use DB;
use View;
use Illuminate\Database\Eloquent\Model;

class DMFrameAlapanyagcikk extends basemodel
{
	 const
       MASTERTOTALCOUNTSQL='select count(cikkekalap_tmp_id) as  totalcount
                         from
                  "01_sys".get_tmpalapanyagcikktmp(%TEMPAZON%)',

      MASTERSQL='select
  			*
 		from "01_sys".get_tmpalapanyagcikktmp(%TEMPAZON%)
            where   1=1
			  ',
	_REFRESHTEMP='select * from  "01_sys".set_tablakapcsolat_temp ('."'2'".',%TEMPID%,%TEMPAZON%,%TEMPSTATUS%)';
    //:TEMPAZON:
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_tmpalapanyagcikktmp(null)';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];


}
