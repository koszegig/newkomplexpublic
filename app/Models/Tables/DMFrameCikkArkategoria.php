<?php

namespace App\Models\Tables;
use DB;
use View;
use Illuminate\Database\Eloquent\Model;

class DMFrameCikkArkategoria extends basemodel
{
	 const
       MASTERTOTALCOUNTSQL='select count(artbl_tmp_id) as  totalcount
                         from
                  "03_cik".get_tmparkategori_1(%TEMPAZON%)',

      MASTERSQL='select
              artbl.artbl_tmp_id,
              artbl.artbl_id,
              artbl.artbl_cikk_id,
              artbl.artbl_arkategoria_kod,
              artbl.artbl_arkategoria_kod_nev,
              artbl.artbl_arres,
              artbl.artbl_kategoria,
              artbl.artbl_brutto_ar,
              artbl.artbl_akcioskategoria,
              artbl.artbl_akciosbrutto,
              artbl.artbl_kezdete,
              artbl.artbl_vege,
              artbl.artbl_letre_felh_nev,
              artbl.artbl_letre_dat,
              artbl.artbl_artbl_tmpazon_id,
              artbl.artbl_pnzn_id,
              artbl.artbl_pnzn_rovidites,
              artbl.artbl_pnzn_nev,
              artbl.artbl_afa_szazalek
             from
             "03_cik".get_tmparkategori_1(%TEMPAZON%) artbl
            where   1=1
			  ',
	_REFRESHTEMP='select * from  "03_cik".set_arkategori_temp (%TEMPID%,%TEMPAZON%,%TEMPSTATUS%)';
    //:TEMPAZON:
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_tmparkategori_1 (null)';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];


}
