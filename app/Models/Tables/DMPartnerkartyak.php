<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;
class DMPartnerkartyak extends BaseModel02Com  {
    public $table = '02_com.partner_kartyak';
    protected $alias = 'prtkrt';
   protected function setStoredProcedure(){
        $this->storedProcedure = '  02_com.partner_kartyak';
    }
      public function setFields(){
          $this->fields = collect([
					new Field("prtkrt_id", "a_id", true, false,null),
					new Field("prtkrt_prt_id", "a_id_mut", true, false,null),
					new Field("prtkrt_szam", "a_vc_20", true, false,null),
					new Field("prtkrt_kep", "a_vc_1200", true, false,null),
					new Field("prtkrt_qrkod", "a_vc_1200", true, false,null),
					new Field("prtkrt_tipus_kod", "a_kodok_c1", true, false,null),
					new Field("prtkrt_rovidmegjegyzes", "a_vc_254", true, false,null),
					new Field("prtkrt_megjegyzes", "a_blob", true, false,null),
					new Field("prtkrt_aktiv", "a_aktiv_boolean", true, false,null),
					new Field("prtkrt_letre_felh_nev", "a_felhasznalo_c10", true, false,null),
					new Field("prtkrt_letre_dat", "a_datum_ido", true, false,null),
					new Field("prtkrt_uuid", "a_uuid_id", true, false,null),
					new Field("prtkrt_eredeti_kinyomtatva", "a_kodok_c1", true, false,null),

                  ]);
      }
}
