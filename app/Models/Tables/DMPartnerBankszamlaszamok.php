<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;
class DMPartnerBankszamlaszamok extends BaseModel02Com
{

  public function initialTable(){
    $this->table =  '02_com.bankszamlaszamok';
  }

    public function setFields(){
      $this->fields = [
        new Field("bsz_id", "a_id", true, true,null),
        new Field("bsz_prt_id", "a_id_mut", true, false,null),
        new Field("bsz_pfjszk", "a_c1", true, false,null),
        new Field("bsz_bankszamlaszam", "a_vc_50_szamlaszam", true, false,null),
        new Field("bsz_atfutasi_napok", "a_integer_2", true, false,null),
        new Field("bsz_iban", "a_vc_50_szamlaszam", true, false,null),
        new Field("bsz_letre_felh_nev", "a_kodtipus_c10", true, false,null),
        new Field("bsz_letre_dat", "a_datum_ido", true, false,null),
        new Field("bsz_tipus_kod", "a_kodok_c1", true, false,null),
        new Field("bsz_bank_id", "a_id_mut", true, false,null),
        new Field("bsz_valasztott", "a_aktiv_boolean", true, false,null),
        new Field("bsz_uuid", "a_uuid_id", true, false,null),
      ];
    }
}
