<?php

namespace App\Models\Tables;

class DMFelhasznalo extends BaseModel01Sys  {
    public function initialTable(){
      $this->table =   '01_sys.felhasznalok';
    }

      public function setFields()
      {
        return [
          new Field("felh_userid", "a_id", true, true),
          new Field("felh_felhnev", "a_felhasznalo_c10", true, false),
          new Field("felh_jelszo", "a_kodtipus_c10", true, false),
          new Field("felh_ervenyesseg", "a_datum", true, false),
          new Field("felh_prt_id", "a_id_mut", true, false),
          new Field("felh_jogszintid", "a_id_mut", true, false),
          new Field("felh_pcname", "a_vc_250", true, false),
          new Field("felh_visible", "a_aktiv_boolean", true, false),
          new Field("felh_bejelentkezve", "a_aktiv_boolean", true, false),
          new Field("felh_jelszocsere", "a_aktiv_boolean", true, false),
          new Field("felh_letre_felh_nev", "a_felhasznalo_c10", true, false),
          new Field("felh_letre_dat", "a_datum_ido", true, false),
          new Field("felh_superkod", "a_c7", true, false),
          new Field("felh_rakt_id", "a_id_mut", true, false),
          new Field("felh_cikk_kereses", "a_kodok_c1", true, false),
          new Field("felh_kszk_id", "a_id_mut", true, false),
          new Field("felh_bizonylatidoszak_kod", "a_kodok_c1", true, false),
          new Field("felh_cikk_szum_keszlet_kod", "a_kodok_c1", true, false),
          new Field("felh_auto_r_nyomtatasa", "a_aktiv_boolean", true, false),
          new Field("felh_modositasimod_kod", "a_kodok_c1", true, false),
          new Field("felh_hibas_bejelentkezes", "a_integer_2", true, false),
          new Field("felh_volserialnum", "a_vc_254", true, false),
          new Field("felh_ertekesites_kod", "a_kodok_c1", true, false),
          new Field("felh_rtmbl_id", "a_id_mut", true, false),
          new Field("felh_web_users_id", "a_uuid_id_mut", true, false),
        ];
      }
}
