<?php

namespace App\Models\Tables;

use App\basemodel;

class DMnavtranzakciok extends basemodel
{
    	const

	   MASTERTOTALCOUNTSQL='select count(nvtrf_id) as  totalcount
	   				  from
                            "14_nav".get_navtranzakcio_fejek_1 (null)
                            where
                            1=1',
	   MASTERSQL='select
                      nvtrf_id,
                      nvtrf_xchtkn_id,
                      nvtrf_xchtkn_exchangetoken,
                      nvtrf_xchtkn_validfrom,
                      nvtrf_xchtkn_validto,
                      nvtrf_transactionid,
                      nvtrf_senddatetime,
                      nvtrf_recevedatetime,
                      nvtrf_letre_felh_nev,
                      nvtrf_letre_dat,
                      nvtrf_status_kod,
                      nvtrf_status_kod_nev,
                      nvtrf_createdatetime
                    from
                    "14_nav".get_navtranzakcio_fejek_1 (null)
                    where
                      1=1',
	   DETAILSQL='Select
                          nvtrt_id,
                          nvtrt_nvtrf_id,
                          nvtrt_inv_id,
                          nvtrt_inv_bizf_id,
                          nvtrt_inv_bizf_azon,
                          nvtrt_inv_bizf_esed_datum,
                          nvtrt_inv_bizf_telj_datum,
                          nvtrt_inv_status_kod,
                          nvtrt_inv_status_kod_nev,
                          nvtrt_inv_invoicedata,
                          nvtrt_inv_filename,
                          nvtrt_index,
                          nvtrt_letre_felh_nev,
                          nvtrt_letre_dat
                         from
                        "14_nav".get_navtranzakcio_tetelek_1 (
                ';
    //
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_navtranzakcio_tetelek_1 ()';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];

}
