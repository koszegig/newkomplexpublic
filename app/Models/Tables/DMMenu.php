<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class DMMenu extends BaseModel01Sys
{
  public function initialTable(){
    $this->table =   '01_sys.menu menu';
  }
 //   protected $MASTERFN ='get_menu_web_1 (?)';
 //   public $alias = 'menu';
//    public $table = '01_sys.menu menu';
//    public $MASTERCOMPOSITTYPE = 't_menu_2';


      public function setFields()
      {
        return [
          new Field("menu_id", "a_id", false, false),
          new Field("menu_title", "a_vc_250", false, false),
          new Field("menu_link", "a_vc_200", false, false),
          new Field("menu_parentid", "a_id_mut", false, false),
          new Field("menu_letre_felh_nev", "a_felhasznalo_c10", false, false),
          new Field("menu_letre_dat", "a_datum_ido", false, false),
          new Field("menu_action", "a_vc_120", false, false),
          new Field("menu_sorrend", "a_integer_2", false, false),
          new Field("menu_image_index", "a_integer_2", false, false),
          new Field("menu_shortcut", "a_c8", false, false),  ];
      }


}
