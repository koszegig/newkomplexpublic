<?php

namespace App\Models\Tables;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Spatie\Permission\Traits\HasRoles;
//use App\Traits\HasEmails;
//use App\Traits\HasGroups;
//use App\Traits\HasPhones;
use App\Models\Base\BaseModel01Sys;
use Laravel\Passport\HasApiTokens;
use App\Libraries\Field;

class User extends BaseModel01Sys implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    JWTSubject

{

    public $alias = 'user';
    protected $table = '01_sys.users';
    public $schema = '01_sys';
    protected $primaryKey = 'id';
    use Notifiable, Authenticatable, Authorizable, CanResetPassword;
    // HasRoles,HasGroups,HasEmails,HasPhones;

     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'avatar' => 'image',
    ];

    public function initialTable(){
      $this->table = 'users';
    }

    public function setFields(){
      $this->fields = [
        new Field("id", "uuid", true, true),
        new Field("username", "varchar", true, false),
        new Field("firstname", "varchar", true, false),
        new Field("middlename", "varchar", true, false),
        new Field("lastname", "varchar", true, false),
        new Field("password", "varchar", true, false),
        new Field("birth_date", "date", true, false),
        new Field("avatar", "bytea", true, false),
        new Field("status", "varchar", true, false),
        new Field("active", "bool", true, false),
        new Field("system", "bool", true, false),
        new Field("remember_token", "varchar", true, false),
        new Field("created_at", "timestamp", false, false),
        new Field("updated_at", "timestamp", false, false),
        new Field("deleted_at", "timestamp", false, false),
        new Field("created_by", "uuid", false, false),
        new Field("updated_by", "uuid", false, false),
      ];
    }

    public function initialHidden(){
      array_push($this->hidden, 'password', 'remember_token');
    }

    public function initialAppends(){
      array_push($this->appends, 'primary_email', 'primary_phone', 'name');
    }

    /**
     * Check whether the user has the requested role.
     */
    public function getNameAttribute()
    {
        $name = $this->firstname;
        $name .= " ";
        if (!is_null($this->middlename) &&  !empty($this->middlename)) {
            $name .= $this->middlename;
            $name .= " ";
        }
        $name .= $this->lastname;
        return trim($name);
    }

    /**
    * The answer that belong to the question.
    */
    public function getPrimaryEmailAttribute()
    {
        /*$email = $this->getPrimaryEmail();
        if(empty($email)) return '';
        return $email->email;**/
        return '';

    }

    /**
      * The answer that belong to the question.
      */
    public function getPrimaryPhoneAttribute()
    {
        /*$phone = $this->getPrimaryPhone();
        if(empty($phone)) return '';
        return $phone->phone;*/
        return '';
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
