<?php
namespace App\Models\Tables;

use App\Models\Base\BaseModel01Sys;
use App\Libraries\Field;

class DMParameterek extends BaseModel01Sys  {
    public function initialTable(){
      $this->table =   '01_sys.parameterek';
    }


      public function setFields()
      {
        return [
          new Field("par_id", "a_id", true, true),
          new Field("par_fgl_id", "a_id_mut", true, false),
          new Field("par_megnevezes", "a_vc_254", true, false),
          new Field("par_hiv_fgl_id", "a_id_mut", true, false),
          new Field("par_fnk_id", "a_id_mut", true, false),
          new Field("par_tipus_kod", "a_kodok_c1", true, false),
          new Field("par_opcionalis_in", "a_igen_nem_c1", true, false),
          new Field("par_ertek", "a_vc_254", true, false),
          new Field("par_letre_felh_nev", "a_kodtipus_c10", true, false),
          new Field("par_letre_dat", "a_datum_ido", true, false),
        ];
      }
}
