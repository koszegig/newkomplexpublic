<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel04Rak;
use App\Libraries\Field;

class DMJogcimek extends BaseModel04Rak
{

    protected $alias = 'jc';
    protected $table = '04_rak.jogcimek';


    protected function setStoredProcedure(){
        $this->storedProcedure = '  "04_rak".get_jogcimek_1 ()';
    }


    public function setFields(){
        $this->fields = collect([
                new Field("jc_id", "a_id", true, false,null),
                new Field("jc_azon", "a_c4", true, false,null),
                new Field("jc_nev", "a_vc_50_nev_hosszu", true, false,null),
                new Field("jc_relacio_kod", "a_kodok_c1", true, false,null),
                new Field("jc_relacio_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
                new Field("jc_keszlet_valtozasi_kod", "a_kodok_c1", true, false,null),
                new Field("jc_keszlet_valtozasi_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
                new Field("jc_nyilvar_hatas_in", "a_igen_nem_c1", true, false,null),
                new Field("jc_nyilvar_hatas_in_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
                new Field("jc_kontirozas_kod", "a_kodok_c1", true, false,null),
                new Field("jc_kontirozas_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
                new Field("jc_kulso_azon", "a_kodtipus_c10", true, false,null),
                new Field("jc_alkalmazott_in", "a_aktiv_boolean", true, false,null),
                new Field("jc_leiras", "a_blob", true, false,null),
                new Field("jc_letre_felh_nev", "a_kodtipus_c10", true, false,null),
                new Field("jc_letre_dat", "a_datum_ido", true, false,null),
                new Field("jc_cikkartonba", "a_aktiv_boolean", true, false,null),
                new Field("jc_cikkartonban_visible", "a_aktiv_boolean", true, false,null),
                new Field("jc_rakt_status_ertekesites_kod", "a_kodok_c1", true, false,null),
                new Field("jc_rakt_status_ertekesites_kod_nev", "a_vc_50_nev_hosszu", true, false,null),

        ]);
    }
}

