<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel03Cik;
use App\Libraries\Field;

class DMCikkek extends BaseModel03Cik
{

  public function initialTable(){
    $this->table =  '03_cik.cikkek';
  }
  public function setFields(){
    $this->fields = [
    new Field("cik_id", "a_id", false, false),
    new Field("cik_kod", "a_vc_15", false, false),
    new Field("cik_nev", "a_vc_200", false, false),
    new Field("cik_vtsz_id", "a_id_mut", false, false),
    new Field("cik_me_id", "a_id_mut", false, false),
    new Field("cik_afa_id", "a_id_mut", false, false),
    new Field("cik_aktiv", "a_aktiv_boolean", false, false),
    new Field("cik_letre_felh_nev", "a_kodtipus_c10", false, false),
    new Field("cik_letre_dat", "a_datum_ido", false, false),
    new Field("cik_megjegyzes", "a_blob", false, false),
    new Field("cik_kep", "a_vc_200", false, false),
    new Field("cik_tipus", "a_kodok_c1", false, false),
    new Field("cik_keszlet_mod", "a_kodok_c1", false, false),
    new Field("cik_jovedeki_termek", "a_kodok_c1", false, false),
    new Field("cik_arlistas", "a_kodok_c1", false, false),
    new Field("cik_szav_ido", "a_kodok_c1", false, false),
    new Field("cikk_handle", "a_id_mut", false, false),
    new Field("cik_suly", "a_n_6_5", false, false),
    new Field("cik_suly_me_id", "a_id_mut", false, false),
    new Field("cik_fixaras", "a_kodok_c1", false, false),
    new Field("cik_beszerzersinettoar", "a_n_12_3", false, false),
    new Field("cik_beszerzersibruttoar", "a_n_12_3", false, false),
    new Field("cik_egyseg_ar", "a_n_12_3", false, false),
    new Field("cik_forditottafa", "a_kodok_c1", false, false),
    new Field("cik_netadotk_id", "a_id_mut", false, false),
    new Field("cik_trmkdj_id", "a_id_mut", false, false),
    new Field("cik_armeny_szorzo", "a_integer_8", false, false),
    new Field("cik_armeny_me_id", "a_id_mut", false, false),
    new Field("cik_csatolt_dokumentum_path", "a_vc_800", false, false),
    new Field("cik_csatolt_tervrajz", "a_vc_800", false, false),
    new Field("cik_imprc_sorszam", "a_vc_200", false, false),
    new Field("cik_kozv_szolgaltatas", "a_kodok_c1", false, false),
    new Field("cik_szigoruszamadasu", "a_aktiv_boolean", false, false),
    new Field("cik_lapszam", "a_integer_8", false, false),
    new Field("cik_beszmodidopontja", "a_datum_ido", false, false),
    new Field("cik_kiszereles", "a_n_3_2", false, false),
    new Field("cik_alkoholfok", "a_n_3_2", false, false),
    new Field("cik_veglegtorolt", "a_aktiv_boolean", false, false),
    new Field("cik_be_rakt_id", "a_id_mut", false, false),
    new Field("cik_ki_rakt_id", "a_id_mut", false, false),
    new Field("cik_kiszereles_me_id", "a_id_mut", false, false),
    new Field("cik_egysegar_me_id", "a_id_mut", false, false),
    new Field("cik_uuid", "a_uuid_id", false, false),
    ];
  }

}
