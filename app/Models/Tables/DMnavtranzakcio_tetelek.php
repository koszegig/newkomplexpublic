<?php

namespace App\Models\Tables;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;
use Auth;
use View;
class DMnavtranzakcio_tetelek extends basemodel
{
        const
       MASTERTOTALCOUNTSQL='select count(nvtrt_id) as  totalcount
                  from
                    "14_nav".get_navtranzakcio_tetelek_1 (null)
                        where
                        1=1 ',

       MASTERSQL='
                Select
                  nvtrt_id,
                  nvtrt_nvtrf_id,
                  nvtrt_inv_id,
                  nvtrt_inv_bizf_id,
                  nvtrt_inv_bizf_azon,
                  nvtrt_inv_bizf_esed_datum,
                  nvtrt_inv_bizf_telj_datum,
                  nvtrt_inv_status_kod,
                  nvtrt_inv_status_kod_nev,
                  nvtrt_inv_invoicedata,
                  nvtrt_inv_filename,
                  nvtrt_index,
                  nvtrt_inv_status,
                  nvtrt_letre_felh_nev,
                  nvtrt_letre_dat,
                  nvtrt_nvtrf_transactionid
                 from
                "14_nav".get_navtranzakcio_tetelek_1 (null)
                where
                1=1
       ';
    //
    //$this->shemaname='01_sys';
     protected $connection = 'pgsql';
     /**
     * The table associated with the model.
     *
     * @var string
     */
    //
    //$this->shemaname='01_sys';
     protected $connection = 'pgsql';
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_navtranzakcio_tetelek_1 ()';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

    public $timestamps = false;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */

     protected $fillable =  ['name'];


}
