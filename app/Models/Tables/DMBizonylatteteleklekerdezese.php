<?php

namespace App\Models\Tables;

use App\basemodel;

class DMBizonylatteteleklekerdezese extends BaseModel04Rak
{
	const

	   MASTERTOTALCOUNTSQL='select count(bizf.bizf_id) as  totalcount
	   				  from
				  "04_rak".get_biz_fejek_1(',
	   MASTERSQL='select
				-- DISTINCT on(bizf.bizf_id)
				  1 as ssz,
				  count(bizf.bizf_id) over() as totalcount,
				  bizf.bizf_id  ,
				  bizf.bizf_azon  ,
				  bizf.bizf_azon_id  ,
				  bizf.bizf_azon_egyseg_prt_id  ,
				  bizf.bizf_azon_prt_nev  ,
				  bizf.bizf_azon_biztip_kod  ,
				  bizf.bizf_azon_biztip_kod_nev  ,
				  bizf.bizf_azon_ev  ,
				  bizf.bizf_atado_prt_id  ,
				  bizf.bizf_atado_prt_nev  ,
				  bizf.bizf_atado_prt_adoszam  ,
				  bizf.bizf_atado_prt_cim_orsz_nev  ,
				  bizf.bizf_atado_prt_cim_helys_megye  ,
				  bizf.bizf_atado_prt_cim_helys_irsz  ,
				  bizf.bizf_atado_prt_cim_helys_nev  ,
				  bizf.bizf_atado_prt_cim_kozterulet_jelleg_kod_nev  ,
				  bizf.bizf_atado_prt_cim_kozterulet_nev  ,
				  bizf.bizf_atado_prt_cim_hazszam  ,
				  bizf.bizf_atado_prt_cim_epulet  ,
				  bizf.bizf_atado_prt_cim_emelet  ,
				  bizf.bizf_atado_prt_cim_lepcsohaz  ,
				  bizf.bizf_atado_prt_cim_ajto  ,
				  bizf.bizf_atado_prt_bsz_bankszamlaszam  ,
				  bizf.bizf_atvevo_prt_id  ,
				  bizf.bizf_atvevo_prt_nev  ,
				  bizf.bizf_atvevo_prt_cim_orsz_nev  ,
				  bizf.bizf_atvevo_prt_cim_helys_megye  ,
				  bizf.bizf_atvevo_prt_cim_helys_irsz  ,
				  bizf.bizf_atvevo_prt_cim_helys_nev  ,
				  bizf.bizf_atvevo_prt_cim_kozterulet_jelleg_kod_nev  ,
				  bizf.bizf_atvevo_prt_cim_kozterulet_nev  ,
				  bizf.bizf_atvevo_prt_cim_hazszam  ,
				  bizf.bizf_atvevo_prt_cim_epulet  ,
				  bizf.bizf_atvevo_prt_cim_lepcsohaz  ,
				  bizf.bizf_atvevo_prt_cim_emelet  ,
				  bizf.bizf_atvevo_prt_cim_ajto  ,
				  bizf.bizf_atvevo_prt_bsz_bankszamlaszam  ,
				  bizf.bizf_atvevo_prt_adoszam  ,
				  bizf.bizf_ukoto_prt_id  ,
				  bizf.bizf_ukoto_prt_nev  ,
				  bizf.bizf_fizeto_prt_id   ,
				  bizf.bizf_fizeto_prt_nev  ,
				  bizf.bizf_fuv_prt_id  ,
				  bizf.bizf_fuv_prt_nev  ,
				  bizf.bizf_penznem_id  ,
				  bizf.bizf_penznem_nev  ,
				  bizf.bizf_fztm_id  ,
				  bizf.bizf_fizmod_kod_nev ,
				  bizf.bizf_feldolg_status_kod  ,
				  bizf.bizf_feldolg_status_kod_nev  ,
				  bizf.bizf_biz_szam  ,
				  bizf.bizf_szlev_szam  ,
				  bizf.bizf_szla_szam  ,
				  bizf.bizf_telj_datum  ,
				  bizf.bizf_konyv_datum  ,
				  bizf.bizf_esed_datum,
				  bizf.bizf_kif_datuma  ,
				  bizf.bizf_rend_ert  ,
				  bizf.bizf_nyilv_ert  ,
				  bizf.bizf_netto_ert  ,
				  bizf.bizf_brutto_ert   ,
				  bizf.bizf_tetel_szam  ,
				  bizf.bizf_rendelt_tetel_szam  ,
				  bizf.bizf_leigazolas_datuma  ,
				  bizf.bizf_pufelad_datuma ,
				  bizf.bizf_eredeti_kinyomtatva  ,
				  bizf.bizf_eredeti_kinyomtatva_kod_nev  ,
				  bizf.bizf_masolat_peldanyszama  ,
				  bizf.bizf_masodlat_peldanyszama  ,
				  bizf.bizf_nyomtatas_szam  ,
				  bizf.bizf_rakt_id  ,
				  bizf.bizf_rakt_nev  ,
				  bizf.bizf_kszk_id  ,
				  bizf.bizf_kszk_nev  ,
				  bizf.bizf_pnztr_id ,
				  bizf.bizf_pnzn_nev,

				  bizf.bizf_letre_felh_nev  ,
				  bizf.bizf_letre_dat ,
				  bizf.bizf_afa_ertek  ,
				  bizf.bizf_rovidmegjegyzes  ,
				  bizf.bizf_vrend_iktatoszama  ,
				  bizf.bizf_fizetendo_brutto_ert ,
				  bizf.bizf_atvevoszallitasicim_helyseg  ,
				  bizf.bizf_atvevoszallitasicim_kozterulet,
				  bizf.bizf_nem_arazott,
				  bizf.bizf_visszavett,
				  bizf_mulapf_id,
				  bizf_mulapf_munkaszam,
				  bizf_sztorno_bizf_id,
				  bizf_sztornozott_bizf_id,
				  bizf_bizonylatkep,
				bizf_netto_beszerzesi_ert,
				  bizf_brutto_beszerzesi_ert,
				  bizf_kifizetendo_beszerzesi_ert,
				  bizf_fkod_id,
				  bizf_mozgas_kod,
				  bizf_mozgas_nev,
				bizf_ervenyeseg,
				bizf_kifizetett_brutto_ert,
				  bizf.bizf_maradekosszeg,
				  bizf.bizf_beszerzesi_maradekosszeg,

				bizf.bizf_kifizetes_datuma,
				bizf.bizf_beszerzesi_afa_erteke,
				 bizf.bizf_afa_id,
				 bizf.bizf_afa_nev,
				 bizf.bizf_atvevo_prt_bsz_iban,
				 bizf.bizf_atvevo_prt_bank_swift_kod,
				 bizf.bizf_atvevo_prt_kozossegiszam,
				 bizf.bizf_atado_prt_bsz_iban,
				 bizf.bizf_atado_prt_bank_swift_kod,
				 bizf.bizf_atado_prt_kozossegiszam,
				 bizf.bizf_munhely_megnevezes,
				 bizf.bizf_munhely_cim,
				 bizf.bizf_mulapf_megnevezese,
				 bizf.bizf_nemarazotetelszam as nemarazotetelszam,
				 bizf.bizf_bearazotetelszam as bearazotetelszam,
				 bizf.bizf_atado_prt_prt_cim_id,
				 bizf.bizf_atvevo_prt_prt_cim_id,
				 bizf.bizf_reszben_arazott,
				 bizf.bizf_lezartbizonylat_tomb
				  from
				  "04_rak".get_biz_fejek_1(',
	   DETAILSQL='Select
					  bizt.bizt_id,
					  bizt.bizt_bizf_id ,
					  bizt.bizt_bizf_id as bizf_id,
					  bizt.bizt_bizf_azon ,
					  bizt.bizt_cik_id,
					  bizt.bizt_cik_kod ,
					  bizt.bizt_cik_nev,
					  bizt.bizt_mnklp_tetel_id,
					  bizt.bizt_bizf_szlev_id,
					  bizt.bizt_bizf_szlev_szam,
					  bizt.bizt_jc_id,
					  bizt.bizt_jc_nev,
					  bizt.bizt_feldolg_status_kod,
					  bizt.bizt_feldolg_status_kod_nev,
					  bizt.bizt_afa_id,
					  bizt.bizt_afa_kod,
					  bizt.bizt_tet_sorszam,
					  bizt.bizt_konyv_datum,
					  bizt.bizt_menny,
					  bizt.bizt_me_id,
					  bizt.bizt_me_nev,
					  bizt.bizt_nyilv_ar,
					  bizt.bizt_alap_ar,
					  bizt.bizt_elad_ar,
					  bizt.bizt_fogy_ar,
					  bizt.bizt_szav_datum,
					  bizt.bizt_artbl_arkategoria_kod_kod,
					  bizt.bizt_artbl_arkategoria_kod_nev,
					  bizt.bizt_megjegyzes,
					  bizt.bizt_rakt_id,
					  bizt.bizt_rakt_nev,
					  bizt.bizt_letre_felh_nev,
					  bizt.bizt_letre_dat,
					  bizt.bizt_brutto_nyilv_ar,
					  bizt.bizt_brutto_alap_ar,
					  bizt.bizt_brutto_elad_ar,
					  bizt.bizt_brutto_fogy_ar,
					  bizt.bizt_prt_egyenikedvezmeny,
					  bizt.bizt_me_rovidites,
					  bizt.bizt_akcios_ar,
					  bizt.bizt_akcios_ar_kod_nev,
					  bizt.bizt_afaertek,
					  bizt.bizt_cik_egyseg_ar,
					  bizt.bizt_cik_brutto_egyseg_ar,
					  bizt.bizt_cik_beszerzesi_ar,
					  bizt.bizt_cik_bruttto_beszerzesi_ar,
					  bizt.bizt_visszavett_menny,
					  bizt.bizt_cgy_id,
					  bizt.bizt_cgy_gyariszam,
					  bizt.bizt_beszerzesi_ar,
					  bizt.bizt_beszerzesi_brutto_ar,
					  bizt.bizt_beszerzesi_afaertek,
					  bizt.bizt_forditottafa,
					  bizt.bizt_forditottafa_kod_nev,
					  bizt.bizt_sulymennyiseg,
					  bizt.bizt_cik_suly_me_id,
					  bizt.bizt_me_suly_rovidites,
					  bizt.bizt_arazott_menny,
					  bizt.bizt_armeny_szorzo,
					  bizt.bizt_armeny_me_id,
					  bizt.bizt_cikk_vtsz_szam,
					  bizt.bizt_jc_relacio_kod,
					  bizt.bizt_jc_relacio_kod_nev

					 from "04_rak".get_biz_tetelei_6( ';
    //
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_biz_fejek_1 ()';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];
}
