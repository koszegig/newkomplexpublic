<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;

class Email extends BaseModel02Com
{
    public function initialTable(){
      $this->table = 'emails';
    }

    public function setFields(){
      $this->fields = [
        new Field("id", "uuid", true, true),
        new Field("email", "varchar", true, false),
        new Field("primary", "bool", true, false),
        new Field("related_id", "uuid", true, false),
        new Field("related_type", "varchar", true, false),
        new Field("active", "bool", true, false),
        new Field("created_at", "timestamp", false, false),
        new Field("updated_at", "timestamp", false, false),
        new Field("deleted_at", "timestamp", false, false),
        new Field("created_by", "uuid", false, false),
        new Field("updated_by", "uuid", false, false),
      ];
    }

     /**
      * Find an email by its name.
      *
      * @param string $email
      *
      * @return App\Models\Tables\Email
      *
      * @throws App\Exceptions\EmailDoesNotExist
      */

    public static function findByEmail(string $email)
    {
        $_email = static::where('email', $email)->first();

        if (! $_email) {
            throw EmailDoesNotExist::named($email);
        }

        return $_email;
    }

    /**
     * Find an email by its id.
     *
     * @param string $id
     *
     * @return App\Models\Tables\Email
     *
     * @throws App\Exceptions\EmailDoesNotExist
     */

    public static function findById(string $id)
    {
        $_email = static::where('id', $id)->first();

        if (! $_email) {
            throw EmailDoesNotExist::withId($id);
        }

        return $_email;
    }

     /**
      * Find an email by its id.
      *
      * @param string $id
      *
      * @return App\Models\Tables\Email
      *
      * @throws App\Exceptions\EmailDoesNotExist
      */

    public static function getPrimary($id)
    {
        $email = static::where('related_id', $id)->where('primary', true)->first();

        if (! $email) {
            throw EmailDoesNotExist::primary();
        }

        return $email;
    }
}
