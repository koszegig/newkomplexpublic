<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;
class DMKaszak extends BaseModel02Com
{
  public function initialTable(){
    $this->table =  '02_com.kaszak';
  }


  public function setFields(){
    $this->fields = [
      new Field("kszk_id", "a_id", true, false,null),
      new Field("kszk_prt_id", "a_id_mut", true, false,null),
      new Field("kszk_kod", "a_c5", true, false,null),
      new Field("kszk_nev", "a_vc_50_nev_partner", true, false,null),
      new Field("kszk_aktiv", "a_aktiv_boolean", true, false,null),
      new Field("kszk_default", "a_aktiv_boolean", true, false,null),
      new Field("kszk_partner_kod", "a_kodok_c1", true, false,null),
      new Field("kszk_letre_felh_nev", "a_kodtipus_c10", true, false,null),
      new Field("kszk_letre_dat", "a_datum_ido", true, false,null),
      new Field("kszk_pnztr_id", "a_id_mut", true, false,null),
      new Field("kszk_uuid", "a_uuid_id", true, false,null),
    ];
  }

}
