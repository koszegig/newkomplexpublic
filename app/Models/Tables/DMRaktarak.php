<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;
class DMRaktarak extends BaseModel02Com
{
    protected $MASTERFN ='get_raktarak_1 (?)';
    public $alias = 'rakt';
    public $table = '02_com.raktarak';
    public $MASTERCOMPOSITTYPE = 't_raktarak_1';
    public $USERMASTERFN = 'get_raktarak_2 ( ? )';
    protected $fields = [];


    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
      parent::__construct($attributes);
    }

    public function __getFields()
    {
      return [
        new Field(true,'rakt_uuid','rakt_uuid', true, ['master','usermaster'], true),
        new Field(true,'rakt_id','rakt_id', false, ['master','usermaster'], false),
        //new Field(false, '1', 'ssz', false, ['master','usermaster'], false),
        new Field(false, 'count(rakt_id) over()', 'totalcount', false, ['master','usermaster'], false),
        new Field(true,'rakt_prt_id','rakt_prt_id', true, ['master','usermaster'], false),
        new Field(true,'rakt_prt_nev','rakt_prt_nev', false, ['master','usermaster'], false),
        new Field(true,'rakt_nev','rakt_nev', true, ['master','usermaster'], false),
        new Field(true,'rakt_aktiv','rakt_aktiv', true, ['master','usermaster'], false),
        new Field(true,'rakt_jovedeki_termek','rakt_jovedeki_termek', true, ['master','usermaster'], false),
        new Field(true,'rakt_jovedeki_termek_kod_nev','rakt_jovedeki_termek_kod_nev', false, ['master','usermaster'], false),
        new Field(true,'rakt_rendezo','rakt_rendezo', true, ['master','usermaster'], false),
        new Field(true,'rakt_status_ertekesites_kod','rakt_status_ertekesites_kod', true, ['master','usermaster'], false),
        new Field(true,'rakt_status_ertekesites_kod_nev','rakt_status_ertekesites_kod_nev', false, ['master','usermaster'], false),


      ];
    }
}
