<?php

namespace App\Models\Tables;
use DB;
use View;
use Illuminate\Database\Eloquent\Model;

class DMFrameVonalkodok extends basemodel
{
	 const
       MASTERTOTALCOUNTSQL='select count(vonal_id) as  totalcount
                         from
                  "03_cik".get_tmpvonalkodok_1(%TEMPAZON%)',

      MASTERSQL='select
  			vonal.*
 		from "03_cik".get_tmpvonalkodok_1(%TEMPAZON%) vonal
            where   1=1
			  ',
	_REFRESHTEMP='select * from  "03_cik".setvonalkodtemp (%TEMPID%,%TEMPAZON%,%TEMPSTATUS%)';
    //:TEMPAZON:
	//$this->shemaname='01_sys';
     protected $connection = 'pgsql';
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'get_tmpvonalkodok_1 (null)';

	/**
	 * Indicates if the IDs are auto-incrementing.
	 *
	 * @var bool
	 */

	public $incrementing = false;

	/**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */

	public $timestamps = false;

	/**
     * The "booting" method of the model.
     *
     * @return void
     */

	 protected $fillable =  ['name'];


}
