<?php

namespace App\Models\Tables;
use App\Models\Base\BaseModel02Com;
use App\Libraries\Field;
class DMPenznem extends BaseModel02Com
{
    public function initialTable(){
      $this->table =  '02_com.penznemek';
    }
    public function setFields(){
      $this->fields = [
        new Field("pnzn_id", "a_id", true, false,null),
        new Field("pnzn_rovidites", "a_c5", true, false,null),
        new Field("pnzn_nev", "a_vc_50_nev_hosszu", true, false,null),
        new Field("pnzn_aktiv", "a_aktiv_boolean", true, false,null),
        new Field("pnzn_default", "a_aktiv_boolean", true, false,null),
        new Field("pnzn_kerekit", "a_aktiv_boolean", true, false,null),
        new Field("pnzn_szamlaszam", "a_vc_50_szamlaszam", true, false,null),
        new Field("pnzn_letre_felh_nev", "a_kodtipus_c10", true, false,null),
        new Field("pnzn_letre_dat", "a_datum_ido", true, false,null),
        new Field("pnzn_jel", "a_c5", true, false,null),
        new Field("pnzn_kerekitesmerteke", "a_integer_2", true, false,null),
        new Field("pnzn_uuid", "a_uuid_id", true, false,null),

      ];
    }

    public static function getPenznem($pnzn_rovidites)
    {
        $sql = 'Select p.pnzn_id from "02_com".get_penznem_1() as p  where pnzn_rovidites=":pnzn_rovidites"';
         $resultDatas = DMPenznem::objecttoArray(DB::select($sql, ['pnzn_rovidites' => $pnzn_rovidites]));
        return $resultDatas[0]['pnzn_id'];
    }
    public static function getPenznemDefault()
    {
        $sql = 'Select pnzn_id from "02_com".get_penznem_1() as p  where p.pnzn_default=TRUE';
         $resultDatas = DMPenznem::objecttoArray(DB::select($sql));
        return $resultDatas[0]['pnzn_id'];
    }


}
