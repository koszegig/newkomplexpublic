<?php

namespace App\Models\Tables;

use App\Models\Base\BaseModel04Rak;
use App\Libraries\Field;
class DMBizonylattetelek extends BaseModel04Rak
{

    public $alias = 'bizt';
    protected $table = '04_rak.biz_tetelek';
    public $typeSelect = [
        'bizt_id',
        'bizt_bizf_id ',
        'bizt_bizf_id as bizf_id',
        'bizt_bizf_azon ',
        'bizt_cik_id',
        'bizt_cik_kod ',
        'bizt_cik_nev',
        'bizt_mnklp_tetel_id',
        'bizt_bizf_szlev_id',
        'bizt_bizf_szlev_szam',
        'bizt_jc_id',
        'bizt_jc_nev',
        'bizt_feldolg_status_kod',
        'bizt_feldolg_status_kod_nev',
        'bizt_afa_id',
        'bizt_afa_kod',
        'bizt_tet_sorszam',
        'bizt_konyv_datum',
        'bizt_menny',
        'bizt_me_id',
        'bizt_me_nev',
        'bizt_nyilv_ar',
        'bizt_alap_ar',
        'bizt_elad_ar',
        'bizt_fogy_ar',
        'bizt_szav_datum',
        'bizt_artbl_arkategoria_kod_kod',
        'bizt_artbl_arkategoria_kod_nev',
        //   'bizt_megjegyzes',
        'bizt_rakt_id',
        'bizt_rakt_nev',
        'bizt_letre_felh_nev',
        'bizt_letre_dat',
        'bizt_brutto_nyilv_ar',
        'bizt_brutto_alap_ar',
        'bizt_brutto_elad_ar',
        'bizt_brutto_fogy_ar',
        'bizt_prt_egyenikedvezmeny',
        'bizt_me_rovidites',
        'bizt_akcios_ar',
        'bizt_akcios_ar_kod_nev',
        'bizt_afaertek',
        'bizt_cik_egyseg_ar',
        'bizt_cik_brutto_egyseg_ar',
        'bizt_cik_beszerzesi_ar',
        'bizt_cik_bruttto_beszerzesi_ar',
        'bizt_visszavett_menny',
        'bizt_cgy_id',
        'bizt_cgy_gyariszam',
        'bizt_beszerzesi_ar',
        'bizt_beszerzesi_brutto_ar',
        'bizt_beszerzesi_afaertek',
        'bizt_forditottafa',
        'bizt_forditottafa_kod_nev',
        'bizt_sulymennyiseg',
        'bizt_cik_suly_me_id',
        'bizt_me_suly_rovidites',
        'bizt_arazott_menny',
        'bizt_armeny_szorzo',
        'bizt_armeny_me_id',
        'bizt_cikk_vtsz_szam',
        'bizt_jc_relacio_kod',
        'bizt_jc_relacio_kod_nev',
        'bizt_jc_keszlet_valtozasi_kod',
        'bizt_rendelt_mennyiseg',
        'bizt_eladott_mennyiseg',
        'bizt_cik_fixaras'
    ];
    public $typeModel = '\App\Models\Types\TBiztetelek6Web';
      /**
     * Create a new Eloquent model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function setFields(){
        $this->fields = collect([
            new Field("bizt_id", "a_id", true, false,null),
            new Field("bizt_bizf_id", "a_id_mut", true, false,null),
            new Field("bizt_bizf_azon", "a_vc_30_nev_rovid", true, false,null),
            new Field("bizt_cik_id", "a_id_mut", true, false,null),
            new Field("bizt_cik_kod", "a_vc_15", true, false,null),
            new Field("bizt_cik_nev", "a_vc_200", true, false,null),
            new Field("bizt_mnklp_tetel_id", "a_id_mut", true, false,null),
            new Field("bizt_bizf_szlev_id", "a_id_mut", true, false,null),
            new Field("bizt_bizf_szlev_szam", "a_vc_254", true, false,null),
            new Field("bizt_jc_id", "a_id_mut", true, false,null),
            new Field("bizt_jc_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_feldolg_status_kod", "a_kodok_c1", true, false,null),
            new Field("bizt_feldolg_status_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_afa_id", "a_id_mut", true, false,null),
            new Field("bizt_afa_kod", "a_c3", true, false,null),
            new Field("bizt_tet_sorszam", "a_integer_2", true, false,null),
            new Field("bizt_konyv_datum", "a_datum_ido", true, false,null),
            new Field("bizt_menny", "a_n_9_3", true, false,null),
            new Field("bizt_me_id", "a_id_mut", true, false,null),
            new Field("bizt_me_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_nyilv_ar", "a_n_12_3", true, false,null),
            new Field("bizt_alap_ar", "a_n_12_3", true, false,null),
            new Field("bizt_elad_ar", "a_n_12_3", true, false,null),
            new Field("bizt_fogy_ar", "a_n_12_3", true, false,null),
            new Field("bizt_szav_datum", "a_datum", true, false,null),
            new Field("bizt_artbl_arkategoria_kod_kod", "a_kodok_c1", true, false,null),
            new Field("bizt_artbl_arkategoria_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_megjegyzes", "a_blob", true, false,null),
            new Field("bizt_rakt_id", "a_id_mut", true, false,null),
            new Field("bizt_rakt_nev", "a_vc_50_nev_partner", true, false,null),
            new Field("bizt_letre_felh_nev", "a_kodtipus_c10", true, false,null),
            new Field("bizt_letre_dat", "a_datum_ido", true, false,null),
            new Field("bizt_brutto_nyilv_ar", "a_n_12_3", true, false,null),
            new Field("bizt_brutto_alap_ar", "a_n_12_3", true, false,null),
            new Field("bizt_brutto_elad_ar", "a_n_12_3", true, false,null),
            new Field("bizt_brutto_fogy_ar", "a_n_12_3", true, false,null),
            new Field("bizt_prt_egyenikedvezmeny", "a_n_5_3", true, false,null),
            new Field("bizt_me_rovidites", "a_c5", true, false,null),
            new Field("bizt_akcios_ar", "a_kodok_c1", true, false,null),
            new Field("bizt_akcios_ar_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_afaertek", "a_n_12_3", true, false,null),
            new Field("bizt_bizf_penznem_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_armeny_szorzo", "a_integer_2", true, false,null),
            new Field("bizt_armeny_me_id", "a_id_mut", true, false,null),
            new Field("bizt_armeny_me_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_cik_egyseg_ar", "a_n_12_3", true, false,null),
            new Field("bizt_cik_brutto_egyseg_ar", "a_n_12_3", true, false,null),
            new Field("bizt_cik_beszerzesi_ar", "a_n_12_3", true, false,null),
            new Field("bizt_cik_bruttto_beszerzesi_ar", "a_n_12_3", true, false,null),
            new Field("bizt_jc_relacio_kod", "a_kodok_c1", true, false,null),
            new Field("bizt_jc_relacio_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_arazott_menny", "a_n_9_3", true, false,null),
            new Field("bizt_arozando_mennyiseg", "a_n_9_3", true, false,null),
            new Field("bizt_bizf_fztm_id", "a_id_mut", true, false,null),
            new Field("bizt_bizf_fizmod_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_bizf_penznem_id", "a_id_mut", true, false,null),
            new Field("bizt_cgy_id", "a_id_mut", true, false,null),
            new Field("bizt_cgy_gyariszam", "a_vc_30_nev_rovid", true, false,null),
            new Field("bizt_visszavett_menny", "a_n_9_3", true, false,null),
            new Field("bizt_beszerzesi_ar", "a_n_12_3", true, false,null),
            new Field("bizt_beszerzesi_brutto_ar", "a_n_12_3", true, false,null),
            new Field("bizt_beszerzesi_afaertek", "a_n_12_3", true, false,null),
            new Field("bizt_forditottafa", "a_kodok_c1", true, false,null),
            new Field("bizt_forditottafa_kod_nev", "a_vc_50_nev_hosszu", true, false,null),
            new Field("bizt_cikk_vtsz_szam", "a_vc_15", true, false,null),
            new Field("bizt_sulymennyiseg", "a_n_9_3", true, false,null),
            new Field("bizt_cik_suly_me_id", "a_id_mut", true, false,null),
            new Field("bizt_me_suly_rovidites", "a_c5", true, false,null),
            new Field("bizt_kcs_id", "a_id_mut", true, false,null),
            new Field("bizt_vonal_azon", "a_vc_20", true, false,null),
            new Field("bizt_alapossznettoar", "a_n_12_3", true, false,null),
            new Field("bizt_alapossz_afaertek", "a_n_12_3", true, false,null),
            new Field("bizt_brutto_alapossz_ertek", "a_n_12_3", true, false,null),
            new Field("bizt_arengedmenystr", "a_vc_20", true, false,null),
            new Field("bizt_szamolas", "a_aktiv_boolean", true, false,null),
            new Field("bizt_engedely", "a_aktiv_boolean", true, false,null),
            new Field("bizt_jc_keszlet_valtozasi_kod", "a_kodok_c1", true, false,null),
            new Field("bizt_rendelt_mennyiseg", "a_n_9_3", true, false,null),
            new Field("bizt_eladott_mennyiseg", "a_n_9_3", true, false,null),
            new Field("bizt_cik_fixaras", "a_kodok_c1", true, false,null),
            new Field("bizt_uuid", "a_uuid_id", true, false,null),
            new Field("bizt_bizf_uuid", "a_uuid_id_mut", true, false,null),
            new Field("bizt_foglaltmenny", "a_n_9_3", true, false,null),

        ]);
    }
    public static function bizonylattetlekrendberakasa($bizf_id,$biztip_kod='')
    {

        $sql = 'select * from "04_rak".set_bizonylattetlekrendberakasa_1('.$bizf_id.')';
        if ($biztip_kod=='c') {
            $sql = 'select * from "04_rak".set_bizonylattetlekrendberakasa_5('.$bizf_id.')';
        }
        \DB::select($sql);
        return true;
    }
    public static function bizonylatfejujraszamolasa($bizf_id)
    {

        $sql = 'select * from "04_rak".set_bizonylatfej_ujraszamolasa_1('.$bizf_id.')';
        \DB::select($sql);
        return true;
    }


}
