<?php


namespace App\Models\Unas;
use App\Libraries\Field;
use App\Models\Base\UnasRestApi;
use Illuminate\Database\Eloquent\Model;

class Products extends UnasRestApi
{
    /**
     * @var string
     */
    public $table="CCProducts";
    protected $tablename='CCProducts';
    protected $guarded = [];
    protected function setStoredProcedure(){
        $this->storedProcedure = 'CCProducts';
    }
    /**
     * @return Models\Base\RootModel|void
     */
    public function setFields()
    {
        $this->fields = collect([
            new Field("ProductId", "int4", true, null, null, true, null),
            new Field("ProductName", "varchar", true, null, null, false, null),
            new Field("SalesVat", "varchar", true, null, null, false, null),
            new Field("UnitId", "varchar", true, null, null, false, null),
            new Field("PurchasingUnitId", "varchar", true, null, null, false, null),
            new Field("IsDivisible", "varchar", true, null, null, false, null),
            new Field("Packaging", "varchar", true, null, null, false, null),
            new Field("ProductComment", "varchar", true, null, null, false, null),
            new Field('created_at', "timestamp", true, null, null, false, null),
            new Field('updated_at', "timestamp", true, null, null, false, null),
        ]);
    }

}
