<?php


namespace App\Models\Base;
use App\Models\Base\RootModel;
use Illuminate\Database\Eloquent\SoftDeletes;


class BaseSAPModel  extends RootModel
{
    public $timestamps = false;
    /**
     * The connection associated with the model.
     *
     * @var string
     */
    protected $connection = 'sapsqlsrv';

    public function getSAPConnection()
    {
        return $this->connection;
    }
//use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    //protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = [];

    /**
     *
     * @var string
     */
    protected $storedProcedure;


    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->setStoredProcedure();
        parent::__construct($attributes);
    }

    protected function setStoredProcedure(){}

    public function modifyActive($_active)
    {
        $this->active = $_active;
        if ($this->save()) {
            return $this;
        }
        return false;
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderbyname($query)
    {
        return $query->orderBy('name');
    }

    public function scopeExclude($query, $value = array())
    {
        return $query->select(array_diff($this->fillable, (array)$value));
    }

    public function getStoredProcedure(){
        return $this->storedProcedure;
    }


}
