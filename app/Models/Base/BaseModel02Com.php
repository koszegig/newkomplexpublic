<?php

namespace App\Models\Base;

use App\Models\Base\BaseModel;

class BaseModel02Com extends BaseModel
{
  public $schema = '02_com';
  protected $connection = 'pgsql_02_com';
}
