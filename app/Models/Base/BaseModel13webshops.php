<?php
namespace App\Models\Base;

use App\Models\Base\BaseModel;

class BaseModel13webshops  extends BaseModel
{
    public $schema = '13_webshops';
    protected $connection = 'pgsql_13_webshops';
}
