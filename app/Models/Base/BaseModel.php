<?php

namespace App\Models\Base;

use App\Models\Base\RootModel;


class BaseModel extends RootModel
{
    protected $storedProcedure;

      /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->setFields();
        $this->initialTable();
        $this->setStoredProcedure();
        $this->processField();
        $this->initialHidden();
        parent::__construct($attributes);
        $this->initialAppends();
    }

    public function processField(){
      foreach($this->fields as $field){
        $name = $field->getName();
        if($field->isFillable()){
          array_push($this->fillable, $name);
        }
        if($field->isPrimary()){
          $this->primaryKey = $name;
        }
        $cast = $field->getCast();
        if($cast != null){
          $this->cast[$name] = $cast;
        }
      }
    }

    public function getFillable(){
      return $this->fillable;
    }

    public function initialTable(){}

    protected function setStoredProcedure(){}

    public function initialHidden(){}

    public function initialAppends(){}

    public function modifyActive($_active)
    {
        $this->active = $_active;
        if ($this->save()) {
            return $this;
        }
        return false;
    }
    public function modifyelrogzites($_value)
    {
        $this->bizf_elorogzites_kod = $_value;
        if ($this->save()) {
            return $this;
        }
        return false;
    }

    /**
      * Scope a query to only include active users.
      *
      * @param \Illuminate\Database\Eloquent\Builder $query
      * @return \Illuminate\Database\Eloquent\Builder
      */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
      * Scope a query to only include active users.
      *
      * @param \Illuminate\Database\Eloquent\Builder $query
      * @return \Illuminate\Database\Eloquent\Builder
      */
    public function scopeOrderbyname($query)
    {
        return $query->orderBy('name');
    }

    public function scopeExclude($query, $value = array())
    {
        return $query->select(array_diff($this->fillable, (array)$value));
    }

    public function getStoredProcedure(){
      return $this->storedProcedure;
    }
    public function getPrimaryKey(){
      return $this->primaryKey;
    }
}
