<?php


namespace App\Models\Base;

use App\Models\Base\RootModel;
use App\Libraries\WebshopConnect\UnasRestApi;

class BaseUnasModel  extends RootModel
{
    public $timestamps = false;
    public $APItable = '';

    protected $connector ;
    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->connector = new UnasRestApi();
        ///$this->connector->get();
        $this->setStoredProcedure();
        parent::__construct($attributes);
    }
    protected function setStoredProcedure(){}
}
