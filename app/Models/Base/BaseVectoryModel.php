<?php


namespace App\Models\Base;
use App\Models\Base\RootModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Libraries\WebshopConnect\VectoryConnector;
use App\Getuts;


class BaseVectoryModel extends RootModel
{
    public $timestamps = false;
    public $APItable = '';
    /**
     * The connection associated with the model.
     *
     * @var string
     */
    //protected $connection = 'sapsqlsrv';
    protected $connector ;

    public function getConnection()
    {
        return $this->APItable;
    }
//use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    //protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $dates = [];

    /**
     *
     * @var string
     */
    protected $storedProcedure;


    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->connector = new VectoryConnector('GET',[]);
        ///$this->connector->get();
        $this->setStoredProcedure();
        parent::__construct($attributes);
    }
    public function getFull($uts=-1)
    {
        if($uts == -1){
            $uts=Getuts::start($this->getStoredProcedure(),1)->uts;
        }
        $result = [];
        //dd('/szinkron/getdata?resourceName='.$this->getStoredProcedure().'&full=1&uts='.$uts) ;
        $response = $this->connector->get('/szinkron/getdata?resourceName='.$this->getStoredProcedure().'&full=1&uts='.$uts);
        if (isset($response[$this->getStoredProcedure()])){
            $result = $response[$this->getStoredProcedure()];
            $result_ = [];
            foreach ($result as $grouprow) {
                foreach ($grouprow as $row) {
                    $result_[] = $row;

                }
            }
            $result = $result_;
        }
        Getuts::stop($this->getStoredProcedure(),1);
        return $result;
    }
    public function getChange($uts=0)
    {
        if($uts == -1){
            $uts=Getuts::start($this->getStoredProcedure(),0)->uts;
        }
        $result = $this->connector->get('/szinkron/getdata?resourceName='.$this->getStoredProcedure().'&full=0&uts='.$uts);
        Getuts::stop($this->getStoredProcedure(),0);
        return $result;
    }

    protected function setStoredProcedure(){}



    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrderbyname($query)
    {
        return $query->orderBy('name');
    }

    public function scopeExclude($query, $value = array())
    {
        return $query->select(array_diff($this->fillable, (array)$value));
    }

    public function getStoredProcedure(){
        return $this->storedProcedure;
    }


}
