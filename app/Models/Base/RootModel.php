<?php

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use App\Libraries\Field;

class RootModel extends Model
{
   // use SoftDeletes;
    public $typeSelect = [];
    public $typeModel = '';

    public $timestamps = false;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;
    /**
     * The connection associated with the model.
     *
     * @var string
     */
    protected $connection = 'pgsql';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */

    public $incrementing = false;



    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */

//    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['pivot'];

    protected $fields = [];

    protected $cast = [];
    protected $alias;
    public $schema;

    protected static function boot()
    {
        parent::boot();

        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `id` field (provided by $model->getKeyName())
         * */
     //   static::creating(function ($model) {
//            $primaryKey = $this->primaryKey;

            ///if (in_array($model->primaryKey, $model->dates)){
            //   $model->primaryKey = $id;
            //}
           // if (in_array('updated_at', $model->dates))
               // $model->updated_by = \MyUser::getUserID();

      //  });

       // static::updating(function ($model) {
           /* if (in_array('updated_at', $model->dates))
                $model->updated_by = \MyUser::getUserID();*/
       /// });
    }
    protected static function nextseq($model)
    {
        return \DB::select('select nextval('."'".'"'.$model->schema.'".seq_'.$model->alias.'_id'."'".')')[0]->nextval;
    }
    public function save(array $options = [])
    {
        if (!is_array($this->getKeyName())) {
            return parent::save($options);
        }

        // Fire Event for others to hook
        if ($this->fireModelEvent('saving') === false) return false;

        // Prepare query for inserting or updating
        $query = $this->newQueryWithoutScopes();

        // Perform Update
        if ($this->exists) {
            if (count($this->getDirty()) > 0) {
                // Fire Event for others to hook
                if ($this->fireModelEvent('updating') === false) {
                    return false;
                }

                // Touch the timestamps
                if ($this->timestamps) {
                    $this->updateTimestamps();
                }

                //
                // START FIX
                //


                // Convert primary key into an array if it's a single value
                $primary = (count($this->getKeyName()) > 1) ? $this->getKeyName() : [$this->getKeyName()];

                // Fetch the primary key(s) values before any changes
                $unique = array_intersect_key($this->original, array_flip($primary));

                // Fetch the primary key(s) values after any changes
                $unique = !empty($unique) ? $unique : array_intersect_key($this->getAttributes(), array_flip($primary));

                // Fetch the element of the array if the array contains only a single element
                //$unique = (count($unique) <> 1) ? $unique : reset($unique);

                // Apply SQL logic
                $query->where($unique);

                //
                // END FIX
                //

                // Update the records
                $query->update($this->getDirty());

                // Fire an event for hooking into
                $this->fireModelEvent('updated', false);
            }
        }
        // Insert
        else {
            // Fire an event for hooking into
            if ($this->fireModelEvent('creating') === false) return false;

            // Touch the timestamps
            if ($this->timestamps) {
                $this->updateTimestamps();
            }

            // Retrieve the attributes
            $attributes = $this->attributes;

            if ($this->incrementing && !is_array($this->getKeyName())) {
                $this->insertAndSetId($query, $attributes);
            } else {
                $query->insert($attributes);
            }

            // Set exists to true in case someone tries to update it during an event
            $this->exists = true;

            // Fire an event for hooking into
            $this->fireModelEvent('created', false);
        }

        // Fires an event
        $this->fireModelEvent('saved', false);

        // Sync
        $this->original = $this->attributes;

        // Touches all relations
        if (array_get($options, 'touch', true)) $this->touchOwners();

        return true;
    }

    /**
     * Cast an attribute to a native PHP type.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        if (is_null($value)) {
            return $value;
        }

        switch ($this->getCastType($key)) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'real':
            case 'float':
            case 'double':
                return (float)$value;
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            case 'object':
                return $this->fromJson($value, true);
            case 'array':
            case 'json':
                return $this->fromJson($value);
            case 'collection':
                return new BaseCollection($this->fromJson($value));
            case 'date':
                return $this->asDate($value);
            case 'datetime':
            case 'custom_datetime':
                return $this->asDateTime($value);
            case 'timestamp':
                return $this->asTimestamp($value);
            case 'image':
                return $this->castImage($value);
            default:
                return $value;
        }
    }

    /**
     * The answer that belong to the question.
     */
    protected function castImage($value)
    {
        // return null;
        if (empty($value) || is_null($value)) {
            return null;
        }
        if (is_string($value)) {
            return $value;
        }
        $image = \MyString::byteToString($value);
        return $image;
    }

    /**
     * Get the value of fields
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set the value of fields
     *
     * @return  self
     */
    public function setFields(){}

}
