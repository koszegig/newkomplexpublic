<?php

namespace App\Providers;

use App\Pano;
use App\Policies\PanoPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
      //  'App\Pano' => 'App\Policies\PanoPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Grant full access to superusers and admins
        Gate::before(function ($user) {
            if ($user->hasRole('admin')
             OR in_array($user->email, config('auth.superusers'))){
                return true;
            }
        });
    }
}
