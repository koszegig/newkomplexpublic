<?php

namespace App\Providers;

use Response;
//use Illuminate\Support\ServiceProvider;
use Appstract\ResponseMacros\ResponseMacrosServiceProvider as ResponseMacrosServiceProviderMain;
class ResponseMacroServiceProvider extends ResponseMacrosServiceProviderMain
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('App\Libraries\ResponseMacros\ResponseMacros');
    }
}
