<?php

return [
    'komplexnode' => [
        'url' =>  env('KOMPLEXNODEURL', '127.0.0.1'),
        'ip' =>  env('KOMPLEXNODEIP', '127.0.0.1'),
        'port' =>  env('KOMPLEXNODEPORT', '127.0.0.1'),
    ],
    'komplextavnyomtatas' => [
        'url' =>  env('TAVNYOMTATASURL', '127.0.0.1'),
        'ip' =>  env('TAVNYOMTATASIP', '127.0.0.1'),
        'port' =>  env('TAVNYOMTATASPORT', '127.0.0.1'),
    ],
];
