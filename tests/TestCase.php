<?php

namespace Tests;

use App\Role;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function signInAs($role)
    {
    	$user = create('App\User');

    	$user->roles()->save(new Role([
    		'name' => $role
    	]));

    	$this->signIn($user);
    }

    protected function signIn($user = null)
    {
    	$user = $user ?: create('App\User');

    	$this->actingAs($user);

    	return $this;
    }
}
