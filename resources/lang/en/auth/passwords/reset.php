<?php

return [

    'title' => 'Reset Password',
    'email' => 'E-Mail Address',
    'password' => 'Password',
    'password-confirm' => 'Confirm Password',
    'button-reset' => 'Reset Password',

];
