<?php

return [

    'title' => 'Oops! - Page Not Found',
    'content' => 'Sorry, an error has occured - either return to the home page, or if you are sure you are looking at the right place, please tell our support!',
    'button-home' => 'Take Me Home',
    'button-support' => 'Contact Support',
];
