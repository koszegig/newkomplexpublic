<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Login Page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on the Login page.
    |
    */

    'email' => 'Email cím',
    'password' => 'Jelszó',
    'login' => 'Bejelentkezés',
    'remember' => 'Bejelentkezve maradok',
    'forgot' => 'Elfelejtetted a jelszavadat?',

];
