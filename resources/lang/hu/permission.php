<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Permission Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage_permissions' => 'Jogosultságok kezelése',
    'manage_roles' => 'Szerepkörök kezelése',
    'manage_users' => 'Felhasználók kezelése',
];
