@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center jumbotron">
                <h1 style="font-family: Josefin Sans,sans-serif;">
                    404
                </h1>
                <h2>
                    @lang('errors/custom.title')
                </h2>
                <br>
                <p>
                    <small>
                        @lang('errors/custom.content')
                    </small>
                </p>
                <hr>
                <a href="/" class="btn btn-primary">
                    <span class="glyphicon glyphicon-home"></span>
                    &nbsp;@lang('errors/custom.button-home')
                </a>
                <a href="mailto:404@antara.hu" class="btn btn-default">
                    <span class="glyphicon glyphicon-envelope"></span>
                    &nbsp;@lang('errors/custom.button-support')
                </a>
            </div>
        </div>
    </div>
@endsection
