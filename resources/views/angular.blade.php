<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta name="description" content="Komplex II Erp rendszer">
  <meta name="author" content="Kőszegi Gábor ">

  <title>:: Komplex II ::</title>
  <base href="http://koszegigabor69.no-ip.biz/komplex/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link href="https://cdn.syncfusion.com/ej2/material.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.6.1/fullcalendar.min.css">

<link rel="stylesheet" href="styles.0b76def6b4748498b5c7.css"></head>

<body>
<app-root>Betöltés...</app-root>
<link rel="stylesheet" href="http://koszegigabor69.no-ip.biz/komplex/assets/css/main.css">
<link rel="stylesheet" href="http://koszegigabor69.no-ip.biz/komplex/assets/css/color_skins.css">
<script src="runtime.eefc74c6f087c400f52e.js"></script><script src="polyfills-es5.a94e2579fdb2235d2222.js" nomodule></script><script src="polyfills.a3df34c3e8905ca7065d.js"></script><script src="main.dd068c9ff1abdc27f3ea.js"></script></body>
</html>
