<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Overwrite default auth register function to log attempts instead of registering them
//

Route::get('/hash/{string}', 'TestController@generatehash');
Route::get('test', 'TestController@index');
Route::get('daterange', 'TestController@testDateRange');
Route::get('kpi', 'TestController@getKPI');
Route::resource('/', 'WelcomeController');
//Route::resource('/', 'AngularController');
//Route::any('/{any}', [AngularController::class, 'index'])->where('any', '^(?!api).*$');
Route::get('/downloaddata/{file}', 'DownloadPdfController@downloadpdf');
Route::get('/downloadenv', 'DownloadPdfController@env');
Route::get('/komplexenv', 'KomplexNodeController@env');
/*Route::get('/', function () {
    return File::get(public_path().'\index.html');
});*/
//Route::get('Threeiworks', 'Externally\Threeiworks@index');
/*Route::get('/', function () {
    //return view('welcome');
    View:addExtension('html','php');
    return View:make(index);
});*/
