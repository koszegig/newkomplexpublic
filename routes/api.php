<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');
// Route::middleware('auth:api')->get('/user', function (Request $request) {
// return $request->user();
// });
$api->version('v1', function ($api) {
  $api->group(['namespace' => 'App\Http\Controllers'], function ($api) {
    $api->group(['middleware' => ['cors']], function ($api) {
      $api->group(['namespace' => 'Auth'], function ($api) {
        $api->group(['prefix' => 'auth'], function ($api) {
          $api->post('/login', 'LoginController@postLogin');
          $api->post('/login/app', 'LoginController@postLoginApp');
        });
        $api->group(['prefix' => 'user'], function ($api) {
          $api->post('/registration', 'RegisterController@register');
        });
      });
    });
// user custom apies
      $api->group(['prefix' => 'user'], function ($api) {
          $api->get('/refresh_token', 'UserController@refreshToken');
      });
    // $api->group(['middleware' => ['cors','jwt-auth']], function ($api) {
    //
    $api->group(['prefix' => 'user'], function ($api) {
      $api->get('/refresh_token', 'UserController@refresh_token');
    });

    $api->group(['prefix' => 'rendszer'], function ($api) {
      $api->get('/mezoleirasok/{p_table_named}', 'DMMezokcontroller@getMezoLeirasok');
    });
    $api->group(['prefix' => 'rendszer'], function ($api) {
      $api->get('/menuparameterek/{menu_id}', 'DMmenuparameterekController@getMenuparameterek');
    });
    /* $api->group(['prefix' => 'cikkek'], function ($api) {
            $api->get('/onepage/{skip}/{base_macro}','App\Http\Controllers\DMCikkekcontroller@onepage');
        });*/
    $api->group(['prefix' => 'cikkek'], function ($api) {
      //$api->get('/list/{size?}','DMCikkekcontroller@list');
    });
    $api->group(['prefix' => 'partnerek'], function ($api) {
      //$api->get('/onepage/{skip}/{base_macro}','App\Http\Controllers\DMPartnerController@onepage');
      //$api->get('/list/{size?}','DMPartnerController@list');
    });
    $api->group(['prefix' => 'partnercimek'], function ($api) {
      //$api->get('/list/{size?}','DMPartnerCimekController@list');
      $api->post('/onepage', 'DMPartnerCimekController@onepage');
    });
    $api->group(['prefix' => 'partnerbankszamlaszamok'], function ($api) {
      //$api->get('/list/{size?}','DMPartnerBankszamlaszamokController@list');
      $api->get('/onepage/{base_macro}', 'DMPartnerBankszamlaszamokController@onepage');
    });
    /*$api->group(['prefix' => 'penznemek'], function ($api) {
      $api->get('/list/dropdown', 'DMPenznemController@listForDn');
      $api->get('/onepage/{base_macro}', 'DMPenznemController@onepage');
      //$api->get('/list/{size?}','DMPenznemController@list');
    });*/
    $api->group(['prefix' => 'afak'], function ($api) {
      $api->post('/list','DMAfakController@list');
      //$api->get('/onepage/{skip}/{base_macro}','App\Http\Controllers\DMPartnerController@onepage');
      //$api->get('/list/{size?}','DMAfakController@list');
      //$api->get('/onepage/{skip}/{base_macro}','App\Http\Controllers\DMAfakController@onepage');
    });
    $api->group(['prefix' => 'fizetesimodok'], function ($api) {
      $api->post('/list','DMfizetesimodokController@list');
      $api->post('/getbizfkifdatuma', 'DMfizetesimodokController@getBizfKifDatuma');
      $api->post('/getazonalifizetes', 'DMfizetesimodokController@get_azonali_fizetes');
      $api->get('/list/dropdown', 'DMfizetesimodokController@listForDn');
      //$api->get('/list/{size?}','DMfizetesimodokController@list');

      //$api->get('/onepage/{skip}/{base_macro}','App\Http\Controllers\DMfizetesimodokController@onepage');
    });
    $api->group(['prefix' => 'rendszerkodok'], function ($api) {
      $api->post('/list','DMrendszerkodokController@list');
      $api->get('/list/dropdown', 'DMrendszerkodokController@listForDn');
    });
    $api->group(['prefix' => 'raktarak'], function ($api) {
      $api->post('/list','DMRaktarakController@list');
      //$api->get('/list/dropdown', 'DMRaktarakController@listForDn');
      ///$api->get('/list/{size?}','DMRaktarakController@list');
      //$api->get('/onepage/{skip}/{base_macro}','App\Http\Controllers\DMRaktarakController@onepage');
    });

    $api->group(['prefix' => 'azonositok'], function ($api) {
      $api->post('/list','DMAzonositokController@list');
      $api->post('/oneuserpage', 'DMAzonositokController@oneUserPage');
      $api->post('/onepage', 'DMAzonositokController@onepage');
      $api->post('/getazonosito', 'DMAzonositokController@azonositokByMenuid');

    });
    /*$api->group(['prefix' => 'bizonylattetelek'], function ($api) {
      $api->put('/','DMBizonylattetelekController@update');
    });*/

    $api->group(['prefix' => 'common'], function ($api) {
      //$api->post('/bizonylatfejmozgasnemdropdown', 'DMCommoncontroller@bizonylatfejmozgasnemlistForDn');
      $api->post('/getrendszerkodok', 'DMCommoncontroller@get_rendszer_kodokForDn');
      //$api->get('/list/{size?}','DMRaktarakController@list');
      //$api->get('/onepage/{skip}/{base_macro}','App\Http\Controllers\DMRaktarakController@onepage');
    });
    $api->group(['prefix' => 'jogcimek'], function ($api) {
      $api->post('/list','DMJogcimekController@list');
      $api->post('/onepage', 'DMJogcimekController@onepage');
    });
    $api->group(['prefix' => 'mennyisegiegyseg'], function ($api) {
      $api->post('/list','DMMennyisegiegysegController@list');
        $api->post('/list/dropdown', 'DMMennyisegiegysegController@listForDn');
      //$api->get('/onepage/{base_macro}', 'DMMennyisegiegysegController@onepage');
    });
      $api->group(['prefix' => 'bizonylatfejmozgasnem'], function ($api) {
          $api->post('/list','DMbizonylatfejmozgasnemController@list');
          //$api->get('/onepage/{base_macro}', 'DMMennyisegiegysegController@onepage');
      });

      $api->group(['prefix' => 'bizonylatoklekerdezese'], function ($api) {
          $api->get('/downloadpdf/{bizf_id}','DMBizonylatTetelekLekerdezeseController@downloadpdf');
          $api->get('/exportinvoice/{bizf_id}','DMBizonylatTetelekLekerdezeseController@exportinvoice');
          //$api->get('/onepage/{base_macro}', 'DMMennyisegiegysegController@onepage');
      });
      $api->group(['prefix' => 'cikkekarlistaweb'], function ($api) {
          $api->get('/exportcikkcsv/{file}','DMCikkekArlistaWebController@exportcikkcsv');
          $api->get('/exportcikkcsvdownload/{file}','DMCikkekArlistaWebController@exportcikkcsvdownload');
          //$api->get('/onepage/{base_macro}', 'DMMennyisegiegysegController@onepage');
      });

    $api->resource('/bizonylatfej', 'DMBizonylatfejController');
    $api->resource('/bizonylattetelek', 'DMBizonylattetelekController');
    $api->resource('/jogcimek', 'DMJogcimekController');
    $api->resource('/cikkek', 'DMCikkekcontroller');
    $api->resource('/partnerek', 'DMPartnerController');
    $api->resource('/partnercimek', 'DMPartnerCimekController');
    $api->resource('/partnerbankszamlaszamok', 'DMPartnerBankszamlaszamokController');
    $api->resource('/partnerkartya', 'DMPartnerkartyakController');
    $api->resource('/azonositok', 'DMAzonositokController');
    $api->resource('/penznemek', 'DMPenznemController');
    $api->resource('/afak', 'DMAfakController');
    $api->resource('/fizetesimodok', 'DMfizetesimodokController');
    $api->resource('/raktarak', 'DMRaktarakController');
    $api->resource('/mennyisegiegysegek', 'DMMennyisegiegysegController');
    $api->resource('/vonalkodok', 'DMVonalkodokController');
    $api->resource('/user', 'UserController');
    $api->resource('/usergroup', 'UserGroupController');
    $api->resource('/role', 'RoleController');
    $api->resource('/permission', 'PermissionController');
    $api->resource('/cikkekarlistaweb', 'DMCikkekArlistaWebController');
    $api->resource('/downloadpdf', 'DownloadPdfController');
    $api->resource('/exportcikkek', 'ExportcikkekController');
    $api->resource('/bizonylatoklekerdezese', 'DMBizonylatTetelekLekerdezeseController');
    $api->resource('/bizonylattetelei', 'DMBizonylatTeteleiController');
    $api->resource('/munkahelyek', 'DMmunkahelyekController');
    $api->resource('/munkalapfejek', 'DMmunkalapfejekController');
    $api->resource('/munkalaptetelek', 'DMmunkalaptetelekController');
    $api->resource('/wbswcorderrecord', 'DMwbswcorderController');
    $api->resource('/navtranzakciotetelek', 'DMnavtranzakciotetelekController');
    $api->resource('/navtasks', 'DMnavtasksController');
    $api->resource('/rendszerkodok', 'DMrendszerkodokController');
    $api->resource('/bizonylatfejmozgasnem', 'DMbizonylatfejmozgasnemController');
    $api->resource('/selectcikkek', 'DMSelectCikkekController');
    $api->get('/komplexenv', 'KomplexNodeController@env');
    $api->get('/downloadenv', 'DownloadPdfController@env');
    //  });
  });
});
